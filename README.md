## Core1990: Interlaken protocol

## Description

Core1990 is a point-to-point communication protocol using the royalty-free Interlaken protocol as its foundation.
It is designed by engineers and students of the Electronics Department of [Nikhef](https://www.nikhef.nl/) (Amsterdam, The Netherlands) with large experiments at CERN (e.g. [ATLAS](https://atlas.cern/)) in mind.  The development of Core1990 was intended to explore and publish an open source protocol providing high throughput with a small percentage of overhead. Core1990 mixes simplicity, being openly accessible in VHDL, with robustness by featuring flow control, error detection and DC balancing. This while being as vendor independent as possible, so no vendor dependent IP cores unless there's no other choice.

## The Interlaken protocol

Core1990 is developed according to the Interlaken Protocol Definition Revision 1.2 which was published on October 7, 2008.

The Interlaken Protocol features:  
\* CRC24 & CRC32 error detection  
\* In-band & out-of-band flow control (simple Xon/Xoff)  
\* 64b67b line encoding and scrambling  
\* Performance that scales with the number of lanes
(channel bonding)  
\* Possibility to reach high data rates (>400Gbps)  
\* Several extensions released by the “Interlaken Alliance”  


![Core1990_overview](images/Interlaken_25G_SL.png)

## Building the firmware
The repository can be cloned by running the following commands.
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/core1990_interlaken.git
cd core1990_interlaken/
```

### Vivado
For Xilinx FPGA's Vivado 2021.1 and 2022.2 have been used to succesfully build the core. To generate the project the Vivado TCL console has to be used.

```
# Change to the git repo directory in the Vivado TCL console 
cd ./scripts/interlaken_top/
source ./FLX128_import_vivado.tcl
```

### IntelFGPA (Work in progress)
For IntelFPGA devices Quartus Pro 21.2 has been used to succesfully build the core. To generate the project the TCL console has to be used. This is only available on the IntelFPGA branch and functionality has not been fully verified yet.

```
# Change to the git repo directory in the TCL console 
cd ./scripts/
source ./quartus_gen_project.tcl
```

## File structure

The generated project has been split in multiple files and entities. The structure is as follows. 

```bash
├── interlaken_top.vhd
    ├── interlaken_interface.vhd
    │   ├── interlaken_gty.vhd (Xilinx)
    │   │   ├── transceiver_64b67b_block_sync_sm.vhd
    │   │   ├── txgearbox_64b67b.vhd (Versal only)
    │   │   └── rxgearbox_64b67b.vhd (Versal only)
    │   ├── reset_logic.vhd
    │   ├── interlaken_transmitter_multichannel.vhd
    │   │   ├── axis64fifo.vhd
    │   │   └── interlaken_transmitter.vhd
    │   │       ├── framing_burst.vhd
    │   │       ├── framing_meta.vhd
    │   │       ├── scrambler.vhd
    │   │       └── encoder.vhd
    │   └── interlaken_receiver_multichannel.vhd
    │       ├── axis64fifo.vhd
    │       └── interlaken_receiver.vhd
    │           ├── deframing_burst.vhd
    │           ├── deframing_meta.vhd
    │           ├── descrambler.vhd
    │           └── decoder.vhd
    └── data_generator.vhd
```

## Interfacing
Transmitting and receiving data can be done by using the AMBA AXI-Stream protocol. \
s_axis(lane) transmits data and m_axis(lane) receives data.
```
s_axis.tdata   : in std_logic_vector(63 downto 0) - User data word
s_axis.tvalid  : in std_logic - User data word is valid  
s_axis.tlast   : in std_logic - Last data word of stream / End of packet  
s_axis.tkeep   : in std_logic_vector(7 downto 0) - Valid bytes (Valid while tlast is asserted)  
s_axis.tuser   : in std_logic_vector(3 downto 0) - Currently usused at the transmitting side  
s_axis.tid     : in std_logic_vector(7 downto 0) - Channel number  
s_axis_tready  : out std_logic - Bus is ready to accept user data
```
At the receiving side m_axis.tuser is utilized for status/error signals.
```
m_axis.tuser(3) - RX FIFO not accepting data
m_axis.tuser(2) - Flowcontrol status (not correctly utilized yet)
m_axis.tuser(1) - Combined error signal from Meta/Burst deframing if any
m_axis.tuser(0) - Combined error signal from CRC24 and CRC32
```

## Configuration
By default the Interlaken receiver will be configured with parameters recommended by the Interlaken Protocol Definition. However it is possible to change some of these configurations according to desires.
```
BurstMax     : positive     - Configurable value of BurstMax
BurstShort   : positive     - Configurable value of BurstShort
PacketLength : positive     - Configurable value of PacketLength
lanes        : positive    - Number of Lanes (Transmission channels)
txlanes      : integer     - Enable/disable tx lanes
rxlanes      : integer     - Enable/disable rx lanes
BondNumberOfLanes : positive   - Bonded amount of lanes (not utilized for now)
CARD_TYPE    : integer    - FPGA card type number
GTREFCLKS    : integer     - Amount of GTRefclks (Connected to transceivers)
```

## Status and control
The core contains several status signals and one control signal to put the core in loopback. Important matters will be notified through the use of dedicated status signa.

```
Decoder_Lock      - Decoder of lane is in lock
Descrambler_lock  - Descrambler of lane is in lock
HealthLane        - Lane is ready to receive user data and has no errors
HealthInterface   - Same as HealthLane but for all lanes in an Interlaken instantiation
FlowControl       - Flow control status received from other end of the connection
loopback_in       - Control signal to put transceiver in loopback
```


## Error handling 
The core features several error signals to notify the user of problems. Two error signals are provided which indicate whether a CRC24 or CRC32 error has occurred. This should invalidate the data.
```
decoder_error_sync        - Decoder cannot synchronize on preamble bits
descrambler_error_badsync - Received three bad Synchronization words while being in lock
descrambler_error_statemismatch - Received three mismatching Scrambler State words while being in lock
descrambler_error_nosync  - No Synchronization words detected in data stream
burst_crc24_error         - Burst packet contains faulty CRC24
meta_crc32_error          - Meta frame contains faulty CRC32
```

## Propriatary information
One of the aims of Core1990 is to make the Interlaken Protocol accessible to all developers despite the FPGA vendor. That's why the firmware will be designed with as much vendor indepenency as possible. This means no verdor specific cores/logic will be used unless there is no other option. All verdor dependent logic used in the core will be listed here.

\* The Interlaken top file contains all vendor dependent IP cores such as ILA's and VIO's (Xilinx).  
\* The design makes use of a Xilinx GTY transceiver.  
\* TX/RX lanes use a XPM FIFO (CDC between user data clk and interlaken data clk).  

## Status

This is the current status of the project:  
\* Data is packed into bursts and metaframes and is unpacked by the receiver  
\* CRC-24/32 are generated and checked  
\* Data is encoded/decoded 64b/67b (includes scrambling)  
\* Hardware implementation targets Xilinx devices (Tested on VCU128 Eval Kit)  
\* Verified with an official Xilinx Interlaken IP-Core  
\* Support for GTX (10G) and GTY (25G) tranceivers and line rates  
  
Last updated April 2021

## To Do

\* Hardware implementation for IntelFPGA devices  
\* Support other FPGA platforms  
\* Implement channel bonding (Currently WIP)  


## OpenCores


\>> Core1990 is open source and available for public on OpenCores:
[Core1990 Interlaken protocol](https://opencores.org/projects/core1990_interlaken)

## Interlaken alliance
Here are some pointers to the official [Interlaken Alliance](http://www.interlakenalliance.com/) and the [Interlaken Protocol Definition v1.2](http://www.interlakenalliance.com/Interlaken_Protocol_Definition_v1.2.pdf) (Copyright © 2006 Cortina Systems, Inc. and Cisco Systems, Inc.)
