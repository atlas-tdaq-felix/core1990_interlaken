
#QSFP4 connected pins
set_property PACKAGE_PIN AG45 [get_ports {gt_rx_p[0]}]
set_property PACKAGE_PIN AG46 [get_ports {gt_rx_n[0]}]
set_property PACKAGE_PIN AK42 [get_ports {gt_tx_p[0]}]
set_property PACKAGE_PIN AK43 [get_ports {gt_tx_n[0]}]

##GTREFCLK0
set_property PACKAGE_PIN AF38 [get_ports GTREFCLK_IN_P[0]]
create_clock -period 6.400 -name qsfp_clock_1 [get_ports GTREFCLK_IN_P[0]]

# CPU_RESET
set_property PACKAGE_PIN E36 [get_ports sys_rst]
set_property IOSTANDARD LVCMOS12 [get_ports sys_rst]
#set_property IS_LOC_FIXED 0 [get_cells sys_rst_IBUF_inst/IBUFCTRL_INST]


set_property PACKAGE_PIN BC9 [get_ports qdr4_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports qdr4_clk_p]
create_clock -period 10.000 -name qdr_clock [get_ports qdr4_clk_p]

