##QSFP1 - Bank 231 X1Y12
set_property PACKAGE_PIN Y2 [get_ports {gt_rx_p[0]}]
set_property PACKAGE_PIN W4 [get_ports {gt_rx_p[1]}]
set_property PACKAGE_PIN V2 [get_ports {gt_rx_p[2]}]
set_property PACKAGE_PIN U4 [get_ports {gt_rx_p[3]}]

##GTREFCLK0
set_property PACKAGE_PIN W9 [get_ports GTREFCLK_IN_P[0]]
create_clock -period 6.400 -name qsfp_clock_1 [get_ports GTREFCLK_IN_P[0]]

# CPU_RESET Pushbutton
set_property PACKAGE_PIN L19 [get_ports sys_rst]
set_property IOSTANDARD LVCMOS12 [get_ports sys_rst]
#set_property IS_LOC_FIXED 0 [get_cells sys_rst_IBUF_inst/IBUFCTRL_INST]

#Free-running clk - 125 MHz
set_property PACKAGE_PIN AY24 [get_ports freerun_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports freerun_clk_p]
create_clock -period 8.000 -name qdr_clock [get_ports freerun_clk_p]

##QSFP1  - Bank 135 X0Y11
#set_property PACKAGE_PIN G53 [get_ports {gt_rx_p[0]}]
#set_property PACKAGE_PIN F51 [get_ports {gt_rx_p[1]}]
#set_property PACKAGE_PIN E53 [get_ports {gt_rx_p[2]}]
#set_property PACKAGE_PIN D51 [get_ports {gt_rx_p[3]}]

##GTREFCLK0
#set_property PACKAGE_PIN P42 [get_ports {GTREFCLK_IN_P[0]}]
#set_property PACKAGE_PIN P43 [get_ports {GTREFCLK_IN_N[0]}]
#create_clock -period 6.400 -name qsfp_clock0 [get_ports {GTREFCLK_IN_P[0]}]

##QSFP2  - Bank 134 X0Y10
#set_property PACKAGE_PIN L53 [get_ports {gt_rx_p[4]}]
#set_property PACKAGE_PIN K51 [get_ports {gt_rx_p[5]}]
#set_property PACKAGE_PIN J53 [get_ports {gt_rx_p[6]}]
#set_property PACKAGE_PIN H51 [get_ports {gt_rx_p[7]}]

##GTREFCLK0
#set_property PACKAGE_PIN T42 [get_ports {GTREFCLK_IN_P[1]}]
#set_property PACKAGE_PIN T43 [get_ports {GTREFCLK_IN_N[1]}]
#create_clock -period 6.400 -name qsfp_clock1 [get_ports {GTREFCLK_IN_P[1]}]

##QSFP3  - Bank 132 X0Y8
#set_property PACKAGE_PIN U53 [get_ports {gt_rx_p[8]}]
#set_property PACKAGE_PIN U49 [get_ports {gt_rx_p[9]}]
#set_property PACKAGE_PIN T51 [get_ports {gt_rx_p[10]}]
#set_property PACKAGE_PIN R53 [get_ports {gt_rx_p[11]}]

##GTREFCLK0
#set_property PACKAGE_PIN Y42 [get_ports {GTREFCLK_IN_P[2]}]
#create_clock -period 6.400 -name qsfp_clock2 [get_ports {GTREFCLK_IN_P[2]}]

##QSFP4  - Bank 131 X0Y7
#set_property PACKAGE_PIN AA53 [get_ports {gt_rx_p[12]}]
#set_property PACKAGE_PIN Y51  [get_ports {gt_rx_p[13]}]
#set_property PACKAGE_PIN W53  [get_ports {gt_rx_p[14]}]
#set_property PACKAGE_PIN V51  [get_ports {gt_rx_p[15]}]

##GTREFCLK0
#set_property PACKAGE_PIN AB42 [get_ports {GTREFCLK_IN_P[3]}]
#create_clock -period 6.400 -name qsfp_clock1 [get_ports {GTREFCLK_IN_P[3]}]
