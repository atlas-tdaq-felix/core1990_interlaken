create_clock -name {altera_reserved_tck} -period 41.667 [get_ports { altera_reserved_tck }]

create_clock -name {refclk} -period "644.53125MHz"   [get_ports { refclk }]

create_clock -name {clk_50} -period "50.0MHz" [get_ports { clk_50 }]

set_clock_groups -asynchronous\
  -group {*|phy|rx_clkout|ch0}\
  -group {*|phy|tx_clkout|ch0}\
  -group {refclk}\
  -group {clk_50}\
  -group {altera_reserved_tck}
