
## FMC DP20-23 - FF4
#set_property PACKAGE_PIN AG53 [get_ports {gt_rx_p[0]}]
#set_property PACKAGE_PIN AF51 [get_ports {gt_rx_p[1]}]
#set_property PACKAGE_PIN AE53 [get_ports {gt_rx_p[2]}]
#set_property PACKAGE_PIN AE49 [get_ports {gt_rx_p[3]}]
##GTREFCLK0
#set_property PACKAGE_PIN AG40 [get_ports {GTREFCLK_IN_P[0]}]
#create_clock -period 6.400 -name FMCP_HSPC_GBTCLK5_M2C [get_ports {GTREFCLK_IN_P[1]}]

## FMC DP16-19 - FF
#set_property PACKAGE_PIN AL49 [get_ports {gt_rx_p[4]}]
#set_property PACKAGE_PIN AK51 [get_ports {gt_rx_p[5]}]
#set_property PACKAGE_PIN AJ53 [get_ports {gt_rx_p[6]}]
#set_property PACKAGE_PIN AH51 [get_ports {gt_rx_p[7]}]
##GTREFCLK1
#set_property PACKAGE_PIN AJ40 [get_ports {GTREFCLK_IN_P[1]}]
#create_clock -period 6.400 -name FMCP_HSPC_GBTCLK4_M2C [get_ports {GTREFCLK_IN_P[0]}]

## FMC DP12-15 - FF
#set_property PACKAGE_PIN AN53 [get_ports {gt_rx_p[8]}]
#set_property PACKAGE_PIN AN49 [get_ports {gt_rx_p[9]}]
#set_property PACKAGE_PIN AM51 [get_ports {gt_rx_p[10]}]
#set_property PACKAGE_PIN AL53 [get_ports {gt_rx_p[11]}]
##GTREFCLK2
#set_property PACKAGE_PIN AL40 [get_ports {GTREFCLK_IN_P[2]}]
#create_clock -period 6.400 -name FMCP_HSPC_GBTCLK3_M2C [get_ports {GTREFCLK_IN_P[2]}]

#############################################################
###############   QSFP4 pin definitions    ##################
#############################################################

set_property PACKAGE_PIN BH21 [get_ports qsfp4_intl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_intl_ls]
set_property PACKAGE_PIN BK23 [get_ports qsfp4_modskll_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modskll_ls]
set_property PACKAGE_PIN BK24 [get_ports qsfp4_resetl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_resetl_ls]
set_property PACKAGE_PIN BL22 [get_ports qsfp4_modprsl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modprsl_ls]
set_property PACKAGE_PIN BF23 [get_ports qsfp4_lpmode_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_lpmode_ls]

#############################################################
###############   QSFP3 pin definitions    ##################
#############################################################

set_property PACKAGE_PIN BL7 [get_ports qsfp3_intl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp3_intl_ls]
set_property PACKAGE_PIN BM5 [get_ports qsfp3_modskll_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp3_modskll_ls]
set_property PACKAGE_PIN BL6 [get_ports qsfp3_resetl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp3_resetl_ls]
set_property PACKAGE_PIN BM7 [get_ports qsfp3_modprsl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp3_modprsl_ls]
set_property PACKAGE_PIN BN4 [get_ports qsfp3_lpmode_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp3_lpmode_ls]



# SMA output
set_property PACKAGE_PIN BL25 [get_ports SMA_CLK_OUTPUT_N]
set_property IOSTANDARD LVCMOS18 [get_ports SMA_CLK_OUTPUT_N]
set_property PACKAGE_PIN BK26 [get_ports SMA_CLK_OUTPUT_P]
set_property IOSTANDARD LVCMOS18 [get_ports SMA_CLK_OUTPUT_P]

# CPU_RESET
set_property PACKAGE_PIN BM29 [get_ports sys_rst]
set_property IOSTANDARD LVCMOS12 [get_ports sys_rst]
#set_property IS_LOC_FIXED 0 [get_cells sys_rst_IBUF_inst/IBUFCTRL_INST]

#Free running clk
set_property PACKAGE_PIN BJ4 [get_ports qdr4_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports qdr4_clk_p]
create_clock -period 10.000 -name qdr_clock [get_ports qdr4_clk_p]

set_property PACKAGE_PIN L23 [get_ports {FF_PRESENTL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_PRESENTL[0]}]
set_property PACKAGE_PIN K22 [get_ports {FF_PRESENTL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_PRESENTL[1]}]
set_property PACKAGE_PIN C25 [get_ports {FF_PRESENTL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_PRESENTL[2]}]
set_property PACKAGE_PIN C24 [get_ports {FF_PRESENTL[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_PRESENTL[3]}]

set_property PACKAGE_PIN K27 [get_ports {FF_ResetL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_ResetL[0]}]
set_property PACKAGE_PIN J27 [get_ports {FF_ResetL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_ResetL[1]}]
set_property PACKAGE_PIN B26 [get_ports {FF_ResetL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_ResetL[2]}]
set_property PACKAGE_PIN B25 [get_ports {FF_ResetL[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_ResetL[3]}]

set_property PACKAGE_PIN J26 [get_ports {FF_INITL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_INITL[0]}]
set_property PACKAGE_PIN J25 [get_ports {FF_INITL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_INITL[1]}]
set_property PACKAGE_PIN B18 [get_ports {FF_INITL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_INITL[2]}]
set_property PACKAGE_PIN B17 [get_ports {FF_INITL[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {FF_INITL[3]}]

set_property PACKAGE_PIN A19 [get_ports {Si5345_INTR[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {Si5345_INTR[0]}]
set_property PACKAGE_PIN A18 [get_ports {Si5345_INTR[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {Si5345_INTR[1]}]

set_property PACKAGE_PIN C18 [get_ports {Si5345_RST[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {Si5345_RST[0]}]
set_property PACKAGE_PIN C17 [get_ports {Si5345_RST[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {Si5345_RST[1]}]

set_property PACKAGE_PIN G21 [get_ports PCA9548_RST]
set_property IOSTANDARD LVCMOS18 [get_ports PCA9548_RST]

#PL_I2C0_SCL/SDA_LS
set_property PACKAGE_PIN BM27 [get_ports SCL]
set_property IOSTANDARD LVCMOS18 [get_ports SCL]
set_property PACKAGE_PIN BL28 [get_ports SDA]
set_property IOSTANDARD LVCMOS18 [get_ports SDA]

#UART0
set_property PACKAGE_PIN BN26 [get_ports UART_TX]
set_property IOSTANDARD LVCMOS18 [get_ports UART_TX]
set_property PACKAGE_PIN BP26 [get_ports UART_RX]
set_property IOSTANDARD LVCMOS18 [get_ports UART_RX]

#############################################################
###############   QSFP4 pin definitions    ##################
#############################################################

set_property PACKAGE_PIN BH21 [get_ports qsfp4_intl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_intl_ls]
set_property PACKAGE_PIN BK23 [get_ports qsfp4_modskll_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modskll_ls]
set_property PACKAGE_PIN BK24 [get_ports qsfp4_resetl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_resetl_ls]
set_property PACKAGE_PIN BL22 [get_ports qsfp4_modprsl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modprsl_ls]
set_property PACKAGE_PIN BF23 [get_ports qsfp4_lpmode_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_lpmode_ls]



##QSFP1  - Bank 135 X0Y11
#set_property PACKAGE_PIN G53 [get_ports {gt_rx_p[0]}]
#set_property PACKAGE_PIN F51 [get_ports {gt_rx_p[1]}]
#set_property PACKAGE_PIN E53 [get_ports {gt_rx_p[2]}]
#set_property PACKAGE_PIN D51 [get_ports {gt_rx_p[3]}]

##GTREFCLK0
#set_property PACKAGE_PIN P42 [get_ports {GTREFCLK_IN_P[0]}]
#set_property PACKAGE_PIN P43 [get_ports {GTREFCLK_IN_N[0]}]
#create_clock -period 6.400 -name qsfp_clock0 [get_ports {GTREFCLK_IN_P[0]}]

##QSFP2  - Bank 134 X0Y10
#set_property PACKAGE_PIN L53 [get_ports {gt_rx_p[4]}]
#set_property PACKAGE_PIN K51 [get_ports {gt_rx_p[5]}]
#set_property PACKAGE_PIN J53 [get_ports {gt_rx_p[6]}]
#set_property PACKAGE_PIN H51 [get_ports {gt_rx_p[7]}]

##GTREFCLK0
#set_property PACKAGE_PIN T42 [get_ports {GTREFCLK_IN_P[1]}]
#set_property PACKAGE_PIN T43 [get_ports {GTREFCLK_IN_N[1]}]
#create_clock -period 6.400 -name qsfp_clock1 [get_ports {GTREFCLK_IN_P[1]}]

##QSFP3  - Bank 132 X0Y8
#set_property PACKAGE_PIN U53 [get_ports {gt_rx_p[8]}]
#set_property PACKAGE_PIN U49 [get_ports {gt_rx_p[9]}]
#set_property PACKAGE_PIN T51 [get_ports {gt_rx_p[10]}]
#set_property PACKAGE_PIN R53 [get_ports {gt_rx_p[11]}]

##GTREFCLK0
#set_property PACKAGE_PIN Y42 [get_ports {GTREFCLK_IN_P[2]}]
#create_clock -period 6.400 -name qsfp_clock2 [get_ports {GTREFCLK_IN_P[2]}]

##QSFP4  - Bank 131 X0Y7
#set_property PACKAGE_PIN AA53 [get_ports {gt_rx_p[12]}]
#set_property PACKAGE_PIN Y51  [get_ports {gt_rx_p[13]}]
#set_property PACKAGE_PIN W53  [get_ports {gt_rx_p[14]}]
#set_property PACKAGE_PIN V51  [get_ports {gt_rx_p[15]}]

##GTREFCLK0
#set_property PACKAGE_PIN AB42 [get_ports {GTREFCLK_IN_P[3]}]
#create_clock -period 6.400 -name qsfp_clock1 [get_ports {GTREFCLK_IN_P[3]}]

##QSFP4  - Bank 131 X0Y7
set_property PACKAGE_PIN AA53 [get_ports {gt_rx_p[0]}]
set_property PACKAGE_PIN Y51  [get_ports {gt_rx_p[1]}]
set_property PACKAGE_PIN W53  [get_ports {gt_rx_p[2]}]
set_property PACKAGE_PIN V51  [get_ports {gt_rx_p[3]}]


##GTREFCLK0
set_property PACKAGE_PIN AB42 [get_ports GTREFCLK_IN_P[0]]
create_clock -period 6.400 -name qsfp_clock_3 [get_ports GTREFCLK_IN_P[0]]

##QSFP3  - Bank 132 X0Y8
set_property PACKAGE_PIN U53 [get_ports {gt_rx_p[4]}]
set_property PACKAGE_PIN U49 [get_ports {gt_rx_p[5]}]
set_property PACKAGE_PIN T51 [get_ports {gt_rx_p[6]}]
set_property PACKAGE_PIN R53 [get_ports {gt_rx_p[7]}]

#GTREFCLK0
set_property PACKAGE_PIN Y42 [get_ports {GTREFCLK_IN_P[1]}]
create_clock -period 6.400 -name qsfp_clock2 [get_ports {GTREFCLK_IN_P[1]}]

