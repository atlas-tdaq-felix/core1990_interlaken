# Export path to quartus
export PATH=$PATH:/opt/intelFPGA_pro/23.4/quartus/bin

scriptdir=$(pwd)

# Checks whether projects folder exists, otherwise make the folder
if [ ! -d ../../S10_interlaken ]; then
    mkdir ../../S10_interlaken
fi

# Navigate to project folder
cd ../../S10_interlaken

# Generate project
quartus_sh -t ../scripts/S10_import_quartus.tcl -project top -revision first

# Open project in background and disown from terminal
quartus intel_interlaken.qpf & disown

# Navigate back to original directory
cd $scriptdir
