#
#	File import script for the FELIX hdl project
# 
#
quit -sim
#project close
source ../helper/clear_filesets.tcl

set PROJECT_NAME CORE1990_INTERLAKEN_QUESTA_SingleLane

source ../filesets/core1990_interlaken_fileset.tcl
#source ../filesets/interlaken_oc_fileset.tcl

#Actually execute all the filesets
source ./external_editor.tcl
source ../helper/questa_import_generic.tcl

vsim -voptargs="+acc" work.interlaken_top_tb work.glbl -t 10fs -L unisim
project compileoutofdate

#Top entity
add wave -group Top -position insertpoint sim:/interlaken_top_tb/*

#group certain framing burst signals
add wave -group Top -position insertpoint  \
sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Framing_Burst/s_axis \
sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Framing_Burst/s_axis_tready \
sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Framing_Burst/CRC24_TX \


#Lane 0
add wave -group Framing_Burst_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Framing_Burst/*
add wave -group Framing_Meta_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Framing_Meta/*
add wave -group Scrambler_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Scrambling/*
add wave -group Encoder_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_TX/g_lanes(0)/lane_tx/Encoding/*

add wave -group Deframing_Burst_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_RX/g_lanes(0)/lane_rx/Deframing_Burst/*
add wave -group Deframing_Meta_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_RX/g_lanes(0)/lane_rx/Deframing_Meta/*
add wave -group Descrambler_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_RX/g_lanes(0)/lane_rx/Descrambler/*
add wave -group Decoder_0 -position insertpoint sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/Interlaken_RX/g_lanes(0)/lane_rx/Decoder/*

#When LOOPBACK = TRUE
#

#When LOOPBACK = FALSE
#add wave -group il0 -position insertpoint sim:/interlaken_top_tb/g_noloopback/il0/interlaken_instance/*

puts "INFO: Done!"

