#!/bin/tclsh
#
#	File import script for the CORE1990 INTERLAKEN hdl project
#
#
source ../helper/clear_filesets.tcl

set PROJECT_NAME INTERLAKEN_SIGASI

#Import blocks for different filesets
source ../filesets/interlaken_fileset.tcl

#Actually execute all the filesets
source ../helper/sigasi_import_generic.tcl

puts "INFO: Done!"





