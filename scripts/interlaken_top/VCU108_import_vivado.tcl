#
#  File import script for the FELIX hdl Vivado project
#  Board: VCU118
#

source ../helper/clear_filesets.tcl

set PROJECT_NAME VCU108_INTERLAKEN
set BOARD_TYPE 108
set TOPLEVEL interlaken_top

#Import blocks for different filesets
source ../filesets/core1990_interlaken_fileset.tcl

#Actually execute all the filesets
source ../helper/vivado_import_generic.tcl

puts "INFO: Done!"
