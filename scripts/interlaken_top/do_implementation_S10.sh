# Generate IP's
quartus_ipgenerate --dni ../../S10_interlaken/intel_interlaken -c intel_interlaken --run_default_mode_op 

# Synthesize design
quartus_syn --dni --read_settings_files=on --write_settings_files=off ../../S10_interlaken/intel_interlaken -c intel_interlaken 

# Place and route
quartus_fit --read_settings_files=on --write_settings_files=off ../../S10_interlaken/intel_interlaken -c intel_interlaken 

# Generate bitfile
quartus_asm --read_settings_files=on --write_settings_files=off ../../S10_interlaken/intel_interlaken -c intel_interlaken 

# Generate timing report
quartus_sta -t ../S10_generate_timing_report.tcl
