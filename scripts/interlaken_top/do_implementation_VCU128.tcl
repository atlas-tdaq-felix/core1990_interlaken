set scriptdir [pwd]
set projdir $scriptdir/../../
set outputdir $projdir/output/
set FILE_NAME "FLX128_INTERLAKEN"

set IMPL_RUN [get_runs impl_1]
set SYNTH_RUN [get_runs synth_1]

set lanes 4
set GTREFCLKS 1
set CARD_TYPE 128

set_property generic "
LANES=$lanes \
GTREFCLKS=$GTREFCLKS \
CARD_TYPE=$CARD_TYPE \
" [current_fileset]

set CORES 16
puts "INFO: $CORES cores are in used"

launch_runs $SYNTH_RUN  -jobs $CORES
wait_on_run $SYNTH_RUN

launch_runs $IMPL_RUN -jobs $CORES
#launch_runs $IMPL_RUN  -to_step write_bitstream
#cd $HDLDIR/Synt/
wait_on_run $IMPL_RUN

open_run $IMPL_RUN
current_run $IMPL_RUN

file mkdir $projdir/output/

write_bitstream -force $outputdir/${FILE_NAME}.bit
write_debug_probes $outputdir/${FILE_NAME}.ltx -force

#Get timing, util and power results
set WNS [get_property STATS.WNS [current_run]]
set WHS [get_property STATS.WHS [current_run]]
set GitSubmoduleStatus [exec git submodule status]

set pass [expr {[get_property SLACK [get_timing_paths -delay_type min_max]] >= 0}]
set report [report_timing -slack_lesser_than 0 -return_string -nworst 10]
set check_timing_report [check_timing -return_string]
report_utilization -name ${FILE_NAME} -spreadsheet_table "Hierarchy" -spreadsheet_file $outputdir/${FILE_NAME}.xlsx -spreadsheet_depth 8 
set util [report_utilization -return_string]
set power [report_power -return_string]

#Write results to file
set GenericFileData ""
set GenericFileData "$GenericFileData

Git submodule status:
$GitSubmoduleStatus


LANES:                          $lanes 
GTREFCLKS:                      $GTREFCLKS
CARD_TYPE:                      $CARD_TYPE \n
Timing met:                     $pass
WNS:                            $WNS
WHS:                            $WHS\n\n
Utilization Report:\n
$util\n
Check Timing:\n
$check_timing_report\n\n
Timing Report:\n
$report\n
Power report:\n
$power\n"

set GenericsFileName "$outputdir/${FILE_NAME}_generics_timing.txt"
set GenericsFileId [open $GenericsFileName "w"]
puts -nonewline $GenericsFileId $GenericFileData
close $GenericsFileId

#WNS [get_property SLACK [get_timing_paths]]
#FF llength [get_cells -hier -filter {PRIMITIVE_GROUP == REGISTER}]
#BUILD_DATETIME:                 $build_date 
#COMMIT_DATETIME:                $COMMIT_DATETIME 
#GIT_HASH:                       $GIT_HASH 
#GIT_TAG:                        $GIT_TAG 
#GIT_COMMIT_NUMBER:              $GIT_COMMIT_NUMBER 
