#
#  File import script for the FELIX hdl Vivado project
#  Board: VCU118
#

source ../helper/clear_filesets.tcl

set PROJECT_NAME VCU118_INTERLAKEN
set BOARD_TYPE 118
set TOPLEVEL interlaken_top
set MEZZANINE false

#Import blocks for different filesets
source ../filesets/core1990_interlaken_fileset.tcl

#Actually execute all the filesets
source ../helper/vivado_import_generic.tcl

puts "INFO: Done!"
