# Copyright (C) 2021  Intel Corporation. All rights reserved.
# Your use of Intel Corporation's design tools, logic functions
# and other software and tools, and any partner logic
# functions, and any output files from any of the foregoing
# (including device programming or simulation files), and any
# associated documentation or information are expressly subject
# to the terms and conditions of the Intel Program License
# Subscription Agreement, the Intel Quartus Prime License Agreement,
# the Intel FPGA IP License Agreement, or other applicable license
# agreement, including, without limitation, that your use is for
# the sole purpose of programming logic devices manufactured by
# Intel and sold by Intel or its authorized distributors.  Please
# refer to the applicable agreement for further details, at
# https://fpgasoftware.intel.com/eula.

# Quartus Prime: Generate Tcl File for Project
# File: gen_project.tcl
# Generated on: Thu Jun 30 13:25:38 2022

# Load Quartus Prime Tcl Project package
package require ::quartus::project

set need_to_close_project 0
set make_assignments 1

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) "intel_interlaken"]} {
		puts "Project intel_interlaken is not open"
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists intel_interlaken]} {
		project_open -revision intel_interlaken intel_interlaken
	} else {
		project_new -revision intel_interlaken intel_interlaken
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
    set_global_assignment -name FAMILY "Stratix 10"
    set_global_assignment -name DEVICE 1SG280HU2F50E2VG
    set_global_assignment -name TOP_LEVEL_ENTITY intel_interlaken_top
    set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
    set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
    set_global_assignment -name DEVICE 1SG280HU2F50E2VG
    set_global_assignment -name FAMILY "Stratix 10"
    set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
    set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 4
    set_global_assignment -name EDA_SIMULATION_TOOL "Questa Intel FPGA (VHDL)"
    set_global_assignment -name EDA_TIME_SCALE "1 ps" -section_id eda_simulation
    set_global_assignment -name EDA_OUTPUT_DATA_FORMAT VHDL -section_id eda_simulation
    set_global_assignment -name BOARD default
    set_global_assignment -name PROJECT_OUTPUT_DIRECTORY ../projects/
    set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "AVST X16"
    set_global_assignment -name USE_PWRMGT_SCL SDM_IO14
    set_global_assignment -name USE_PWRMGT_SDA SDM_IO11
    set_global_assignment -name USE_CONF_DONE SDM_IO16
    set_global_assignment -name USE_INIT_DONE SDM_IO0
    set_global_assignment -name PWRMGT_SLAVE_DEVICE_TYPE LTM4677
    set_global_assignment -name PWRMGT_PAGE_COMMAND_ENABLE ON
    set_global_assignment -name DEVICE_INITIALIZATION_CLOCK OSC_CLK_1_125MHZ
    set_global_assignment -name MINIMUM_SEU_INTERVAL 301
    set_global_assignment -name PWRMGT_SLAVE_DEVICE0_ADDRESS 4F
    set_global_assignment -name POWER_APPLY_THERMAL_MARGIN ADDITIONAL
    set_global_assignment -name PRESERVE_UNUSED_XCVR_CHANNEL ON
    set_global_assignment -name VHDL_INPUT_VERSION VHDL_2008

    set_global_assignment -name DEVICE_IO_STANDARD_ALL "1.8 V"
    set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
    set_global_assignment -name ACTIVE_SERIAL_CLOCK AS_FREQ_115MHZ_IOSC
    set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING ON
    set_global_assignment -name USE_CONFIGURATION_DEVICE OFF
    set_global_assignment -name GENERATE_PR_RBF_FILE ON
    set_global_assignment -name ENABLE_ED_CRC_CHECK ON
    set_global_assignment -name OPTIMIZATION_MODE BALANCED
    set_global_assignment -name GENERATE_RBF_FILE OFF

    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/Axis64Fifo_altera.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/axis_data_generator.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/axi_stream_package.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/intel_interlaken_MGT.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/intel_interlaken_top.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/interlaken_interface.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/interlaken_receiver_multiChannel.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/interlaken_transmitter_multiChannel.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/word_boundary_sync.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/IntelFPGA/interlaken_reset.vhd

    set_global_assignment -name VHDL_FILE ../sources/interlaken/crc/crc-32.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/crc/crc-24.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/receiver/deframing_meta.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/receiver/deframing_burst.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/receiver/interlaken_receiver.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transmitter/interlaken_transmitter.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transmitter/framing_meta.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transmitter/framing_burst.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/packages/interlaken_package.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/receiver/descrambler.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/receiver/decoder.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transmitter/encoder.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transmitter/scrambler.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transceiver/rxgearbox_64b67b.vhd
    set_global_assignment -name VHDL_FILE ../sources/interlaken/transceiver/txgearbox_64b67b.vhd


    set_global_assignment -name ENABLE_SIGNALTAP ON
    set_global_assignment -name USE_SIGNALTAP_FILE ../sources/ip_cores/Stratix10H/intel_interlaken.stp
    set_global_assignment -name SIGNALTAP_FILE ../sources/ip_cores/Stratix10H/intel_interlaken.stp

    set_global_assignment -name IP_FILE ../sources/ip_cores/Stratix10H/reset_release.ip
    set_global_assignment -name IP_FILE ../sources/ip_cores/Stratix10H/transceiver_phy.ip
    set_global_assignment -name IP_FILE ../sources/ip_cores/Stratix10H/transceiver_pll.ip
    set_global_assignment -name IP_FILE ../sources/ip_cores/Stratix10H/transceiver_reset.ip
    set_global_assignment -name SDC_FILE ../constraints/Stratix10H.sdc

    set_location_assignment PIN_Y38 -to refclk
    set_location_assignment PIN_AC43 -to rx_serial
    set_location_assignment PIN_AE47 -to tx_serial
    set_location_assignment PIN_BH33 -to clk_50

    set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to rx_serial
    set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to tx_serial
    set_instance_assignment -name XCVR_S10_REFCLK_TERM_TRISTATE TRISTATE_OFF -to refclk
    set_instance_assignment -name IO_STANDARD LVDS -to refclk



    set_location_assignment PIN_BD26 -to qsfp_lp_mode
    set_location_assignment PIN_BE27 -to qsfp_rst_n
    set_instance_assignment -name SLEW_RATE 1 -to qsfp_rst_n
    set_location_assignment PIN_BJ26 -to qsfp_scl
    set_location_assignment PIN_BF26 -to qsfp_mod_sel_n
    set_location_assignment PIN_BH27 -to qsfp_sda


    set_instance_assignment -name IO_STANDARD "1.8 V" -to qsfp_lp_mode
    set_instance_assignment -name IO_STANDARD "1.8 V" -to qsfp_rst_n
    set_instance_assignment -name IO_STANDARD "1.8 V" -to qsfp_mod_sel_n
    set_instance_assignment -name IO_STANDARD "1.8 V" -to qsfp_scl
    set_instance_assignment -name IO_STANDARD "1.8 V" -to qsfp_sda

    set_instance_assignment -name SLEW_RATE 1 -to qsfp_lp_mode
    set_instance_assignment -name SLEW_RATE 1 -to qsfp_mod_sel_n
    set_instance_assignment -name SLEW_RATE 1 -to qsfp_scl
    set_instance_assignment -name SLEW_RATE 1 -to qsfp_sda

    set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to qsfp_lp_mode
    set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to qsfp_rst_n
    set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to qsfp_mod_sel_n
    set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to qsfp_scl
    set_instance_assignment -name CURRENT_STRENGTH_NEW "MAXIMUM CURRENT" -to qsfp_sda


    set_location_assignment PIN_B20 -to user_pb_n[0]
    set_location_assignment PIN_A19 -to user_pb_n[1]
    set_location_assignment PIN_B17 -to user_pb_n[2]
    set_instance_assignment -name IO_STANDARD "1.8 V" -to user_pb_n[0]
    set_instance_assignment -name IO_STANDARD "1.8 V" -to user_pb_n[1]
    set_instance_assignment -name IO_STANDARD "1.8 V" -to user_pb_n[2]


}
