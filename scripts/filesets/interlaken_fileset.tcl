
set VHDL_FILES [concat $VHDL_FILES \
    interlaken/packages/interlaken_package.vhd \
    interlaken/axis_utils/axi_stream_package.vhd \
    interlaken/axis_utils/axis64Fifo.vhd \
    interlaken/crc/crc-24.vhd \
    interlaken/crc/crc-32.vhd \
    interlaken/receiver/decoder.vhd \
    interlaken/receiver/deframing_burst.vhd \
    interlaken/receiver/deframing_meta.vhd \
    interlaken/receiver/descrambler.vhd \
    interlaken/receiver/interlaken_receiver.vhd \
    interlaken/receiver/interlaken_receiver_channel.vhd \
    interlaken/transceiver/interlaken_gty.vhd \
    interlaken/transceiver/rxgearbox_64b67b.vhd \
    interlaken/transceiver/txgearbox_64b67b.vhd \
    interlaken/transceiver/transceiver_10g_64b67b_block_sync_sm.vhd \
    interlaken/transmitter/encoder.vhd \
    interlaken/transmitter/framing_burst.vhd \
    interlaken/transmitter/framing_meta.vhd \
    interlaken/transmitter/interlaken_transmitter.vhd \
    interlaken/transmitter/interlaken_transmitter_channel.vhd \
    interlaken/transmitter/scrambler.vhd \
    interlaken/interface/interlaken_interface.vhd \
    interlaken/interface/interlaken_top.vhd \
    interlaken/test/axis_data_generator.vhd]

set SIM_FILES [concat $SIM_FILES \
    interlaken_top_tb.vhd
]

set XDC_FILES_VC709 [concat $XDC_FILES_VC709 \
    pcie_dma_top_VC709.xdc \
    probes.xdc \
    Core1990_Constraints.xdc \
]  

set XDC_FILES_VMK180 [concat $XDC_FILES_VMK180 \
    interlaken_vmk180.xdc \
]

set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 \
    core1990_vcu128-es1.xdc \
]

set XCI_FILES [concat $XCI_FILES \
  interlaken_0.xci \
  gtwizard_ultrascale_0.xci \
]

