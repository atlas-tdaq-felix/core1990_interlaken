set VHDL_FILES [concat $VHDL_FILES \
    interlaken/axis_utils/axi_stream_package.vhd \
    interlaken/axis_utils/axis64Fifo.vhd \
    interlaken/transceiver/word_boundary_sync.vhd \
    interlaken/receiver/decoder.vhd \
    interlaken/receiver/deframing_burst.vhd \
    interlaken/receiver/descrambler.vhd \
    interlaken/receiver/deframing_meta.vhd \
    interlaken/receiver/interlaken_receiver_channel.vhd \
    interlaken/receiver/interlaken_receiver.vhd \
    interlaken/packages/interlaken_package.vhd \
    interlaken/transmitter/framing_meta.vhd \
    interlaken/transmitter/interlaken_transmitter_channel.vhd \
    interlaken/transmitter/interlaken_transmitter.vhd \
    interlaken/transmitter/encoder.vhd \
    interlaken/transmitter/scrambler.vhd \
    interlaken/transmitter/framing_burst.vhd \
    interlaken/interface/interlaken_interface.vhd \
    interlaken/interface/interlaken_reset.vhd \
    interlaken/test/axis_data_generator.vhd \
    interlaken/crc/crc-24.vhd \
    interlaken/crc/crc-32.vhd \
    interlaken/transceiver/txgearbox_64b67b.vhd \
    interlaken/transceiver/rxgearbox_64b67b.vhd \
    interlaken/transceiver/interlaken_gty.vhd \
    interlaken/interface/interlaken_statistics.vhd \
    interlaken/interface/gc_multichannel_frequency_meter.vhd \
    interlaken/interface/datarate_meter.vhd ]
    #interlaken/transceiver/transceiver_10g_64b67b_block_sync_sm.vhd \
    #interlaken/test/interlaken_top_tb.vhd \
    #interlaken/receiver/lane_to_chan.vhd \
    #interlaken/receiver/sync67to67fifo.vhd \
    #interlaken/transmitter/async66to264Fifo.vhd \
    
#set SIM_FILES [concat $SIM_FILES \
#    interlaken_top_tb.vhd ]

if {$MEZZANINE == true} {     
    set VHDL_FILES [concat $VHDL_FILES interlaken/interface/interlaken_top_mezzanine.vhd ]
} else {
	set VHDL_FILES [concat $VHDL_FILES interlaken/interface/interlaken_top.vhd ]
}

set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
    interlaken/transceiver/txgearbox_64b67b.vhd \
    interlaken/transceiver/rxgearbox_64b67b.vhd ]
    #interlaken/transceiver/transceiver_versal_interlaken_wrapper.vhd]
    
set SIM_FILES_VERSAL [concat $SIM_FILES_VERSAL \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd.vhd \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_bufg_gt_0_sim_netlist.vhdl \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_bufg_gt_1_0_sim_netlist.vhdl \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_gt_bridge_ip_0_0_sim_netlist.vhdl \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_gt_quad_base_0_sim_netlist.vhdl \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_urlp_0_sim_netlist.vhdl \
    ../sources/ip_cores/VMK180/gt_quad_bd/gt_quad_bd_util_ds_buf_0_sim_netlist.vhdl\
    ../sources/ip_cores/sim/versal_cips_bd.vhd]


set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
    ../VU37P/gtwizard_ultrascale_0.xci\
    ../VU37P/vio_0.xci \
    ../VU37P/vio_1.xci \
    ../VU37P/vio_5.xci \
    ../VU37P/vio_6.xci \
    ../VU37P/ila_0.xci \
    ../VU37P/ila_1.xci \
    ../VU37P/ila_2.xci ]
    
set XCI_FILES_VCU118 [concat $XCI_FILES_VCU118 \
    ../VCU118/gtwizard_ultrascale_0.xci\
    ../VCU118/vio_0.xci \
    ../VCU118/vio_1.xci \
    ../VCU118/vio_5.xci \
    ../VCU118/vio_6.xci \
    ../VCU118/ila_0.xci \
    ../VCU118/ila_1.xci \
    ../VCU118/ila_2.xci ]

set XDC_FILES_VCU118 [concat $XDC_FILES_VCU118 \
    core1990_vcu118.xdc \
]
if {$MEZZANINE == true} {     
    set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 core1990_vcu128-es1_mezzanine.xdc ]
} else {
    set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 core1990_vcu128-es1.xdc ]
}


#set XCI_FILES_VERSAL [concat $XCI_FILES_VERSAL \
#    ip_cores/VMK180/gt_quad_bd/gt_quad_bd_xlcp_0.xci \
#    ip_cores/VMK180/versal_cips_bd/versal_cips_bd_versal_cips_0_0.xci]    
set BD_FILES_BNL181 "" 
set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  transceiver_versal_interlaken.bd]

if {$MEZZANINE == true} {     
    set BD_FILES_VU37P [concat $BD_FILES_VU37P \
      mb_vcu128.bd]
}
  
set SIM_FILES [concat $SIM_FILES \
     ../simulation/UVVMtests/tb/interlaken_top_tb_uvvm.vhd]

# ]UVVMtests/tb/Loopback25G_tb.vhd]
#    ../sources/core1990_interlaken/sources/interlaken/test/Core1990_Test.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/interlaken150G_wrapper.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/interlaken_top_tb.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/axis_data_generator.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/pipeline.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/data_generator.vhd]
    
#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 
#set VHDL_FILES_KU [concat $VHDL_FILES_KU 
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P 
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P 
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL 



