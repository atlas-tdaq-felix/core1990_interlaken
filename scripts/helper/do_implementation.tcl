set IMPL_RUN [get_runs impl_1]
set SYNTH_RUN [get_runs synth_1]

reset_run $SYNTH_RUN

foreach design [get_designs] {
   puts "Closing design: $design"
   current_design $design
   close_design
}

set CORES 16
puts "INFO: $CORES cores are in used"

launch_runs $SYNTH_RUN  -jobs $CORES
wait_on_run $SYNTH_RUN

launch_runs $IMPL_RUN -jobs $CORES
wait_on_run $IMPL_RUN

open_run $IMPL_RUN
current_run $IMPL_RUN
