
source ../helper/clear_filesets.tcl

set MEZZANINE false
source ../filesets/core1990_interlaken_fileset.tcl

foreach v $VHDL_FILES {
    catch { exec python3 ../vhdl-style-guide/bin/vsg -f ../../sources/$v -c ../vsg-styles/interlaken.yaml} result
    puts "$result"
}
