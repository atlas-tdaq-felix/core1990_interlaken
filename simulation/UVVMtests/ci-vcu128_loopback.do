source ./Loopback25G_import_questa.tcl
vsim -voptargs=+acc work.interlaken_top_tb_uvvm work.glbl -t fs

add wave -position insertpoint -group interface sim:/interlaken_top_tb/uut/interface/*
add wave -position insertpoint -group generate_data sim:/interlaken_top_tb/uut/generate_data/*
add wave -position insertpoint -group interlaken_gty sim:/interlaken_top_tb/uut/interface/interlaken_gty/*
add wave -position insertpoint -group txlane sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/txlane/Interlaken_TX/*
add wave -position insertpoint -group rxlane sim:/interlaken_top_tb/uut/interface/g_unbonded_channels(0)/rxlane/Interlaken_RX/*
#add wave -position insertpoint -group versal_cips sim:/interlaken_top_tb/uut/g_vmk180/versal_cips_bd_inst/*
#add wave -position insertpoint -group quad0 sim:/interlaken_top_tb/uut/interface/Interlaken_gty/g_quads(0)/*

run -all
