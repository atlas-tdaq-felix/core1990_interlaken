#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Elena Zhivun
#               Frans Schreuder
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#questasim: Installation path and license server at Nikhef. For other institutes, please try another location.
#modelsim (free): follow ModelsimInstructions.txt
echo "free -h"
free -h
if [ -d work/ ]; then
    echo "Deleting work directory"
    rm -rf work/
fi

MODELSIMDIR=/opt/intelFPGA_pro/23.2/modelsim_ase/bin/ #your modelsim executable here
XILINX_SIMLIBDIR=~/xilinx_simlib/ #your xilinx_simlib location here

if [ -d /eda/fpga/mentor/questasim_2019.1/bin/ ]; then
    #Nikhef questasim installation path and license server.
    export PATH=/eda/fpga/mentor/questasim_2019.1/bin/:$PATH
    export LM_LICENSE_FILE=@lbmetapp001.nikhef.nl
elif [ -d /afs/cern.ch/work/f/fschreud/public/questasim_2019.1/ ]; then
    #Cern questasim installation path and license server
    export PATH=/afs/cern.ch/work/f/fschreud/public/questasim_2019.1/linux_x86_64/:$PATH
    export MGLS_LICENSE_FILE=1717@lxlicen01,1717@lxlicen02,1717@lxlicen03,1717@lnxmics1,1717@lxlicen08
elif [ -d $MODELSIMDIR ]; then
    export PATH=$MODELSIMDIR:$PATH
else
    echo "Could not find questasim installation path, exiting."
    exit 1
fi

if [ ! -d ../UVVM/uvvm_util/sim/ ]; then
  source ./compile-uvvm.sh $MODELSIMDIR
fi

if [ ! -d ../xilinx_simlib/ ]; then
  echo xilinx_simlib does not exist, copying from 
  if [ -d /project/et/vhdllibs/v2019.1/xilinx_lib ]; then
    echo /project/et/
    cp -r /project/et/vhdllibs/v2019.1/xilinx_lib ../xilinx_simlib
  elif [ -d /afs/cern.ch/work/f/fschreud/public/xilinx_simlib/ ]; then
    echo /afs/cern.ch/work/f/fschreud/public/
    cp -r /afs/cern.ch/work/f/fschreud/public/xilinx_simlib/ ../xilinx_simlib
  elif [ -d $XILINX_SIMLIBDIR ]; then
    echo $XILINX_SIMLIBDIR
    cp -r $XILINX_SIMLIBDIR ../
  else
    echo "Could not find xilinx libraries, exiting"
    exit 1
  fi
fi

if [ $# -eq 0 ]
then
    TESTS="\
        vcu128_loopback_uvvm"
    echo "No arguments supplied, running the following tests:
    $TESTS"
else
    TESTS=$1
    echo "Running test:
    $TESTS"
fi



for TEST in $TESTS 
do
  vsim -do ci-${TEST}.do
  SIMSTATUS=$?
  mv transcript transcript-${TEST}
  if grep -q "Simulation SUCCESS: No mismatch between counted and expected serious alerts" transcript-${TEST} && [ $SIMSTATUS == "0" ]
then
      echo ${TEST} simulation successful
else
      echo ${TEST} simulation ERROR
    exit 1
fi
done

