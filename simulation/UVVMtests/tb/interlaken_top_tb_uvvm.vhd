library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;
library xpm;
use xpm.vcomponents.all;
use std.env.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

-- INTERLAKEN_VCU128_tb
-- Test bench entity
entity interlaken_top_tb_uvvm is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end interlaken_top_tb_uvvm;

architecture tb of interlaken_top_tb_uvvm is
    constant lanes        : positive := 4;
    constant GTREFCLKS    : integer  := 1;
    
    signal freerun_clk_p      : std_logic:= '0';
    signal freerun_clk_n      : std_logic:= '0';
    signal freerunclk_en   : boolean   := false;
    signal GTREFCLK_IN_P : std_logic_vector(GTREFCLKS-1 downto 0);
    signal GTREFCLK_IN_N : std_logic_vector(GTREFCLKS-1 downto 0);
    signal gtrefclk        : std_logic:= '0';
    signal gtrefclk_en     : boolean   := true;

    constant    GTREFCLK_PERIOD          : time                :=  6.4 ns;  -- 156.25 MHz
    constant    freerun_PERIOD           : time                :=  10.0 ns; -- 100 MHz
    


    signal gt_rx_p, gt_rx_n : std_logic_vector(lanes-1 downto 0);
    signal gt_tx_p, gt_tx_n : std_logic_vector(lanes-1 downto 0);
    signal sys_rst : std_logic := '1';
    
      
    constant    BondNumberOfLanes       : positive := 1;
    
    signal TX_Out_P_s        : std_logic_vector(lanes-1  downto 0);
    signal TX_Out_N_s        : std_logic_vector(lanes-1  downto 0);
    signal RX_In_P_s         : std_logic_vector(lanes-1  downto 0);
    signal RX_In_N_s         : std_logic_vector(lanes-1  downto 0);
    signal clk100            : std_logic;

    signal m_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal m_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_tready     : axis_tready_array_type(0 to Lanes-1);
    signal s_axis            : axis_64_array_type(0 to Lanes-1);
    signal s_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal s_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal s_axis_tready     : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal s_axis_tready is never read"
    signal m_axis            : axis_64_array_type(0 to Lanes-1);        -- @suppress "signal m_axis is never read"
    signal m_axis_prog_empty : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal m_axis_prog_empty is never read"
    signal Decoder_lock      : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit-- @suppress "signal Decoder_lock is never read"
    signal HealthLane        : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal HealthLane is never read"
    signal HealthInterface   : std_logic_vector((Lanes/BondNumberOfLanes)-1 downto 0);
    signal Descrambler_lock  : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal Descrambler_lock is never read"

    signal decoder_error_sync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal decoder_error_sync is never read"
    signal descrambler_error_badsync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_badsync is never read"
    signal descrambler_error_statemismatch : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_statemismatch is never read"
    signal descrambler_error_nosync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_nosync is never read"
    signal burst_crc24_error : std_logic_vector(lanes-1 downto 0); -- @suppress "signal burst_crc24_error is never read" -- @suppress "signal meta_crc32_error is never read"
    signal meta_crc32_error  : std_logic_vector(lanes-1 downto 0); -- @suppress "signal meta_crc32_error is never read"

    signal tx_user_clk_out, rx_user_clk_out : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_burst, m_axis_deburst : axis_64_array_type(0 to Lanes - 1);
    
    signal latency_o : slv_16_array(lanes-1 downto 0);
    signal valid_o   : std_logic_vector(lanes-1 downto 0);
    
    signal count_rx_o : slv_32_array(lanes-1 downto 0);
    signal packet_num_rx_o : slv_32_array(lanes-1 downto 0);
    signal pkt_err_cnt_o : slv_16_array(lanes-1 downto 0);
    signal wrd_err_cnt_o : slv_16_array(lanes-1 downto 0); 

    signal axis_rst_ext, interlaken_rst_ext : std_logic;
    signal loopback : std_logic_vector(2 downto 0);
    signal axis_burst_mode : std_logic_vector(lanes-1 downto 0); -- configures burst mode. 0 = burstmax/short; 1 = burst config ignored, only axis tlast triggers eop/sop
    
    signal data_length_i : slv_array(0 to lanes-1)(15 downto 0);
    signal packet_length_i : slv_array(0 to lanes-1)(15 downto 0);
    
    signal rx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal tx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal tx_data_rate_value    : slv_array(0 to lanes-1)(63 downto 0);
    signal rx_data_rate_value    : slv_array(0 to lanes-1)(63 downto 0);

    signal crc24_error_count : slv_array(0 to lanes-1)(15 downto 0);
    signal crc32_error_count : slv_array(0 to lanes-1)(15 downto 0);
    signal error_truncation  : std_logic_vector(lanes-1 downto 0);
begin
    
    ------------------------------------------------
    -- Generating clock signals
    ------------------------------------------------
    clock_generator(freerun_clk_p, freerunclk_en, freerun_PERIOD, "100 MHz CLK");
    clock_generator(gtrefclk, gtrefclk_en, GTREFCLK_PERIOD, "156.25 MHz CLK");
    
    freerun_clk_n    <= not freerun_clk_p;
    GTREFCLK_IN_N(0) <= not gtrefclk;
    GTREFCLK_IN_P(0) <= gtrefclk;

    ------------------------------------------------
    -- Transceiver loopback
    ------------------------------------------------
    gt_rx_p <= gt_tx_p;
    gt_rx_n <= gt_tx_n;
    
    ------------------------------------------------
    -- Control signals - default values
    ------------------------------------------------
    loopback <= (others => '0');
    axis_rst_ext <= '0';
    interlaken_rst_ext <= '0';
    axis_burst_mode <= (others => '0');
    data_length_i <= (others => x"0008");
    packet_length_i <= (others => x"0010");
    ------------------------------------------------
    -- Interlaken logic
    -- This is a direct copy from interlaken_top and no edits should be made in this block
    ------------------------------------------------
    ---- 100MHz clock DS to SE
    IBUFDS_inst : IBUFDS -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE, IOSTANDARD"
        port map(
            O  => clk100,
            I  => freerun_clk_p,
            IB => freerun_clk_n
        );

    RX_In_N_s <= gt_rx_n;
    RX_In_P_s <= gt_rx_p;
    gt_tx_n   <= TX_Out_N_s;
    gt_tx_p   <= TX_Out_P_s;

    ------- The Interlaken Interface -------
    interface : entity work.interlaken_interface
    generic map(
         BurstMax     => 256, --(Bytes)
         BurstShort   => 64, --(Bytes)
         MetaFrameLength => 2048, --(Words)
         Lanes        => lanes,
         txlanes => 1,
         rxlanes => 1,
         CARD_TYPE => 128,
         GTREFCLKS => GTREFCLKS
    )
    port map(
            clk100 => clk100,
            reset  => sys_rst,
            axis_rst_ext => axis_rst_ext,
            interlaken_rst_ext => interlaken_rst_ext,            
            GTREFCLK_IN_P => GTREFCLK_IN_P,
            GTREFCLK_IN_N => GTREFCLK_IN_N,
            tx_user_clk_out => tx_user_clk_out, --only first lane for now
            rx_user_clk_out => rx_user_clk_out,
            TX_Out_P => TX_Out_P_s,
            TX_Out_N => TX_Out_N_s,
            RX_In_P  => RX_In_P_s,
            RX_In_N  => RX_In_N_s,
            TX_FlowControl => (others => (others => '0')),
            m_axis_burst  => m_axis_burst,
            m_axis_deburst  => m_axis_deburst,
            s_axis_aresetn => s_axis_aresetn,
            m_axis_aresetn => m_axis_aresetn,
            s_axis        => s_axis,
            s_axis_aclk   => s_axis_aclk, --tx_user_clk_out 
            s_axis_tready => s_axis_tready,
            FlowControl => open,
            m_axis_aclk   => m_axis_aclk, --rx_user_clk_out
            m_axis        => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            rx_usr_clk_freq => rx_usr_clk_freq,
            tx_usr_clk_freq => tx_usr_clk_freq,
            tx_data_rate_value => tx_data_rate_value,
            rx_data_rate_value => rx_data_rate_value,
            Decoder_Lock     => Decoder_lock,
            Descrambler_lock => Descrambler_lock,
            --Channel => Channel,
            loopback_in                     => loopback,
            axis_burst_mode                 => axis_burst_mode,
            HealthLane                      => HealthLane,
            HealthInterface                 => HealthInterface,
            decoder_error_sync              => decoder_error_sync,
            descrambler_error_badsync       => descrambler_error_badsync,
            descrambler_error_statemismatch => descrambler_error_statemismatch,
            descrambler_error_nosync        => descrambler_error_nosync,
            burst_crc24_error               => burst_crc24_error,
            meta_crc32_error                => meta_crc32_error,
            crc24_error_count               => crc24_error_count,
            crc32_error_count               => crc32_error_count,
            error_truncation                => error_truncation
        );
        
    s_axis_aclk <= tx_user_clk_out; 
    m_axis_aclk <= rx_user_clk_out; 
        
    ---- Generates input data and interface signals ----
    generate_data : entity work.axis_data_generator
    generic map (
        lanes => lanes
    )
    port map (
        s_axis_aclk => s_axis_aclk,
        m_axis_aclk => m_axis_aclk,
        s_axis_aresetn => s_axis_aresetn,
        m_axis_aresetn => m_axis_aresetn,
      
        s_axis => s_axis,
        s_axis_tready => s_axis_tready,

        m_axis            => m_axis,
        m_axis_tready     => m_axis_tready,
        m_axis_prog_empty => m_axis_prog_empty,
        
        data_length_i => data_length_i,
        packet_length_i => packet_length_i,
        
        latency_o  => latency_o,--latency_o,
        valid_o    => valid_o,
        count_rx_o => count_rx_o,  
        packet_num_rx_o => packet_num_rx_o,
        pkt_err_cnt_o  => pkt_err_cnt_o,
        wrd_err_cnt_o  => wrd_err_cnt_o,
        
        HealthLane  => HealthLane,
        HealthInterface => HealthInterface(0)
    );
    
        
    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
         
    begin
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        log(ID_LOG_HDR, "Simulation of TB for 25G Interlaken on the VCU128", C_SCOPE);
        log(ID_LOG_HDR, "Design consists of " & integer'image(lanes) & " lanes and " & integer'image(GTREFCLKS) & " GTREFCLKS", C_SCOPE);
        
        -- Enable clock and initial reset
        freerunclk_en<= true;
        sys_rst <=  '1';
        wait for 300 ns;

        -- Start simulation
        log(ID_LOG_HDR, "Starting simulation", C_SCOPE);
        sys_rst <=  '0';

        log(ID_LOG_HDR, "Stopping simulation", C_SCOPE);


        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        await_value(valid_o, "1111", 0 ns, 20 ms, "waiting for valid_o to become true");
        await_stable(valid_o, 100 us, FROM_LAST_EVENT, 1 ms, FROM_NOW, ERROR, "Waiting for valid_o to stabilize");
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        
        -- VUnit requires another way to finish simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';

        wait;  -- to stop completely
    end process p_main;
    
    
end tb;


