source ../../scripts/helper/clear_filesets.tcl

set SIM_ARCH "VCU128"

set XIL_PROJECTS "VCU128_INTERLAKEN"
set PROJECT_NAME VCU128_INTERLAKEN_LOOPBACK
set MEZZANINE false

source ../../scripts/filesets/core1990_interlaken_fileset.tcl
source external_editor.tcl

source ../../scripts/helper/questa_import_generic.tcl

