
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library lib;
--

library vunit_lib;
    context vunit_lib.vunit_context;

-- Test case entity
entity vcu128_loopback_vunit_tb is
    generic (
        runner_cfg : string
    );
end entity;

architecture func of vcu128_loopback_vunit_tb is
    signal uvvm_completed : std_logic;
    constant C_CLK_PERIOD_250 : time := 4 ns;
begin

    uut_tb: entity work.interlaken_top_tb_uvvm
        generic map(
            use_vunit => true
        )
        port map(
            uvvm_completed => uvvm_completed
        );


    vunit_main : process is
    begin
        report "VUnit initializing";
        test_runner_setup(runner, runner_cfg);
        report "VUnit initialized";
        while uvvm_completed /= '1' loop
            wait for C_CLK_PERIOD_250;
        end loop;
        report "VUnit UVVM done, cleaning up";
        test_runner_cleanup(runner);

    end process vunit_main;

end architecture;


