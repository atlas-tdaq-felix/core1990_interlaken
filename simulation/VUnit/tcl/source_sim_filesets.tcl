#proc sim_filesets {} {

#TEMP, should be placed somewhere else
set MEZZANINE false

source $PROJECT_ROOT/scripts/helper/clear_filesets.tcl
source $PROJECT_ROOT/scripts/filesets/core1990_interlaken_fileset.tcl

set SIM_ARCH "VU37P"
if { $SIM_ARCH == "V7" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_V7]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_V7]
} elseif { $SIM_ARCH == "VU37P" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_VU37P]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU37P]
} elseif { $SIM_ARCH == "VERSAL" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSAL]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
}

set SOURCE_FILES ""
foreach VHDL_FILE $VHDL_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/${VHDL_FILE}"]
}

foreach VERILOG_FILE $VERILOG_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/${VERILOG_FILE}"]
}

foreach SIM_FILE $SIM_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/simulation/${SIM_FILE}"]
}

foreach XCI_FILE $XCI_FILES {
    set XCI_FILE [string trimright $XCI_FILE i]
    set XCI_FILE [string trimright $XCI_FILE c]
    set XCI_FILE [string trimright $XCI_FILE x]
    set XCI_FILE [string trimright $XCI_FILE .]
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/ip_cores/sim/${XCI_FILE}_sim_netlist.vhdl"]
}

