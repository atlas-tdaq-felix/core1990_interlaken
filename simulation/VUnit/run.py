#!/usr/bin/python3
from pathlib import Path
from vunit import VUnit
from vunit.sim_if.factory import SIMULATOR_FACTORY
from vivado_util import compile_standard_libraries
import tkinter
import subprocess


import os


ROOT = (Path(__file__).parent / "../..").resolve()

VU = VUnit.from_argv()
#VU.set_sim_option('modelsim.vsim_flags', ['-suppress 8684'])


simulator = SIMULATOR_FACTORY.select_simulator().name

# Create library 'lib'
lib = VU.add_library("lib")

XILINX_VIVADO = os.getenv('XILINX_VIVADO')
GHDL_PATH = os.getenv('GHDL')

if GHDL_PATH == None:
    GHDL_PATH = "/usr/local"
    
print(simulator)

if simulator == "modelsim":
    #Compile UVVM library with GHDL if it doesn't exist yet
    if not os.path.exists(ROOT / "simulation/UVVM/uvvm_util/sim/uvvm_util/"):
        print("UVVM library not found, compiling")
        subprocess.call([str(ROOT)+'/simulation/UVVMtests/compile-uvvm.sh'])
    bitvis_vip_axistream       = VU.add_external_library("bitvis_vip_axistream"        , ROOT / "simulation/UVVM/bitvis_vip_axistream/sim/bitvis_vip_axistream/")
    bitvis_vip_clock_generator = VU.add_external_library("bitvis_vip_clock_generator"  , ROOT / "simulation/UVVM/bitvis_vip_clock_generator/sim/bitvis_vip_clock_generator/")
    bitvis_vip_error_injection = VU.add_external_library("bitvis_vip_error_injection"  , ROOT / "simulation/UVVM/bitvis_vip_error_injection/sim/bitvis_vip_error_injection/")
    bitvis_vip_i2c             = VU.add_external_library("bitvis_vip_i2c"              , ROOT / "simulation/UVVM/bitvis_vip_i2c/sim/bitvis_vip_i2c/")
    bitvis_vip_sbi             = VU.add_external_library("bitvis_vip_sbi"              , ROOT / "simulation/UVVM/bitvis_vip_sbi/sim/bitvis_vip_sbi/")
    bitvis_vip_scoreboard      = VU.add_external_library("bitvis_vip_scoreboard"       , ROOT / "simulation/UVVM/bitvis_vip_scoreboard/sim/bitvis_vip_scoreboard/")
    bitvis_vip_wishbone        = VU.add_external_library("bitvis_vip_wishbone"         , ROOT / "simulation/UVVM/bitvis_vip_wishbone/sim/bitvis_vip_wishbone/")
    bitvis_vip_gpio            = VU.add_external_library("bitvis_vip_gpio"             , ROOT / "simulation/UVVM/bitvis_vip_gpio/sim/bitvis_vip_gpio/")
    uvvm_util                  = VU.add_external_library("uvvm_util"                   , ROOT / "simulation/UVVM/uvvm_util/sim/uvvm_util/")
    uvvm_vvc_framework         = VU.add_external_library("uvvm_vvc_framework"          , ROOT / "simulation/UVVM/uvvm_vvc_framework/sim/uvvm_vvc_framework/")
    
    lib.add_source_files(XILINX_VIVADO + "/data/verilog/src/glbl.v")
    
    #To save some time, at Nikhef the precompiled libraries can be found on /project/et/vhdllibs/v2019.1, otherwise compile them.
    if not os.path.exists(ROOT / "simulation/VUnit/vivado_libs") and os.path.exists("/project/et/vhdllibs/v2019.1/vivado_libs-2019.1.tar.gz"):
        subprocess.call(['tar','-zxvf','/project/et/vhdllibs/v2019.1/vivado_libs-2019.1.tar.gz', '-C', str(ROOT/'simulation/VUnit')])
    compile_standard_libraries(
        VU,
        output_path=ROOT / "simulation/VUnit/vivado_libs"
    )

elif simulator == "ghdl":
    #Compile UVVM library with GHDL if it doesn't exist yet
    if not os.path.exists(ROOT / "simulation/VUnit/uvvm_util/v08/"):
        print("UVVM library not found, compiling")
        subprocess.call([GHDL_PATH+'/lib/ghdl/vendors/compile-uvvm.sh','-a','--source','../UVVM/'])
    #Compile Xilinx libraries with GHDL if they don't exist yet
    if not os.path.exists(ROOT / "simulation/VUnit/xilinx-vivado/unisim/v08/"):
        print("Vivado libraries not compiled, compiling...")
        subprocess.call([GHDL_PATH+'/lib/ghdl/vendors/compile-xilinx-vivado.sh', '-a', '--vhdl2008', '--source', XILINX_VIVADO+'/data/vhdl/src'])

    bitvis_vip_axistream       = VU.add_external_library("bitvis_vip_axistream"        , ROOT / "simulation/VUnit/bitvis_vip_axistream/v08/")
    bitvis_vip_clock_generator = VU.add_external_library("bitvis_vip_clock_generator"  , ROOT / "simulation/VUnit/bitvis_vip_clock_generator/v08/")
    bitvis_vip_error_injection = VU.add_external_library("bitvis_vip_error_injection"  , ROOT / "simulation/VUnit/bitvis_vip_error_injection/v08/")
    bitvis_vip_i2c             = VU.add_external_library("bitvis_vip_i2c"              , ROOT / "simulation/VUnit/bitvis_vip_i2c/v08/")
    bitvis_vip_sbi             = VU.add_external_library("bitvis_vip_sbi"              , ROOT / "simulation/VUnit/bitvis_vip_sbi/v08/")
    bitvis_vip_scoreboard      = VU.add_external_library("bitvis_vip_scoreboard"       , ROOT / "simulation/VUnit/bitvis_vip_scoreboard/v08/")
    bitvis_vip_wishbone        = VU.add_external_library("bitvis_vip_wishbone"         , ROOT / "simulation/VUnit/bitvis_vip_wishbone/v08/")
    bitvis_vip_gpio            = VU.add_external_library("bitvis_vip_gpio"             , ROOT / "simulation/VUnit/bitvis_vip_gpio/v08/")
    uvvm_util                  = VU.add_external_library("uvvm_util"                   , ROOT / "simulation/VUnit/uvvm_util/v08/")
    uvvm_vvc_framework         = VU.add_external_library("uvvm_vvc_framework"          , ROOT / "simulation/VUnit/uvvm_vvc_framework/v08/")
    
    #secureip = VU.add_external_library("secureip"  , ROOT / "simulation/VUnit/xilinx-vivado/secureip/v08/")
    unifast  = VU.add_external_library("unifast"   , ROOT / "simulation/VUnit/xilinx-vivado/unifast/v08/")
    unimacro = VU.add_external_library("unimacro"  , ROOT / "simulation/VUnit/xilinx-vivado/unimacro/v08/")
    secureip = VU.add_library("secureip")
    secureip.add_source_files(ROOT / 'simulation/VUnit/secureip_ghdl/*.vhd')
    
    unisim = VU.add_library("unisim")
    unisim.add_source_files(ROOT / 'simulation/VUnit/secureip_ghdl/*.vhd')
    unisim.add_source_files(XILINX_VIVADO+'/data/vhdl/src/unisims/unisim_VCOMP.vhd')
    unisim.add_source_files(XILINX_VIVADO+'/data/vhdl/src/unisims/unisim_VPKG.vhd')
    unisim.add_source_files(XILINX_VIVADO+'/data/vhdl/src/unisims/primitive/*.vhd')
    #unisim   = VU.add_external_library("unisim"    , ROOT / "simulation/VUnit/xilinx-vivado/unisim/v08/")
    
    
    
    xpm = VU.add_library("xpm")
    xpm.add_source_files(ROOT / 'simulation/VUnit/xpm_vhdl/src/xpm/xpm_memory/hdl/*.vhd')
    xpm.add_source_files(ROOT / 'simulation/VUnit/xpm_vhdl/src/xpm/xpm_cdc/hdl/*.vhd')
    xpm.add_source_files(ROOT / 'simulation/VUnit/xpm_vhdl/src/xpm/xpm_fifo/hdl/*.vhd')
    xpm.add_source_files(ROOT / 'simulation/VUnit/xpm_vhdl/src/xpm/xpm_fifo/simulation/*.vhd')
    xpm.add_source_files(ROOT / 'simulation/VUnit/xpm_vhdl/src/xpm/xpm_VCOMP.vhd')
else:
    print(simulator+" unsupported")
    quit()
    




# Class to access TCL variables
# From https://wiki.tcl-lang.org/page/Python-Tcl-Interactions

# Create library 'lib'
class TclVars(object):
    def __init__(self, tcl):
        self.tcl = tcl

    def __setattr__(self, name, value):
        if name == 'tcl':
            object.__setattr__(self, name, value)
        else:
            self.tcl.call('set', '::' + name, value)

    def __getattr__(self, name):
        if name == 'tcl':
            return object.__getattr__(self, name)
        else:
            return self.tcl.call('set', '::' + name)



tcl_intrpr = tkinter.Tcl()
tcl_vars = TclVars(tcl_intrpr)


#r=tkinter.Tk()
tcl_vars.PROJECT_ROOT = str(ROOT).replace('\\', '/')
tcl_intrpr.eval('source ' + str(ROOT / 'simulation/VUnit/tcl/source_sim_filesets.tcl').replace('\\', '/'))
INTERLAKEN_SOURCE_FILES = tcl_vars.SOURCE_FILES

print(INTERLAKEN_SOURCE_FILES)

for FILE in INTERLAKEN_SOURCE_FILES.split():
    lib.add_source_files(ROOT / FILE)
    
lib.add_source_files(ROOT / "simulation/VUnit/tb/*.vhd")

if simulator == "modelsim":
    VU.set_sim_option("modelsim.vsim_flags", ['-t fs', '-voptargs=+acc', 'lib.glbl'])
    lib.set_sim_option("disable_ieee_warnings", True)
if simulator == "ghdl":
    secureip.set_compile_option("ghdl.a_flags", ["-frelaxed-rules", "-fexplicit", "-fsynopsys"])
    unisim.set_compile_option("ghdl.a_flags", ["-frelaxed-rules", "-fexplicit", "-fsynopsys"])
    lib.set_compile_option("ghdl.a_flags", ["-frelaxed-rules", "-fexplicit", "-fsynopsys"])
    lib.set_sim_option("disable_ieee_warnings", True)
    lib.set_sim_option("ghdl.elab_flags", ["-frelaxed-rules", "-fsynopsys", "-Wbinding"])
    lib.set_sim_option("ghdl.sim_flags", ["--max-stack-alloc=0"])
    xpm.set_sim_option("ghdl.sim_flags", ["--max-stack-alloc=0"])

#
## Run vunit function
VU.main()

