----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/01/2021 04:27:39 PM
-- Design Name: 
-- Module Name: interlaken_top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity interlaken_top_tb is
end interlaken_top_tb;

architecture Behavioral of interlaken_top_tb is
    constant    QSFP4_PERIOD           : time                :=  6.4 ns;  -- 156.25 MHz
    constant    QDR4_PERIOD            : time                :=  10.0 ns; -- 100 MHz
    constant    Lanes                   : integer              :=  4;
    
    signal qsfp4_clock_p, qsfp4_clock_n : std_logic;
    signal qdr4_clk_p, qdr4_clk_n : std_logic;
    signal gt_rx_p, gt_rx_n : std_logic_vector(lanes-1 downto 0);
    signal gt_tx_p, gt_tx_n : std_logic_vector(lanes-1 downto 0);
    signal sys_rst : std_logic;
begin
    
    gt_rx_p <= gt_tx_p;
    gt_rx_n <= gt_tx_n;

    uut : entity work.interlaken_top
        port map(
            qdr4_clk_p => qdr4_clk_p,
            qdr4_clk_n => qdr4_clk_n,
    
            -- GTY 156,25 MHz clock 
            qsfp4_clock_p => qsfp4_clock_p,
            qsfp4_clock_n => qsfp4_clock_n,
            sys_rst => sys_rst,

            -- QSFP4 data signals
            gt_rx_n => gt_rx_n,
            gt_rx_p => gt_rx_p,
            gt_tx_n => gt_tx_n,
            gt_tx_p => gt_tx_p
        );
    
    process
    begin
        sys_rst <=  '1';
        wait for QDR4_PERIOD * 30;
        sys_rst <=  '0';
        wait;
    end process;
        
    process
    begin
        qsfp4_clock_n  <=  '1';
        qsfp4_clock_p  <=  '0';
        wait for QSFP4_PERIOD/2;
        qsfp4_clock_n  <=  '0';
        qsfp4_clock_p  <=  '1';
        wait for QSFP4_PERIOD/2;
    end process;
    
    process
    begin
        qdr4_clk_n  <=  '1';
        qdr4_clk_p  <=  '0';
        wait for QDR4_PERIOD/2;
        qdr4_clk_n  <=  '0';
        qdr4_clk_p  <=  '1';
        wait for QDR4_PERIOD/2;
    end process;

end Behavioral;
