--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandra Camplani
--!               Frans Schreuder
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
   -- use work.pcie_package.all;

package FELIX_package is

    type array_69b is array (natural range <>) of std_logic_vector(68 downto 0);
    type array_66b is array (natural range <>) of std_logic_vector(65 downto 0);
    type array_65b is array (natural range <>) of std_logic_vector(64 downto 0);
    type array_64b is array (natural range <>) of std_logic_vector(63 downto 0);
    type array_36b is array (natural range <>) of std_logic_vector(35 downto 0);
    type array_32b is array (natural range <>) of std_logic_vector(31 downto 0);
    type array_34b is array (natural range <>) of std_logic_vector(33 downto 0);
    type array_11b is array (natural range <>) of std_logic_vector(10 downto 0);
    type array_8b  is array (natural range <>) of std_logic_vector(7 downto 0);
    type array_2d_5b is array (natural range <>, natural range <>) of std_logic_vector(4 downto 0);
    type array_5b  is array (natural range <>) of std_logic_vector(4 downto 0);
    type array_4b  is array (natural range <>) of std_logic_vector(3 downto 0);
    type array_3b  is array (natural range <>) of std_logic_vector(2 downto 0);
    type array_2b  is array (natural range <>) of std_logic_vector(1 downto 0);


    --Some types combined from GBT / lpgbt package, to avoid conflicts removed them from both.
    type txrx20b_type                 is array (natural range <>) of std_logic_vector(19 downto 0);
    type txrx40b_type                 is array (natural range <>) of std_logic_vector(39 downto 0);
    type txrx116b_type                 is array (natural range <>) of std_logic_vector(115 downto 0);
    type txrx2b_type                 is array (natural range <>) of std_logic_vector(1 downto 0);
    type txrx32b_type                         is array (natural range <>) of std_logic_vector(31 downto 0);
    type txrx230b_type                         is array (natural range <>) of std_logic_vector(229 downto 0);
    type txrx224b_type                         is array (natural range <>) of std_logic_vector(223 downto 0);
    type txrx10b_type                         is array (natural range <>) of std_logic_vector(9 downto 0);
    type txrx120b_type           is array (natural range <>) of std_logic_vector(119 downto 0);
    type txrx116b_24ch_type         is array (23 downto 0) of std_logic_vector(115 downto 0);
    type txrx116b_12ch_type         is array (11 downto 0) of std_logic_vector(115 downto 0);

    --MT added
    type txrx256b_type                         is array (natural range <>) of std_logic_vector(255 downto 0);
    type txrx36b_type                         is array (natural range <>) of std_logic_vector(35 downto 0);
    --RL
    type txrx228b_type                         is array (natural range <>) of std_logic_vector(227 downto 0);
    --

    -- GTH PLL selection
    -- When using GREFCLK, QPLL should be used
    -- use CPLL for VC-709 and BNL-711
    -- use QPLL for HTG-710 (cannot use dedicated clock pin) <- this has WORSE jitter performance
    constant CPLL                                         : std_logic := '0';
    constant QPLL                                         : std_logic := '1';

    constant FIRMWARE_MODE_GBT         : integer := 0;-- 0: GBT mode
    constant FIRMWARE_MODE_FULL        : integer := 1;-- 1: FULL mode
    constant FIRMWARE_MODE_LTDB        : integer := 2;-- 2: LTDB mode (GBT mode with only IC/EC/Aux)
    constant FIRMWARE_MODE_FEI4        : integer := 3;-- 3: FEI4 / RD53A
    constant FIRMWARE_MODE_PIXEL       : integer := 4;-- 4: ITK Pixel (RD53B)
    constant FIRMWARE_MODE_STRIP       : integer := 5;-- 5: ITK Strip
    constant FIRMWARE_MODE_FELIG_GBT   : integer := 6;-- 6: FELIG_GBT
    constant FIRMWARE_MODE_FMEMU       : integer := 7;-- 7: FULL mode emulator
    constant FIRMWARE_MODE_MROD        : integer := 8; --8: FELIX mrod (2Gb/s S-links) mode.
    constant FIRMWARE_MODE_LPGBT       : integer := 9; --9: LPGBT mode
    constant FIRMWARE_MODE_INTERLAKEN  : integer := 10; --10: 25Gb/s interlaken links
    constant FIRMWARE_MODE_FELIG_LPGBT : integer := 11;-- 11: FELIG_LPGBT
    constant FIRMWARE_MODE_HGTD_LUMI   : integer := 12; --12: HGTD ALTIROC Lumi
    constant FIRMWARE_MODE_BCM_PRIME   : integer := 13; --13: BCM' readout
    constant FIRMWARE_MODE_FELIG_PIXEL : integer := 14; --14: FELIG pixel / 64b66b
    constant FIRMWARE_MODE_FELIG_STRIP : integer := 15; --15: FELIG strip

    type IntArray is array (natural range<>) of integer;
    constant MAX_GROUPS_PER_STREAM_FROMHOST : integer := 8;
    type IntArray2D is array (natural range<>) of IntArray(0 to MAX_GROUPS_PER_STREAM_FROMHOST-1);
    type IntArray2D_24 is array (natural range<>) of IntArray(0 to 23);
    constant STREAMS_TOHOST_MODE : IntArray(0 to 11) :=
    (
      42, --GBT mode: 40 EPaths + IC + EC + TTCToHost + BusyXoff
      1,  --FULL mode: + TTCToHost + BusyXoff
      5,  --LTDB mode:
      42, --FEI4 (tbd)
      30,  --ITK Pixel 28 EPaths + IC + EC
      30,  --ITK Strip - IC/EC + 7 egroups * 4 channels (320 Mbps 8b10b)
      42,  --FELIG GBT
      42,  --FMEmu
      42,  --FELIX mrod
      30,   --LPGBT mode: 28 EPaths + IC + EC
      1,   -- Interlaken mode
      30   -- FELIG LPGBT
    );
    constant STREAMS_FROMHOST_MODE : IntArray2D(0 to 11) :=
    (
      (8,8,8,8,8,2,0,0), --GBT mode: 40 EPaths + IC + EC divided into 6 groups (5x8 + 1x2)
      (8,8,8,8,8,2,0,0), --FULL mode: 40 EPaths + IC + EC
      (8,8,8,8,8,2,0,0), --LTDB mode:  ??
      (8,8,8,8,8,2,0,0), -- FEI4: 40 EPaths + IC + EC
      (4,4,4,4,2,0,0,0), -- ITK Pixel: 16 EPATH = 4x4 + IC + EC.
      (5,5,5,5,2,0,0,0), -- ITK Strip: lpGBT mode + IC/EC
      (8,8,8,8,8,2,0,0), -- FELIG GBT
      (8,8,8,8,8,2,0,0), -- FMEmu
      (8,8,8,8,8,2,0,0), -- FELIX mrod
      (4,4,4,4,2,0,0,0), -- LPGBT mode  16 EPATH = 4x4 + IC + EC
      (1,0,0,0,0,0,0,0), -- Interlaken mode
      (4,4,4,4,2,0,0,0)  -- FELIG LPGBT
    );

    --Determines whether axis32/axis8 or axis64 will be used by CRToHost / CRFromHost for each link.
    constant LINK_CONFIG_MODE : IntArray2D_24(0 to 10) :=
    (
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --GBT mode: 40 EPaths + IC + EC divided into 6 groups (5x8 + 1x2)
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --FULL mode: 40 EPaths + IC + EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --LTDB mode:  ??
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FEI4: 40 EPaths + IC + EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- ITK Pixel: 16 EPATH = 4x4 + IC + EC.
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- ITK Strip: lpGBT mode + IC/EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIG
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FMEmu
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIX mrod
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- LPGBT mode  16 EPATH = 4x4 + IC + EC
      (1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1)  -- Interlaken mode
    );


    --sum up all numbers in an IntArray
    function sum(constant x : IntArray) return integer;

    --To go from nenory depth to number of address bits
    function f_log2 (constant x : positive) return natural;
    function div_ceil(a : natural; b : positive) return natural;

    type TTC_data_type is record
        --***Phase2***
        -- Header
        MT                  : std_logic;
        PT                  : std_logic;
        Partition           : std_logic_vector(1 downto 0);
        BCID                : std_logic_vector(11 downto 0);
        SyncUserData        : std_logic_vector(15 downto 0);
        SyncGlobalData      : std_logic_vector(15 downto 0);
        TS                  : std_logic;
        ErrorFlags          : std_logic_vector(3 downto 0);

        -- TTC Message
        SL0ID               : std_logic;
        SOrb                : std_logic;
        Sync                : std_logic;
        GRst                : std_logic;
        L0A                 : std_logic;
        L0ID                : std_logic_vector(37 downto 0);
        OrbitID             : std_logic_vector(31 downto 0);
        TriggerType         : std_logic_vector(15 downto 0);
        LBID                : std_logic_vector(15 downto 0);

        --User message
        AsyncUserData       : std_logic_vector(63 downto 0);

        --! FS: No need to distribute LTI trailer info through the firmware
        -- Trailer
        --CRC                 : std_logic_vector(15 downto 0);
        --D16_2               : std_logic_vector(7 downto 0);
        --Comma               : std_logic_vector(7 downto 0);
        LTI_decoder_aligned   : std_logic;
        LTI_CRC_valid         : std_logic;

        --***Phase1***
        -- TTC Message
        L1A                 : std_logic; --bit 0
        Bchan               : std_logic; --bit 1
        BCR                 : std_logic; --bit 2
        ECR                 : std_logic; -- bit 3
        Brcst               : std_logic_vector(5 downto 0); --[7...2] bits 4-9
        Brcst_latched       : std_logic_vector(5 downto 0); -- [7..2] bits 10-15
        ExtendedTestPulse   : std_logic; -- bit 16
        L1Id                : std_logic_vector(23 downto 0);
        ITk_sync            : std_logic;
        ITk_tag             : std_logic_vector(6 downto 0);
        ITk_trig            : std_logic_vector(3 downto 0);
    --L0A                 : std_logic; --For Phase I TTC: Delayed version of L1A

    end record;
    
    type TTC_data_array_type is array(natural range <>) of TTC_data_type;

    constant TTC_zero : TTC_data_type := (
                                           -- Header
                                           MT                  => '0',
                                           PT                  => '0',
                                           Partition           => (others => '0'),
                                           BCID                => (others => '0'),
                                           SyncUserData        => (others => '0'),
                                           SyncGlobalData      => (others => '0'),
                                           TS                  => '0',
                                           ErrorFlags          => (others => '0'),
                                           
                                           -- TTC Message
                                           SL0ID               => '0',
                                           SOrb                => '0',
                                           Sync                => '0',
                                           GRst                => '0',
                                           L0A                 => '0',
                                           L0ID                => (others => '0'),
                                           OrbitID             => (others => '0'),
                                           TriggerType         => (others => '0'),
                                           LBID                => (others => '0'),
                                           
                                           --User message
                                           AsyncUserData       => (others => '0'),
                                           
                                           -- Trailer
                                           --CRC                 => (others => '0'),
                                           --D16_2               => (others => '0'),
                                           --Comma               => (others => '0'),
                                           LTI_decoder_aligned   => '1',
                                           LTI_CRC_valid         => '1',
                                           
                                           L1A => '0',
                                           Bchan => '0',
                                           BCR => '0',
                                           ECR => '0',
                                           Brcst => (others => '0'),
                                           Brcst_latched => (others => '0'),
                                           ExtendedTestPulse => '0',
                                           L1Id => (others => '0'),
                                           ITk_sync => '0',
                                           ITk_tag => (others => '0'),
                                           ITk_trig => "0000"
                                         );

    type DDR_out_type is record
        act_n   :  STD_LOGIC_VECTOR ( 0 to 0 );
        adr     :  STD_LOGIC_VECTOR ( 16 downto 0 );
        ba      :  STD_LOGIC_VECTOR ( 1 downto 0 );
        bg      :  STD_LOGIC_VECTOR ( 1 downto 0 );
        ck_c    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        ck_t    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        cke     :  STD_LOGIC_VECTOR ( 1 downto 0 );
        cs_n    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        odt     :  STD_LOGIC_VECTOR ( 1 downto 0 );
        reset_n :  STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type DDR_out_array_type is array(natural range <>) of DDR_out_type;
    type DDR_inout_type is record
        dm_n  : STD_LOGIC_VECTOR ( 7 downto 0 );
        dq    : STD_LOGIC_VECTOR ( 63 downto 0 );
        dqs_c : STD_LOGIC_VECTOR ( 7 downto 0 );
        dqs_t : STD_LOGIC_VECTOR ( 7 downto 0 );
    end record;
    type DDR_inout_array_type is array(natural range <>) of DDR_inout_type;
    type DDR_in_type is record
        sys_clk_n : STD_LOGIC_VECTOR ( 0 to 0 );
        sys_clk_p : STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type DDR_in_array_type is array(natural range <>) of DDR_in_type;

    --Calculate ToHost AXI Stream clock frequency based on FIRMWARE_MODE.
    function TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE: integer) return integer;

    --! Function to limit toplevel ports
    function NUM_BUSY_OUTPUTS(CARD_TYPE: integer) return integer;
    function NUM_NT_PORTSEL(CARD_TYPE: integer) return integer;
    function NUM_LMK(CARD_TYPE: integer) return integer;
    function NUM_PEX(CARD_TYPE: integer) return integer;
    function NUM_ADN(CARD_TYPE: integer) return integer;
    function NUM_TTC_INPUTS(CARD_TYPE: integer) return integer;
    function NUM_LTITTC_INPUTS(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer;
    function LMKFREQ(TTC_SYS_SEL: std_logic) return integer;
    function NUM_TB_TRIGGERS(CARD_TYPE: integer) return integer;
    function NUM_SI5324(CARD_TYPE: integer) return integer;
    function NUM_BPI_FLASH(CARD_TYPE: integer) return integer;
    function NUM_SI5345(CARD_TYPE: integer) return integer;
    function NUM_STN0_PORTCFG(CARD_TYPE: integer) return integer;
    function NUM_STN1_PORTCFG(CARD_TYPE: integer) return integer;
    function NUM_SMA(CARD_TYPE: integer) return integer;
    function NUM_TACH(CARD_TYPE: integer) return integer;
    function NUM_TESTMODE(CARD_TYPE: integer) return integer;
    function NUM_UPSTREAM_PORTSEL(CARD_TYPE: integer) return integer;
    function NUM_EMCCLK(CARD_TYPE: integer) return integer;
    function NUM_LEDS(CARD_TYPE: integer) return integer;
    function NUM_UC_RESET_N(CARD_TYPE: integer) return integer;
    function NUM_OPTO_LOS(CARD_TYPE: integer) return integer;
    function NUM_I2C_MUXES(CARD_TYPE: integer) return integer;
    function NUM_GTREFCLK1S(GTREFCLKS: integer;FIRMWARE_MODE: integer) return integer;
    function NUM_DDR(CARD_TYPE: integer) return integer;
    function NUM_FAN_FAIL(CARD_TYPE: integer) return integer;
    function NUM_FF3_PRSTN(CARD_TYPE: integer) return integer;
    function NUM_IOEXP(CARD_TYPE: integer) return integer;
    function NUM_PCIE_PWRBRK(CARD_TYPE: integer) return integer;
    function NUM_QSPI_RST(CARD_TYPE: integer) return integer;
    function NUM_FAN_PWM(CARD_TYPE: integer) return integer;


end FELIX_package;

package body FELIX_package is
    function sum(constant x : IntArray) return integer is
        variable xsum : integer;
    begin
        xsum := 0;
        for i in x'range loop
            xsum := xsum + x(i);
        end loop;
        return xsum;
    end function;

    function f_log2 (constant x : positive) return natural is
        variable i : natural;
    begin
        i := 0;
        while (2**i < x) and i < 31 loop
            i := i + 1;
        end loop;
        return i;
    end function;

    --! integer division; always round-up
    --! calculates: ceil(a / b)
    function div_ceil(a : natural; b : positive) return natural is
    begin
        return (a + (b - 1)) / b;
    end function;

    --Calculate ToHost AXI Stream clock frequency based on FIRMWARE_MODE.
    function TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE: integer) return integer is
    begin
        case FIRMWARE_MODE is
            when FIRMWARE_MODE_GBT         => return 160;
            when FIRMWARE_MODE_FULL        => return 250;
            when FIRMWARE_MODE_LTDB        => return 160;
            when FIRMWARE_MODE_FEI4        => return 160;
            when FIRMWARE_MODE_PIXEL       => return 160;
            when FIRMWARE_MODE_STRIP       => return 240;
            when FIRMWARE_MODE_FELIG_GBT   => return 160;
            when FIRMWARE_MODE_FMEMU       => return 0;
            when FIRMWARE_MODE_MROD        => return 160;
            when FIRMWARE_MODE_LPGBT       => return 250;
            when FIRMWARE_MODE_INTERLAKEN  => return 160; --Using AXI-Stream 64b for Interlaken, this frequency is used for AUX E-Links.
            when FIRMWARE_MODE_FELIG_LPGBT => return 160;
            when FIRMWARE_MODE_HGTD_LUMI   => return 240;
            when FIRMWARE_MODE_BCM_PRIME   => return 240;
            when FIRMWARE_MODE_FELIG_PIXEL => return 160;
            when FIRMWARE_MODE_FELIG_STRIP => return 160;
            when others => report "Unknown FIRMWARE_MODE given, don't know how to calculate ToHost AXI Stream clock frequency" severity error;

        end case;

    end function;


    --! Function to limit toplevel ports
    function NUM_BUSY_OUTPUTS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_NT_PORTSEL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    --function NUM_MGMT_PORT_EN(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 3;
    --  else
    --      return 0;
    --  end if;
    --end function;

    function NUM_LMK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_PEX(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_ADN(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;


    function NUM_TTC_INPUTS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_LTITTC_INPUTS(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer is
    begin
        if (CARD_TYPE = 711 or CARD_TYPE = 712) and TTC_SYS_SEL = '1' then
            return 1;
        else
            return 0;
        end if;
    end function;

    function LMKFREQ(TTC_SYS_SEL: std_logic) return integer is
    begin
        if TTC_SYS_SEL = '1' then
            return 240;
        else
            return 320;
        end if;
    end function;

    function NUM_TB_TRIGGERS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_SI5324(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_BPI_FLASH(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    --function NUM_I2C_SMB_CFG(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 1;
    --  else
    --      return 0;
    --  end if;
    --end function;
    --
    --function NUM_PCIE_PERSTN_OUT(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 2;
    --  else
    --      return 0;
    --  end if;
    --end function;
    --
    --function NUM_SHPC_INT(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 1;
    --  else
    --      return 0;
    --  end if;
    --end function;

    function NUM_SI5345(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        elsif CARD_TYPE = 181 then
            return 4;
        elsif CARD_TYPE = 128 or CARD_TYPE = 182 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_STN0_PORTCFG(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_STN1_PORTCFG(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_SMA(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 4;
        elsif CARD_TYPE = 709 or CARD_TYPE = 181 then
            return 4;
        else
            return 0;
        end if;
    end function;


    function NUM_TACH(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 709 or CARD_TYPE = 181 or CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_TESTMODE(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    function NUM_UPSTREAM_PORTSEL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    function NUM_EMCCLK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 709 or CARD_TYPE = 710 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_LEDS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 then
            return 8;
        elsif CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 7;
        elsif CARD_TYPE = 800 or CARD_TYPE = 181 or CARD_TYPE = 182 then
            return 4;
        else
            return 8;
        end if;
    end function;

    function NUM_UC_RESET_N(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_OPTO_LOS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 181 then
            return 4;
        elsif CARD_TYPE = 710 then
            return 2;
        elsif CARD_TYPE = 182 then --For FLX182, the firefly modules are monitored through I2C GPIO
            return 0;
        else
            return 0;
        end if;
    end function;

    function NUM_I2C_MUXES(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 181 or CARD_TYPE = 128 then
            return 2;
        else
            return 1;
        end if;
    end function;

    --Return the number of secondary gtrefclks, for Interlaken we have the TTC/LTI transmitter which uses the primary one.
    function NUM_GTREFCLK1S(GTREFCLKS: integer;FIRMWARE_MODE: integer) return integer is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN then
            return GTREFCLKS;
        else
            return 0;
        end if;
    end function;

    function NUM_DDR(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then --For now only implement DDR controller for FLX182. FLX181 has 3 but we don't use them for now.
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_FAN_FAIL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_FF3_PRSTN(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_IOEXP(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_PCIE_PWRBRK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_QSPI_RST(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 3;
        else
            return 0;
        end if;
    end function;

    function NUM_FAN_PWM(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

end FELIX_package;
