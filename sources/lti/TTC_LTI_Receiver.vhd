--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!      Nayib Boukadida
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------

library IEEE, UNISIM;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use work.FELIX_package.all;

Library xpm;
    use xpm.vcomponents.all;

--library uvvm_util;
--    context uvvm_util.uvvm_util_context;

entity TTC_LTI_Receiver is
    generic(
        GBT_NUM : integer := 4
    );
    Port(
        TTCout                  : out TTC_data_array_type(0 to GBT_NUM-1);
        LTI_CRC_Err             : out std_logic_vector(0 to GBT_NUM-1);
        LTI_RX_Data_Transceiver : in array_32b(0 to GBT_NUM - 1);
        LTI_RX_RX_CharIsK       : in array_4b(0 to GBT_NUM - 1);
        LTI_RXUSRCLK_in         : in  std_logic_vector(GBT_NUM - 1 downto 0)
    );
end TTC_LTI_Receiver;

architecture Behavioral of TTC_LTI_Receiver is
//    COMPONENT ila_3
//    PORT (
//        clk : IN STD_LOGIC;
//        probe0 : IN STD_LOGIC_VECTOR(35 DOWNTO 0)
//    );
//    END COMPONENT  ;
begin

    g_lanes : for i in 0 to GBT_NUM-1 generate
        signal ttc_dec_counter           : integer range 0 to 5;
        signal TTC_frame                 : std_logic_vector(159 downto 0);
        signal TTC_decoded               : TTC_data_type;
        signal crc_reset, crc_en                    : std_logic;
        signal crc_out, ttc_frame_crc, ttc_calc_crc : std_logic_vector(15 downto 0);
        signal crc_in                               : std_logic_vector(31 downto 0);
        signal start_crc                            : std_logic;
    begin
        crc_reset <= '1' when ttc_dec_counter = 5 else '0';
        crc_en    <= not crc_reset;
        crc_in    <= LTI_RX_Data_Transceiver(i) when ttc_dec_counter /= 5 else (others => '1');

        CRC16_0 : entity work.crc16_lti
            port map(
                data_in => crc_in,
                crc_en  => crc_en,
                rst     => crc_reset,
                clk     => LTI_RXUSRCLK_in(i),
                crc_out => crc_out
            );

        calc_crc : process(LTI_RXUSRCLK_in)
        begin
            if rising_edge(LTI_RXUSRCLK_in(i)) then
                if (ttc_dec_counter = 0) then
                    start_crc <= '1';
                elsif (ttc_dec_counter = 5 and start_crc = '1') then
                    ttc_calc_crc <= crc_out;
                    start_crc    <= '0';
                end if;
            end if;
        end process;

        LTI_CRC_Err(i) <= '1' when ttc_frame_crc /= ttc_calc_crc else '0';
        --Set at Quad 0 since output is single record

        build_ttc_frame_proc : process(LTI_RXUSRCLK_in)
        begin
            if rising_edge(LTI_RXUSRCLK_in(i)) then
                if LTI_RX_RX_CharIsK(i) = "0001" and LTI_RX_Data_Transceiver(i)(7 downto 0) = x"BC" then
                    ttc_dec_counter <= 0;
                    ttc_frame_crc   <= LTI_RX_Data_Transceiver(i)(31 downto 16);
                else
                    if (ttc_dec_counter < 5) then
                        ttc_dec_counter                                                  <= ttc_dec_counter + 1;
                        TTC_frame(ttc_dec_counter * 32 + 31 downto ttc_dec_counter * 32) <= LTI_RX_Data_Transceiver(i);
                    --Set at Quad 0 since output is single record
                    end if;
                end if;
            end if;
        end process;

        TTC_decoded.TriggerType       <= TTC_frame(4 * 32 + 31 downto 4 * 32 + 16);
        TTC_decoded.LBID              <= TTC_frame(4 * 32 + 15 downto 4 * 32 + 0);
        TTC_decoded.OrbitID           <= TTC_frame(3 * 32 + 31 downto 3 * 32 + 0);
        TTC_decoded.L0ID(37 downto 6) <= TTC_frame(2 * 32 + 31 downto 2 * 32 + 0);
        TTC_decoded.SyncGlobalData    <= TTC_frame(1 * 32 + 31 downto 1 * 32 + 16);
        TTC_decoded.TS                <= TTC_frame(1 * 32 + 15);
        TTC_decoded.ErrorFlags        <= TTC_frame(1 * 32 + 14 downto 1 * 32 + 11);
        TTC_decoded.SL0ID             <= TTC_frame(1 * 32 + 10);
        TTC_decoded.SOrb              <= TTC_frame(1 * 32 + 9);
        TTC_decoded.Sync              <= TTC_frame(1 * 32 + 8);
        TTC_decoded.GRst              <= TTC_frame(1 * 32 + 7);
        TTC_decoded.L0A               <= TTC_frame(1 * 32 + 6);
        TTC_decoded.L0ID(5 downto 0)  <= TTC_frame(1 * 32 + 5 downto 1 * 32 + 0);
        TTC_decoded.MT                <= TTC_frame(0 * 32 + 31);
        TTC_decoded.PT                <= TTC_frame(0 * 32 + 30);
        TTC_decoded.Partition         <= TTC_frame(0 * 32 + 29 downto 0 * 32 + 28);
        TTC_decoded.BCID              <= TTC_frame(0 * 32 + 27 downto 0 * 32 + 16);
        TTC_decoded.SyncUserData      <= TTC_frame(0 * 32 + 15 downto 0 * 32 + 0);

        TTCout(i) <= TTC_decoded;
        
//        ila_ttc_rx : ila_3
//        PORT MAP (
//            clk => LTI_RXUSRCLK_in(i),
//            probe0 => LTI_RX_RX_CharIsK(i) & LTI_RX_Data_Transceiver(i)
//        );

    end generate g_lanes;


end architecture;
