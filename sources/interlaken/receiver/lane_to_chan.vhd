library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;

library XPM;
use xpm.vcomponents.all;

entity lane_to_chan is
    generic(
		lanes : integer := 4
	);
    port(
        clk              : in std_logic;                     -- Clock input
        rst  		     : in std_logic;					 -- Reset decoder

        meta_data_in     : in std_logic_vector(LANES*67-1 downto 0); -- data from meta deframing
        meta_valid_In    : in std_logic_vector(lanes-1 downto 0);
        
        data_out         : out std_logic_vector(LANES*67-1 downto 0);--(LANES*LANES*67-1 downto 0);-- data to burst channels

        Data_Valid_Out   : out std_logic_vector(lanes-1 downto 0)
    );
end entity lane_to_chan;

architecture conv of lane_to_chan is
    type state_type is (IDLE, CRC);
    signal pres_state : state_type; --ToDo fix statemachine or get rid of it -- @suppress "signal pres_state is never read"
    signal channel_s : integer;
    
    type slv_multi_67_array is array(natural range <>) of std_logic_vector(lanes*67-1 downto 0);
    signal data_fifo : slv_multi_67_array(0 to Lanes-1);

    signal almost_full: std_logic_vector(Lanes-1 downto 0); 
    
    signal data_channel  : std_logic_vector(LANES*LANES*67-1 downto 0);
    signal valid_channel : std_logic_vector(lanes-1 downto 0);
    
    signal fifo_select_s : integer range 0 to 3;
    
begin
    
    --~ state_decoder : process (rst, clk)
    --~ begin
        --~ if rst = '1' then
            --~ pres_state <= IDLE;
        --~ elsif rising_edge(clk) then
            --~ case pres_state is
            
                --~ when IDLE =>
                    --~ if meta_data_in(i*67+65) = '1' and meta_data_in(i*67+63) = '1' and meta_data_in(i*67+62) = '1' and meta_data_in(i*67+61) = '1' then --SOP 
                      --~ pres_state <= PACKET;
                      
                      --~ for fifo_select in 0 to lanes-1 loop 
					      --~ if almost_full(fifo_select) = '1' then
                              --~ fifo_select := fifo_select + 1;
                              --~ if fifo_select = 3 then --static for now
                                  --~ fifo_select := 0;
                              --~ end if;
                          --~ elsif almost_full(fifo_select) = '0' then
                              --~ exit;
                          --~ end if;
                      --~ end loop;
                    --~ end if;
                    
               --~ when PACKET => 
                    --~ mux_rdy(chan) <= '1';
                    --~ data_out <= data_in((chan+1)*LANES*67-1 downto (chan)*LANES*67);
                    --~ data_temp := data_in((chan+1)*LANES*67-1 downto (chan)*LANES*67);
                    
                    --~ --Test to ensure correct output after gearbox = '0'
                    --~ if mux_rdy(chan) = '0' then --VHDL2008 read output to know gearbox had been put to 0
                        --~ data_out <= data_temp_s;
                    --~ end if;
                    
                    --~ for i in 0 to LANES-1 loop
  	                    --~ --eop_in(i) := data_temp(65+i*67+chan*67) and data_temp(63+i*67) and data_temp(60+i*67);
  	                    --~ --if eop_in(i) = '1' then
  	                     --~ --   eop_detected := '1'
  	                     --~ tempval := (chan+1)*LANES+65+i*67;
  	                     --~ if gearboxready_p1 = "0000" and data_temp_s(65+i*67) = '1' and data_temp_s(63+i*67) = '1' and data_temp_s(60+i*67) = '1'then
                             --~ pres_state <= IDLE;
                             --~ chan <= chan + 1; --Switch to next channel so this channel won't be reselected
                                 --~ if chan = 3 then
                                     --~ chan <= 0;
                                 --~ end if;
                            --~ mux_rdy(chan) <= '0';
                            
  	                     --~ elsif (data_in((chan)*67*LANES+65+i*67) = '1' and data_in((chan)*67*LANES+63+i*67) = '1' and data_in((chan)*67*LANES+60+i*67) = '1') then --or (mux_rdy(chan) = '0' and (data_temp_s(65+i*67) = '1' and data_temp_s(63+i*67) = '1' and data_temp_s(60+i*67) = '1')) then --WRONG - Move last part to separate state
  	                         
  	                         --~ pres_state <= IDLE;
  	                         --~ --In this condition the EOP is still stored in the FIFO and requires an additional cycle to transmit
                             --~ if gearboxready_p1 = "0000" or meta_tready_p1 = "0000" then
                                 --~ --if data_temp_s(65+i*67) = '1' and data_temp_s(63+i*67) = '1' and data_temp_s(60+i*67) = '1'then
                                 --~ --pres_state <= IDLE;
                                 --~ --else
                                 --~ pres_state <= EOP;
                                 --~ --end if;
                             --~ else
                                 --~ chan <= chan + 1; --Switch to next channel so this channel won't be reselected
                                 --~ if chan = 3 then
                                     --~ chan <= 0;
                                 --~ end if;
                                 --~ mux_rdy(chan) <= '0';
                             --~ end if;
                       
                             --~ --data_out <= "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA"; 
                             --~ exit;
                         --~ end if;
  	                --~ end loop;
  	                
                    --~ --if eop_out(chan) = '1'  then
                      --~ --next_state <= IDLE;
                      --~ --mux_rdy(chan) <= '0';
                      --~ --data_out <= "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA" & "010" & X"8000_0000_00D6_3DBA"; 
                    
                    --~ --end if;
                --~ when EOP =>
                    --~ data_out <= data_in((chan+1)*LANES*67-1 downto (chan)*LANES*67);
                    --~ chan <= chan + 1; --Switch to next channel so this channel won't be reselected
                    --~ if chan = 3 then
                        --~ chan <= 0;
                    --~ end if;
                    --~ mux_rdy(chan) <= '0';
                    --~ pres_state <= IDLE;
                    
                --~ when others =>  -- @suppress "Case statement contains all choices explicitly. You can safely remove the redundant 'others'"
                    --~ pres_state <= IDLE;
            --~ end case;
        --~ end if;
    --~ end process state_decoder;
    
    
    
    
    tochannel : process (clk, rst) is
        variable channel : integer;
        variable fifo_select : integer range 0 to 3;
    begin
        if rst = '1' then
           channel := 0; 
           fifo_select := 0;
           data_valid_out <= (others => '0');
        elsif rising_edge(clk) then
            -- detect control word
            data_valid_out <= (others => '0');
            valid_channel <= (others => '0');
            for i in 0 to lanes-1 loop
                if meta_valid_in(i) = '1' then
                    
                    -- if control word, read channel number (only with SOP)
                    if meta_data_in(i*67+65) = '1' and meta_data_in(i*67+63) = '1' and meta_data_in(i*67+62) = '1' and meta_data_in(i*67+61) = '1' then --SOP
                        channel := to_integer(unsigned(meta_data_in(39 downto 32)));
                        
                        --for i in 0 to lanes-1 loop 
					      if almost_full(fifo_select) = '1' then -- Select FIFO to write packet to
                              if fifo_select = 3 then --static for now 
                                  fifo_select := 0;
                              end if;
                              fifo_select := fifo_select + 1;
                          elsif almost_full(fifo_select) = '0' then
                              fifo_select_s <= fifo_select;
                          end if;
                        --end loop;
                    end if;
                
                    --data_channel(channel*(lanes*67)+((i+1)*67)-1 downto channel*(lanes*67)+(i*67)) <= meta_data_in((i+1)*67-1 downto i*67); -- Remove huge vector (Can be reduced in size) 
                    data_fifo(fifo_select)((i+1)*67-1 downto i*67) <= meta_data_in((i+1)*67-1 downto i*67); 
                    valid_channel(fifo_select) <= '1'; 
                    
                    if meta_data_in(i*67+65) = '1' and meta_data_in(i*67+63) = '1' and meta_data_in(i*67+62) = '0' then
                        valid_channel(fifo_select) <= '0'; --IDLE word, no channel connected to it and otherwise causes overflows, handled elsewhere
                    end if;
                     
                end if;
            end loop;
            channel_s <= channel;
            
        end if;
    end process tochannel;
    
    --~ data_to_fifo : process (clk, rst) --Process which keeps track of the most empty fifo
        --~ variable fifo_select : integer range 0 to 3;
    --~ begin
        --~ if rst = '1' then
           --~ fifo_select := 0;
        --~ elsif rising_edge(clk) then
            --~ if almost_full(fifo_select) = '1' then
                --~ fifo_select := fifo_select + 1;
                --~ if fifo_select = 3 then --static for now
                  --~ fifo_select := 0;
                --~ end if;
            --~ end if;
            --~ fifo_select_s <= fifo_select;
            --~ for i in 0 to lanes-1 loop 
			    --~ if almost_full(i) = '1' then
                  --~ --i := i + 1;
                  --~ --if i = 3 then --static for now
                   --~ --   i := 0;
                  --~ --end if;
                --~ elsif almost_full(i) = '0' then
                  --~ fifo_select_s <= i;
                  --~ exit;
                --~ end if;
            --~ end loop;
        --~ end if;
    --~ end process;
    
    -- Fifo's for each channel to convert the lanes*lanes*67 bits to a lanes*67 bits bus (1024 = 4*lanes to 256 = lane to 64 = chan)
    g_lanes: for i in 0 to lanes-1 generate      -- Generate RX Lanes (Channels)
    begin
    
    xpm_fifo_sync_inst_2 : xpm_fifo_sync
    generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
      DOUT_RESET_VALUE => "0",    -- String
      ECC_MODE => "no_ecc",       -- String
      FIFO_MEMORY_TYPE => "auto", -- String
      FIFO_READ_LATENCY => 1,     -- DECIMAL
      FIFO_WRITE_DEPTH => 64,   -- DECIMAL
      FULL_RESET_VALUE => 0,      -- DECIMAL
      PROG_EMPTY_THRESH => 10,    -- DECIMAL
      PROG_FULL_THRESH => 64-16,     -- DECIMAL
      RD_DATA_COUNT_WIDTH => 5,   -- DECIMAL
      READ_DATA_WIDTH => 67,      -- DECIMAL
      READ_MODE => "fwft",         -- String
      SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      USE_ADV_FEATURES => "1707", -- String
      WAKEUP_TIME => 0,           -- DECIMAL
      WRITE_DATA_WIDTH => lanes*67,     -- DECIMAL
      WR_DATA_COUNT_WIDTH => 5    -- DECIMAL
    )
    port map (
      wr_clk => clk,  
      rst    => rst,  
      wr_en  => valid_channel(i),  
      din    => data_fifo(i),--data_channel((i+1)*(lanes*67)-1 downto i*(lanes*67)),
      data_valid => data_valid_out(i), --ltc_valid_in(i),
      prog_empty => open,
      prog_full  => almost_full(i),
      
      dout   => data_out((i+1)*67-1 downto i*67),--ltc_data_in((i+1)*67-1 downto i*67),
      rd_en  => '1', --fiforead(i), 
       
      almost_empty => open,
      almost_full  => open,
      empty        => open,
      full         => open,
      overflow     => open,
      
      rd_data_count => open,
      rd_rst_busy   => open,
      sbiterr       => open, 
      dbiterr       => open,
      underflow     => open,
      wr_ack        => open, -- useful?
      wr_data_count => open, --wr_data_count(i),
      wr_rst_busy   => open,
      injectdbiterr => '0',
      injectsbiterr => '0',
      sleep => '0'
    );
    
    end generate;


end architecture conv;
