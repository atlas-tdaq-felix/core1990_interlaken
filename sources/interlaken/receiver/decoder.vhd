-----------------------------------------------------------------
-- Word boundary lock state machine for 64b/67b Interlaken
-- See Figure 13 in the Interlaken protocol definition document
--
-- Authors: T. Xu and A. Paramonov (Argonne National Laboratory)
-- January 2022
--
-----------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
use work.interlaken_package.all;

entity Decoder is
    port(
        Clk             : in std_logic;                     -- Clock input
        Reset            : in std_logic;                        -- Reset decoder
        Data_In         : in std_logic_vector(66 downto 0); -- Data input
        Data_Valid_In   : in std_logic;
        Data_Valid_Out  : out std_logic;
        Data_Out        : out std_logic_vector(66 downto 0);-- Decoded 64-bit output
        Decoder_Lock    : out std_logic;
        Sync_Error        : out std_logic;
        Bitslip         : out std_logic
    );
end entity Decoder;

architecture Decoding of Decoder is


    signal sync_valid : std_logic;
    signal Bitslip_wait_cnt : integer range 0 to 31;
    signal lock : std_logic := '0';

    type state_type is (RESET_S, COUNT_SYNCS, WORD_LOCK, WAIT_FOR_BITSLIP);
    signal pres_state  : state_type := RESET_S;
    signal Sync_Counter, Word_Counter : integer range 0 to 127;
    signal Sync_Error_Counter : integer range 0 to 31;
    signal data_out_s : std_logic_vector(66 downto 0);
    signal data_valid_out_s : std_logic;
 
begin

    sync_valid <= '1' when ((Data_In(65 downto 64) = "01") OR (Data_In(65 downto 64) = "10")) else '0'; -- these are the valid sync headers
    Sync_Error <= Data_Valid_In and (not sync_valid); --Anternatively, we can define it in the word boundary lock state machine
    Decoder_Lock <= lock;
    
    data_valid_out_s <= Data_Valid_In when ((Reset  = '0') and (lock = '1')) else '0';
    data_out_s <= Data_In when (Data_In(66) = '0') else (Data_In(66 downto 64) & not(Data_In(63 downto 0)) ); -- inversion
    
    word_boundary_lock_state_machine : process (Clk)
    begin
        if (rising_edge(Clk)) then
            Data_Valid_Out <= data_valid_out_s;
            Data_Out <= data_out_s; 
            case pres_state is            
                when RESET_S => 
                    Bitslip <= '0';
                    Bitslip_wait_cnt <= 0;             
                    Sync_Counter <= 0;
                    lock <= '0';
                    Word_Counter <= 0;
                    Sync_Error_Counter <= 0;
                    
                    if(Reset = '0') then
                        pres_state <= COUNT_SYNCS;                
                    end if;
                    
                when COUNT_SYNCS =>
                    if(Reset = '1') then
                        pres_state <= RESET_S;
                    else -- not reset
                        if (Sync_Counter = 64) then 
                            Sync_Counter <= 0;
                            pres_state <= WORD_LOCK;
                            lock <= '1';
                        elsif (Data_Valid_In = '1') then
                           if(sync_valid = '1') then
                                Sync_Counter <= Sync_Counter + 1;
                           else
                                Sync_Counter <= 0;
                                Bitslip <= '1';
                                pres_state <= WAIT_FOR_BITSLIP;
                           end if;
                        end if;
                    end if; -- not reset
                    
                   --count clock sycles till the gearbox advances the phase 
                when WAIT_FOR_BITSLIP =>  
                    Bitslip <= '0';
--                    Bitslip_wait_cnt
                    if(Reset = '1') then
                        pres_state <= RESET_S;
                    else
                        if(Bitslip_wait_cnt = 16) then
                            Bitslip_wait_cnt <= 0;
                            pres_state <= COUNT_SYNCS;  --resume inspection of the sync headers    
                        else
                            Bitslip_wait_cnt <= Bitslip_wait_cnt +1;
                        end if;
                     end if;
                         
                when WORD_LOCK =>    
                     if(Reset = '1') then
                        pres_state <= RESET_S;
                     else                
                          if (Word_Counter = 64) then
                             Word_Counter <= 0;
                             Sync_Error_Counter <= 0;
                          else                   
                            if(Data_Valid_In = '1' and sync_valid = '0') then 
                                Sync_Error_Counter <= Sync_Error_Counter + 1;
                                if(Sync_Error_Counter = 16) then
--                                if(Sync_Error_Counter = 2) then
                                    pres_state <= RESET_S;
                                end if;
                            end if;                               
                         end if;
                     end if;                   

                when others =>
            end case;
        end if;
    end process word_boundary_lock_state_machine;
-------------------------------------------------------------------------------------------
  
end architecture Decoding;


