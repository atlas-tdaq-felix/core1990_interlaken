library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

library XPM;
use xpm.vcomponents.all;
use work.interlaken_package.all;
use work.axi_stream_package.ALL;

entity interlaken_receiver_channel is
    generic (
        MetaFrameLength  : positive
    );
    port (
        clk   		     : in std_logic;
        reset 		     : in std_logic;
		
        RX_Data_In 	     : in std_logic_vector(66 downto 0);
        FlowControl	     : out std_logic_vector(15 downto 0);   -- Flow control data (yet unutilized)
        RX_Datavalid     : in std_logic; -- From transceiver
        Bitslip          : out std_logic;
        m_axis_deburst   : out axis_64_type;
        m_axis_aclk       : in std_logic;
        m_axis            : out axis_64_type;
        m_axis_tready     : in std_logic;
        m_axis_prog_empty : out std_logic;
        Descrambler_lock : out std_logic;
        Decoder_Lock : out std_logic;
        decoder_error_sync              : out std_logic;
        descrambler_error_badsync       : out std_logic;
        descrambler_error_statemismatch : out std_logic;
        descrambler_error_nosync        : out std_logic;
        burst_crc24_error               : out std_logic;
        meta_crc32_error                : out std_logic;
        crc24_error_count               : out std_logic_vector(15 downto 0);
        crc32_error_count               : out std_logic_vector(15 downto 0);
        error_truncation                : out std_logic;
        
        HealthLane : out std_logic;
        HealthInterface: out std_logic
        
    );
end entity interlaken_receiver_channel;

architecture receiver of interlaken_receiver_channel is
    signal HealthInterface_s : std_logic;  
    signal s_axis_aresetn : std_logic;
    signal axis_tready  : std_logic;
    signal axis : axis_64_type;
    
    signal rx_packet_count, rx_byte_count : integer;
    signal crc24_count, crc32_count : unsigned(15 downto 0);
    
begin
    m_axis_deburst <= axis;
    
--    HealthInterface_procc : process(HealthInterface_s)
--    begin
--        HealthInterface <= '1';
--            if (HealthInterface_s = '0') then
--                HealthInterface <= '0';
--            end if;
--    end process;

    lane_rx: entity work.Interlaken_Receiver
            generic map (
                MetaFrameLength   => MetaFrameLength,
                LaneNumber        => 1                  -- Current Lane (RX channel)
            )
            port map(
                clk => clk,
                reset => reset,
 
                RX_Data_In   => RX_Data_In(66 downto 0),
                RX_Datavalid => RX_Datavalid,
                m_axis => axis,                         --: out axis_64_type;
                m_axis_tready => axis_tready,           --: in std_logic;
                Flowcontrol => FlowControl,
                Descrambler_lock => Descrambler_lock,
                Decoder_Lock => Decoder_Lock,
                decoder_error_sync              => decoder_error_sync,
                descrambler_error_badsync       => descrambler_error_badsync,
                descrambler_error_statemismatch => descrambler_error_statemismatch,
                descrambler_error_nosync        => descrambler_error_nosync,
                burst_crc24_error               => burst_crc24_error,
                meta_crc32_error                => meta_crc32_error,
                Bitslip => Bitslip,
                HealthLane => HealthLane,
                HealthInterface => HealthInterface
            );

        s_axis_aresetn <= not reset;

        fifo0 : entity work.Axis64Fifo
            generic map(
                DEPTH => 2048
            )
            port map(
                -- axi stream slave
                s_axis_aresetn    => s_axis_aresetn,            --: in std_logic;
                s_axis_aclk       => clk,                       --: in std_logic;
                s_axis            => axis,                      --: in axis_64_type;
                s_axis_tready     => axis_tready,               --: out std_logic;

                -- axi stream master
                m_axis_aclk       => m_axis_aclk,               --: in std_logic;
                m_axis            => m_axis,                 --: out axis_64_type;
                m_axis_tready     => m_axis_tready,          --: in std_logic;

                --Indication that the FIFO contains a block of data (for MUX).
                m_axis_prog_empty => m_axis_prog_empty      --: out std_logic
            );
            
         stats: process (clk)
         begin
             if reset = '1' then
                rx_packet_count <= 0;
                rx_byte_count <= 0;
             
             elsif rising_edge(clk) then
                if m_axis_tready = '1' then

                    if axis.tlast = '1' then
                        rx_packet_count <= rx_packet_count + 1;
                    end if;
                    
                    if axis.tvalid = '1' then
                        rx_byte_count <= rx_byte_count + 8;
                    end if;
                 end if;   
             end if;
         
         end process;
         
         crc_counters: process (clk)
         begin
             if reset = '1' then
                crc24_count <= (others => '0');
                crc32_count <= (others => '0');
             
             elsif rising_edge(clk) then
             
                if burst_crc24_error = '1' then
                    crc24_count <= crc24_count + 1;
                end if;
                
                if meta_crc32_error = '1' then
                    crc32_count <= crc32_count + 1;
                end if;
                crc24_error_count <= std_logic_vector(crc24_count);
                crc32_error_count <= std_logic_vector(crc32_count);
             end if;
         
         end process;
         
end architecture receiver;
