library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

entity axis_data_generator is
  generic(
      lanes : positive := 1
  );
  Port ( 
      s_axis_aclk : in std_logic_vector(Lanes-1 downto 0);
      m_axis_aclk : in std_logic_vector(Lanes-1 downto 0);
      s_axis_aresetn : in std_logic_vector(lanes-1 downto 0);
      m_axis_aresetn : in std_logic_vector(lanes-1 downto 0);
      
      s_axis : out axis_64_array_type(0 to Lanes-1);
      s_axis_tready : in axis_tready_array_type(0 to Lanes-1);

      m_axis            : in axis_64_array_type(0 to Lanes-1);
      m_axis_tready     : out axis_tready_array_type(0 to Lanes-1);
      m_axis_prog_empty : in axis_tready_array_type(0 to Lanes-1);
      
      data_length_i : in slv_array(0 to lanes-1)(15 downto 0);
      packet_length_i : in slv_array(0 to lanes-1)(15 downto 0);
      
      latency_o : out slv_16_array(lanes-1 downto 0);         
      valid_o   : out std_logic_vector(lanes-1 downto 0);
      
      count_rx_o : out slv_32_array(lanes-1 downto 0);   
      packet_num_rx_o : out slv_32_array(lanes-1 downto 0);   
      pkt_err_cnt_o : out slv_16_array(lanes-1 downto 0);   
      wrd_err_cnt_o : out slv_16_array(lanes-1 downto 0);   
      
      HealthLane : in std_logic_vector(lanes-1 downto 0);
      HealthInterface : in std_logic
  );
end axis_data_generator;

architecture Behavioral of axis_data_generator is
    --signal latency, latency_val : integer;
    signal HealthInterface_sync: std_logic;
    signal HealthLane_sync: std_logic_vector(lanes-1 downto 0);
    signal valid : std_logic_vector(lanes-1 downto 0);
    
    type int_array is array(natural range <>) of integer;
    signal pkt_error_count : int_array(lanes-1 downto 0) := (others => 0);
    signal wrd_error_count : int_array(lanes-1 downto 0) := (others => 0);

begin

    g_hlsync: for i in 0 to Lanes-1 generate
        xpm_cdc_single_inst : xpm_cdc_single
        generic map (
          DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
          INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
          SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
          SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
          dest_out => HealthLane_sync(i), -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
          dest_clk => s_axis_aclk(i), -- 1-bit input: Clock signal for the destination clock domain.
          src_clk => m_axis_aclk(i),   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
          src_in => HealthLane(i)      -- 1-bit input: Input signal to be synchronized to dest_clk domain.
        );
    end generate;
     
    m_axis_tready <= (others => '1');
    
    g_gen: for i in 0 to Lanes-1 generate
        signal count : unsigned(31 downto 0);
        signal packet_num : unsigned(31 downto 0);
        signal tdata_t1, tdata_t2 : std_logic_vector(63 downto 0);
        signal data_length, packet_length, data_length_s : integer;
    begin
    
        assign_signals : process(s_axis_aresetn, s_axis_aclk)
        begin
            if rising_edge (s_axis_aclk(i)) then
                data_length_s <= to_integer(unsigned(data_length_i(i))); 
                packet_length <= to_integer(unsigned(packet_length_i(i)));
                data_length <= packet_length when data_length_s > packet_length else data_length_s;
            end if;
        end process;
        
        generate_axis_data : process(s_axis_aresetn, s_axis_aclk)
            variable do_count : std_logic;
        begin
            if s_axis_aresetn(i) = '0' then
                s_axis(i).tvalid <= '0';
                s_axis(i).tlast <='0';
                count  <= to_unsigned(1,32);
                packet_num <= to_unsigned(1,32);
                
            elsif rising_edge (s_axis_aclk(i)) then
                if HealthLane_sync(i) = '1' then
                    s_axis(i).tvalid <= '1';
                    s_axis(i).tkeep <= (others => '0');
                    s_axis(i).tuser <= (others => '0');
                    s_axis(i).tid <= (others => '0');
                    
                    do_count := '0';
                    if count < packet_length+1 then
                        do_count := '0';
                        if (s_axis_tready(i) = '1') then --and (HealthLane(i) = '1') then
                            s_axis(i).tlast <='0';
                            --count <= count + 1;
                            do_count := '1';
                            s_axis(i).tdata <= std_logic_vector(packet_num & count + i);
                            --tdata_t1 <= std_logic_vector(packet_num & count);
                            
                            if count = data_length then
                                s_axis(i).tlast <='1'; --Improve so it applies to all lanes!
                                if count = packet_length then 
                                    count <= to_unsigned(1,32);
                                    packet_num <= packet_num + 1;
                                    do_count := '0';
                                end if;
                            elsif count >= packet_length then
                                count <= to_unsigned(1,32);
                                s_axis(i).tvalid <= '0';
                                packet_num <= packet_num + 1;
                                do_count := '0';
                            elsif count > data_length then
                                s_axis(i).tvalid <= '0';
                            end if;
                        end if;

                        if do_count = '1' then
                            count <= count + 1;
                        end if;
                    else
                        count <= (others => '0');
                    end if;
                    
                end if;
            end if;
        end process;
    end generate;
    
    g_check: for i in 0 to Lanes-1 generate
        signal packet_num_rx, count_rx : integer := 0;
        
    begin
        check_data : process(m_axis_aresetn, m_axis_aclk) 
            variable packet_num_v, count_v : integer;
        begin
            if m_axis_aresetn(i) = '0' then
                packet_num_rx <= 1;
                count_rx <= 1;
                count_v := 0;
                packet_num_v := 0;
                valid(i) <= '0';
                pkt_error_count(i) <= 0;
                wrd_error_count(i) <= 0;
            elsif rising_edge (m_axis_aclk(i)) then
                --for i in 0 to Lanes-1 loop
                    if m_axis(i).tvalid = '1' then
                        valid(i) <= '0';
                        count_v := to_integer(unsigned(m_axis(i).tdata(31 downto 0)));
                        packet_num_v := to_integer(unsigned(m_axis(i).tdata(63 downto 32)));
                        
                        if count_v = count_rx + i and packet_num_v = packet_num_rx then 
                            valid(i) <= '1';
                        end if;
                        
                        count_rx <= count_rx + 1;
                        
                        if m_axis(i).tlast ='1' then
                            packet_num_rx <= packet_num_rx + 1;
                            count_rx <= 1;
                            if valid (i) = '0' then
                                packet_num_rx <= packet_num_v + 1;
                            end if;
                        end if;
                        
                        --Try to resync
--                        if valid(i) = '0' and count_v = 1 + i then
--                            count_rx <= count_v + 1;
--                            packet_num_rx <= packet_num_v;
--                        end if;
                        
                        --Count lost packages
                        if valid(i) = '0' and count_rx = 1 then
                            pkt_error_count(i) <= pkt_error_count(i) + 1;
                        end if;
                        
                        --Count lost words
                        if valid(i) = '0' then
                            wrd_error_count(i) <= wrd_error_count(i) + 1;
                        end if;
                        
                    end if;
                    
                --end loop;
            end if;
        end process;
        
        count_rx_o(i) <= std_logic_vector(to_unsigned(count_rx,32));
        packet_num_rx_o(i) <= std_logic_vector(to_unsigned(packet_num_rx,32));
        pkt_err_cnt_o(i) <= std_logic_vector(to_unsigned(pkt_error_count(i),16));
        wrd_err_cnt_o(i) <= std_logic_vector(to_unsigned(wrd_error_count(i),16));
    end generate;
    
    valid_o <= valid;

    g_latency_cdc: for i in 0 to Lanes-1 generate
        signal data_latency : slv_array(0 to lanes-1)(64 downto 0);
        signal data_store : std_logic_vector(63 downto 0);
        signal start_count : std_logic;
        signal latency, latency_val : unsigned(15 downto 0);
    begin
        xpm_cdc_array_single_inst : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
            WIDTH => 65   -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out => data_latency(i),
            dest_clk => m_axis_aclk(i), 
            src_clk => s_axis_aclk(i),
            src_in => s_axis(i).tvalid & s_axis(i).tdata
        );
        
        calc_latency : process (s_axis_aresetn, m_axis_aclk) 
        begin
            if m_axis_aresetn(i) = '0' then
                start_count <= '0';
                latency <= (others => '0');
                latency_val <= (others => '0');
                data_store <= (others => '0');
            elsif rising_edge (m_axis_aclk(i)) then
                -- trigger on first data words of packet and toggle start_c, to not disturb counting during multiple packet transmits
                if start_count = '0' then 
                    if data_latency(i)(64) = '1' then
                        data_store <= data_latency(i)(63 downto 0);
                        start_count <= '1';
                    end if;
                else
                    latency <= latency + 1;
                    if m_axis(i).tdata = data_store then
                        latency_val <= latency+5;
                        start_count <= '0';
                        latency <= (others => '0');
                    end if;
                    -- timeout
                    if latency > 1000 then
                        start_count <= '0';
                        latency <= (others => '0');
                    end if;
                end if;
                
            end if;
        end process;
        -- Add 5 cycles due to 4 cycles CDC and 1 cycle delay assigning data_store
        latency_o(i) <= std_logic_vector(latency_val);
    end generate;
    
--    g_latency: for i in 0 to Lanes-1 generate
--        signal latency, latency_val : integer;
--        signal data_store : std_logic_vector(63 downto 0);
--        signal start_count : std_logic;
        
--    begin
--        calc_latency : process (s_axis_aresetn, s_axis_aclk) 
--        begin
--            if s_axis_aresetn(i) = '0' then
--                start_count <= '0';
--                latency <= 0;
--                latency_val <= 0;
--                data_store <= (others => '0');
--            elsif rising_edge (s_axis_aclk(i)) then
--                -- trigger on first data words of packet and toggle start_c, to not disturb counting during multiple packet transmits
--                if s_axis(i).tdata(31 downto 0) = x"00000001" and start_count = '0' then
--                    data_store <= s_axis(i).tdata;
--                    start_count <= '1';
--                end if;
                
--                if start_count = '1' then
--                    latency <= latency + 1;
--                end if;
                
--                if m_axis(i).tdata = data_store then
--                    latency_val <= latency;
--                    start_count <= '0';
--                    latency <= 0;
--                end if;
                
--            end if;
--        end process;
        
--        latency_o(i) <= std_logic_vector(to_unsigned(latency_val, 16));
        
--    end generate;
    
--    g_latency_cdc_core: for i in 0 to Lanes-1 generate
--        signal data_latency : slv_array(0 to lanes-1)(64 downto 0);
--        signal data_store : std_logic_vector(63 downto 0);
--        signal start_count : std_logic;
--        signal latency, latency_val : unsigned(15 downto 0);
--    begin
--        xpm_cdc_array_single_inst : xpm_cdc_array_single
--        generic map (
--            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
--            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
--            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
--            SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
--            WIDTH => 65   -- DECIMAL; range: 1-1024
--        )
--        port map (
--            dest_out => data_latency(i),
--            dest_clk => m_axis_aclk(i), 
--            src_clk => s_axis_aclk(i),
--            src_in => m_axis_burst(i).tvalid & m_axis_burst(i).tdata
--        );
        
--        calc_latency : process (s_axis_aresetn, m_axis_aclk) 
--        begin
--            if s_axis_aresetn(i) = '0' then
--                start_count <= '0';
--                latency <= (others => '0');
--                latency_val <= (others => '0');
--                data_store <= (others => '0');
--            elsif rising_edge (m_axis_aclk(i)) then
--                -- trigger on first data words of packet and toggle start_c, to not disturb counting during multiple packet transmits
--                if start_count = '0' then 
--                    if data_latency(i)(64) = '1' then
--                        data_store <= data_latency(i)(63 downto 0);
--                        start_count <= '1';
--                    end if;
--                else
--                    latency <= latency + 1;
--                    if m_axis_deburst(i).tdata = data_store then
--                        latency_val <= latency+5;
--                        start_count <= '0';
--                        latency <= (others => '0');
--                    end if;
--                end if;
                
--            end if;
--        end process;
--        -- Add 5 cycles due to 4 cycles CDC and 1 cycle delay assigning data_store
--        latency_core_cdc(i) <= std_logic_vector(latency_val);
--    end generate;

--    g_latency: for i in 0 to Lanes-1 generate
--        signal latency, latency_val : integer;
--        signal data_store : std_logic_vector(63 downto 0);
--        signal start_count : std_logic;
        
--    begin
--        calc_latency : process (s_axis_aresetn, s_axis_aclk) 
--        begin
--           if s_axis_aresetn(i) = '0' then
--               start_count <= '0';
--               latency <= 0;
--               latency_val <= 0;
--               data_store <= (others => '0');
--           elsif rising_edge (s_axis_aclk(i)) then
--               -- trigger on first data words of packet and toggle start_c, to not disturb counting during multiple packet transmits
--               if (m_axis_burst(i).tdata(31 downto 0) = x"00000001" + i) and start_count = '0' then
--                   data_store <= m_axis_burst(i).tdata;
--                   start_count <= '1';
--               end if;
               
--               if start_count = '1' then
--                   latency <= latency + 1;
--               end if;
               
--               if m_axis_deburst(i).tdata = data_store then
--                   latency_val <= latency;
--                   start_count <= '0';
--                   latency <= 0;
--               end if;
               
--           end if;
--        end process;
        
--        latency_o(i) <= std_logic_vector(to_unsigned(latency_val, 16));
        
--    end generate;

--    g_latency_core: for i in 0 to Lanes-1 generate
--        signal latency, latency_val : integer;
--        signal data_store : std_logic_vector(63 downto 0);
--        signal start_count : std_logic;
        
--    begin
--        calc_latency : process (s_axis_aresetn, s_axis_aclk) 
--        begin
--           if s_axis_aresetn(i) = '0' then
--               start_count <= '0';
--               latency <= 0;
--               latency_val <= 0;
--               data_store <= (others => '0');
--           elsif rising_edge (s_axis_aclk(i)) then
--               -- trigger on first data words of packet and toggle start_c, to not disturb counting during multiple packet transmits
--               if (s_axis(i).tdata(31 downto 0) = x"00000001" + i) and start_count = '0' then
--                   data_store <= s_axis(i).tdata;
--                   start_count <= '1';
--               end if;
               
--               if start_count = '1' then
--                   latency <= latency + 1;
--               end if;
               
--               if m_axis(i).tdata = data_store then
--                   latency_val <= latency;
--                   start_count <= '0';
--                   latency <= 0;
--               end if;
               
--           end if;
--        end process;
        
--        latency_core_o(i) <= std_logic_vector(to_unsigned(latency_val, 16));
        
--    end generate;

end Behavioral;
