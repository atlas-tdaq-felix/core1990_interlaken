----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/01/2021 04:27:39 PM
-- Design Name: 
-- Module Name: interlaken_top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity interlaken_top_tb is
end interlaken_top_tb;

architecture Behavioral of interlaken_top_tb is
    constant GTREFCLK_PERIOD        : time                :=  6.4 ns;  -- 156.25 MHz
    constant FREERUN_PERIOD         : time                :=  10.0 ns; -- 100 MHz
    constant Lanes                  : integer             :=  4;
    constant GTREFCLKS              : integer             :=  1;
    signal GTREFCLK_IN_P, GTREFCLK_IN_N : std_logic_vector(GTREFCLKS-1 downto 0);
    signal qsfp4_clock_p, qsfp4_clock_n : std_logic;
    signal freerun_clk_p, freerun_clk_n : std_logic;
    signal gt_rx_p, gt_rx_n : std_logic_vector(lanes-1 downto 0);
    signal gt_tx_p, gt_tx_n : std_logic_vector(lanes-1 downto 0);
    signal sys_rst : std_logic;
begin
    
    gt_rx_p <= gt_tx_p;
    gt_rx_n <= gt_tx_n;

    uut : entity work.interlaken_top
        generic map(
            lanes => lanes,
            GTREFCLKS => GTREFCLKS
        )    
        port map(
            freerun_clk_p => freerun_clk_p,
            freerun_clk_n => freerun_clk_n,
    
            -- GTY 156,25 MHz clock 
            GTREFCLK_IN_P => GTREFCLK_IN_P,
            GTREFCLK_IN_N => GTREFCLK_IN_N,
            sys_rst => sys_rst,

            -- QSFP4 data signals
            gt_rx_n => gt_rx_n,
            gt_rx_p => gt_rx_p,
            gt_tx_n => gt_tx_n,
            gt_tx_p => gt_tx_p
        );
    
    
    process
    begin
        sys_rst <=  '1';
        wait for FREERUN_PERIOD * 30;
        sys_rst <=  '0';
        wait;
    end process;
        
    process
    begin
        GTREFCLK_IN_N  <=  (others => '1');
        GTREFCLK_IN_P  <=  (others => '0');
        wait for GTREFCLK_PERIOD/2;
        GTREFCLK_IN_N  <=  (others => '0');
        GTREFCLK_IN_P  <=  (others => '1');
        wait for GTREFCLK_PERIOD/2;
    end process;
    
    process
    begin
        freerun_clk_n  <=  '1';
        freerun_clk_p  <=  '0';
        wait for FREERUN_PERIOD/2;
        freerun_clk_n  <=  '0';
        freerun_clk_p  <=  '1';
        wait for FREERUN_PERIOD/2;
    end process;

end Behavioral;
