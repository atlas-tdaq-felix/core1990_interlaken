library IEEE;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;

entity word_boundary_sync is
  generic (
    --! Number of consecutive words with correct header before word boundary is
    --! locked (64 according to the Interlaken specifcation)
    GOOD_WORDS_TO_LOCK     : positive := 64;
    --! Number of consecutive frames (after successful lock) during which the
    --! number of sync errors must not reach #SYNC_ERROR_TO_UNLOCK (64 according
    --! to the Interlaken specification)
    SYNC_WORD_COUNT_PERIOD : positive := 64;
    --! Number of sync errors (per #SYNC_WORD_COUNT_PERIOD words)
    --! to lose lock (16 according to the Interlaken specification)
    SYNC_ERRORS_TO_UNLOCK  : positive := 16;
    --! Number  of clock cycles #bitslip_o remains high
    BITSLIP_PULSE_LENGTH   : positive := 5;
    --! Number of clock cycles after #bitslip_o becomes high during which the
    --! incoming header is ignored to account for transceiver behavior
    BITSLIP_WAIT_DELAY     : positive := 32
  );
  port (
    --! Parallel clock (synchronous to the other ports)
    clk       : in    std_logic;
    --! Reset signal. The synchronizer starts reading input immediately after
    --! the reset is released. Some additional external delay might thus be needed.
    rst       : in    std_logic;
    --! Header bits (i.e. invesion bit and framing bits) from the incoming data word
    header_i  : in    std_logic_vector(2 downto 0);
    --! Indicates incoming header bits on the #header_i port. Set to low if
    --! there is no incoming frame, e.g. due to a 64b/67b gearbox.
    valid_i   : in    std_logic;
    --! Bitslip signal for hardware transceiver
    bitslip_o : out   std_logic;
    --! Indicates successfully locked word boundary
    lock_o    : out   std_logic
  );
end entity word_boundary_sync;

--! Implementation of word_boundary_sync
architecture behavioral of word_boundary_sync is

  function log2ceil (arg: positive) return natural is
    variable tmp    : positive;
    variable result : natural;
  begin

    tmp    := 1;
    result := 0;
    while arg > tmp loop
      tmp    := tmp * 2;
      result := result + 1;
    end loop;
    return result;

  end function;

  constant GOOD_WORDS_TO_LOCK_COUNTMAX : integer := GOOD_WORDS_TO_LOCK - 1;
  constant SYNC_WORD_COUNTMAX          : integer := SYNC_WORD_COUNT_PERIOD - 1;
  constant SYNC_ERROR_COUNTMAX         : integer := SYNC_ERRORS_TO_UNLOCK - 1;
  constant WORD_COUNTMAX               : integer :=
    maximum(GOOD_WORDS_TO_LOCK_COUNTMAX, SYNC_WORD_COUNTMAX);

  -- subtract offset of 1 and 2 to match definition of the generics
  constant BITSLIP_PULSE_LENGTH_COUNTMAX : integer := BITSLIP_PULSE_LENGTH - 1;
  constant BITSLIP_WAIT_DELAY_COUNTMAX   : integer := BITSLIP_WAIT_DELAY - 1;
  constant BITSLIP_CYCLE_COUNTMAX        : integer :=
    maximum(BITSLIP_PULSE_LENGTH_COUNTMAX, BITSLIP_WAIT_DELAY_COUNTMAX);

  type t_sync_fsm is (
    RESET,
    CHECK_SYNC,
    BITSLIP_PULSE,
    BITSLIP_WAIT,
    SYNCED
  );

  signal state : t_sync_fsm;

  -- word/cycle counters and reset signals
  signal sync_word_count     : unsigned(log2ceil(WORD_COUNTMAX + 1) - 1 downto 0);
  signal sync_error_count    : unsigned(log2ceil(SYNC_ERROR_COUNTMAX + 1) - 1 downto 0);
  signal bitslip_cycle_count : unsigned(log2ceil(BITSLIP_CYCLE_COUNTMAX + 1) - 1 downto 0);
  signal reset_word_count    : std_logic;
  signal reset_word_count2   : std_logic;
  signal reset_bitslip_count : std_logic;

  --! internal locked signal
  signal lock : std_logic;

  signal acquire_lock : std_logic;
  signal release_lock : std_logic;

  --! Header sync pattern is wrong
  signal header_sync_bad : std_logic;

  -- signals for bitslip logic
  signal bitslip            : std_logic;
  signal bitslip_pulse_done : std_logic;
  signal bitslip_wait_done  : std_logic;

begin

  header_sync_bad <=
    '1' when not(header_i(1 downto 0) = "10" or header_i(1 downto 0) = "01") else
    '0';

  proc_fsm_next_state : process (clk)
    variable next_state : t_sync_fsm;
  begin
    if rising_edge(clk) then
      if rst = '1' then
        state <= RESET;
      else
        next_state := state;

        case state is

          when RESET =>
            next_state := CHECK_SYNC;

          when CHECK_SYNC =>
            if valid_i = '1' then
              if header_sync_bad = '1' then
                next_state := BITSLIP_PULSE;
              elsif acquire_lock = '1' then
                next_state := SYNCED;
              end if;
            end if;

          when BITSLIP_PULSE =>
            if bitslip_pulse_done = '1' then
              next_state := BITSLIP_WAIT;
            end if;

          when BITSLIP_WAIT =>
            if bitslip_wait_done = '1' then
              next_state := CHECK_SYNC;
            end if;

          when SYNCED =>
            if release_lock = '1' then
              next_state := BITSLIP_PULSE;
            end if;

        end case;

        state <= next_state;
      end if;
    end if;
  end process proc_fsm_next_state;

  proc_fsm_output : process (all)
  begin
    bitslip             <= '0';
    reset_word_count    <= '0';
    reset_bitslip_count <= '0';
    lock                <= '0';

    case state is

      when RESET =>
        reset_word_count    <= '1';
        reset_bitslip_count <= '1';

      when CHECK_SYNC =>
        reset_bitslip_count <= '1';

      when BITSLIP_PULSE =>
        reset_word_count <= '1';
        bitslip          <= '1';

      when BITSLIP_WAIT =>
        reset_word_count <= '1';

      when SYNCED =>
        reset_bitslip_count <= '1';
        lock                <= '1';

    end case;

  end process proc_fsm_output;

  --! Count total number of incoming headers (#valid_i is '1') and erroneous headers since last reset
  proc_sync_word_count : process (clk)
  begin
    if rising_edge(clk) then
      if reset_word_count = '1' or reset_word_count2 = '1' then
        sync_word_count  <= (others => '0');
        sync_error_count <= (others => '0');
      elsif valid_i = '1' then
        sync_word_count <= sync_word_count + 1;
        if header_sync_bad = '1' then
          sync_error_count <= sync_error_count + 1;
        end if;
      end if;
    end if;
  end process proc_sync_word_count;

  -- restart word counter when changing to locked state and regularly during locked state
  reset_word_count2 <=
    '1' when valid_i = '1' and
      ((state = CHECK_SYNC and acquire_lock = '1') or
      (state = SYNCED and to_integer(sync_word_count) = SYNC_WORD_COUNTMAX)) else
    '0';

  acquire_lock <=
    '1' when sync_word_count = GOOD_WORDS_TO_LOCK_COUNTMAX else
    '0';

  release_lock <=
    '1' when to_integer(sync_error_count) = SYNC_ERROR_COUNTMAX and valid_i = '1' and header_sync_bad = '1' else
    '0';

  --! Count clock cycles during bitslip to properly time the pulse length and
  --! the delay afterwards
  proc_bitslip_count : process (clk)
  begin
    if rising_edge(clk) then
      if reset_bitslip_count = '1' then
        bitslip_cycle_count <= (others => '0');
      else
        bitslip_cycle_count <= bitslip_cycle_count + 1;
      end if;
    end if;
  end process proc_bitslip_count;

  bitslip_pulse_done <=
    '1' when to_integer(bitslip_cycle_count) = BITSLIP_PULSE_LENGTH_COUNTMAX else
    '0';

  bitslip_wait_done <=
    '1' when to_integer(bitslip_cycle_count) = BITSLIP_WAIT_DELAY_COUNTMAX else
    '0';

  bitslip_o <= bitslip;
  lock_o    <= lock;

end architecture behavioral;
