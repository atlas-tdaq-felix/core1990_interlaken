library IEEE;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;

entity txgearbox is
  generic (
    INPUT_BITWIDTH         : positive := 67;
    OUTPUT_BITWIDTH        : positive := 64;
    --! Should the actual serial transmission of #data_i be MSB-first or
    --! LSB-first? (Interlaken: MSB-first)
    TRANSMISSION_MSB_FIRST : boolean  := true;
    --! Does the FPGA's hardware transceiver transmit its parallel input data
    --! (i.e. #data_o) MSB-first or LSB-first?
    --! (Intel L-/H-/F-Tile and Xilinx GTY: LSB-first)
    TRANSCEIVER_MSB_FIRST  : boolean  := false
  );
  port (
    clk     : in    std_logic;
    rst     : in    std_logic;
    data_i  : in    std_logic_vector(INPUT_BITWIDTH - 1 downto 0);
    valid_i : in    std_logic;
    data_o  : out   std_logic_vector(OUTPUT_BITWIDTH - 1 downto 0);
    valid_o : out   std_logic;
    ready_o : out   std_logic
  );
end entity txgearbox;

--!
architecture behavioral of txgearbox is

  -- worst-case: buffer contains OUTPUT_BITWIDTH-1 bits and in the next clock
  -- all but one bit of the input needs to be buffered
  constant BUF_CAPACITY : integer := INPUT_BITWIDTH - 1;

  signal bits_buf : integer range 0 to BUF_CAPACITY;
  signal buf      : std_logic_vector(BUF_CAPACITY - 1 downto 0);

  -- the range of this signal is selected too large on purpose. Otherwise
  -- simulation may fail on clock cycles where `pause_input` becomes `1`, in
  -- case `buf_next` is written to before `pause_input` is updated.
  signal buf_next : std_logic_vector(BUF_CAPACITY + INPUT_BITWIDTH - OUTPUT_BITWIDTH - 1 downto 0);

  signal data_concat         : std_logic_vector(BUF_CAPACITY + INPUT_BITWIDTH - 1 downto 0);
  signal data_from_gearbox   : std_logic_vector(OUTPUT_BITWIDTH - 1 downto 0);
  signal data_from_gearbox_r : std_logic_vector(OUTPUT_BITWIDTH - 1 downto 0);
  signal data_output         : std_logic_vector(OUTPUT_BITWIDTH - 1 downto 0);

  signal buf_valid               : std_logic;
  signal data_from_gearbox_valid : std_logic;

  signal pause_input : std_logic;

begin

  -- We assume that every clock cycle one data word is transmitted on the
  -- output side and the input pauses if the buffer if full -- so the bitwidth
  -- needs to decrease under these conditions.
  assert INPUT_BITWIDTH > OUTPUT_BITWIDTH
    report "Currently only transmission to lower bus widths is supported"
    severity error;

  pause_input <=
    '1' when (bits_buf >= OUTPUT_BITWIDTH) or (rst = '1') else
    '0';

  gen_gearbox : if TRANSMISSION_MSB_FIRST generate

    proc_gearbox_msb_first : process (all)
    begin
      data_concat       <= buf & data_i;
      data_from_gearbox <=
        data_concat(INPUT_BITWIDTH + bits_buf - 1 downto INPUT_BITWIDTH + bits_buf - OUTPUT_BITWIDTH);

      buf_next <= (others => '0');
      if pause_input = '0' then
        buf_next(INPUT_BITWIDTH + bits_buf - OUTPUT_BITWIDTH - 1 downto 0) <=
          data_concat(INPUT_BITWIDTH + bits_buf - OUTPUT_BITWIDTH - 1 downto 0);
      end if;
    end process proc_gearbox_msb_first;

  else generate -- transmission is LSB first

    proc_gearbox_lsb_first : process (all)
    begin
      data_concat       <= data_i & buf;
      data_from_gearbox <= data_concat(BUF_CAPACITY - bits_buf + OUTPUT_BITWIDTH - 1 downto BUF_CAPACITY - bits_buf);

      buf_next <= (others => '0');
      if pause_input = '0' then
        buf_next(BUF_CAPACITY - 1 downto BUF_CAPACITY - bits_buf + OUTPUT_BITWIDTH - INPUT_BITWIDTH) <=
          data_concat(BUF_CAPACITY + INPUT_BITWIDTH - 1 downto BUF_CAPACITY - bits_buf + OUTPUT_BITWIDTH);
      end if;
    end process proc_gearbox_lsb_first;

  end generate gen_gearbox;

  proc_gearbox : process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        bits_buf                <= 0;
        buf                     <= (others => '0');
        buf_valid               <= '1';
        data_from_gearbox_r     <= (others => '0');
        data_from_gearbox_valid <= '0';
      else
        if pause_input = '1' then
          bits_buf  <= bits_buf - OUTPUT_BITWIDTH;
          buf       <= buf;
          buf_valid <= buf_valid;

          data_from_gearbox_r     <= data_from_gearbox;
          data_from_gearbox_valid <= buf_valid;
        else
          bits_buf  <= bits_buf + INPUT_BITWIDTH - OUTPUT_BITWIDTH;
          buf       <= buf_next(buf'range);
          buf_valid <= valid_i;

          data_from_gearbox_r     <= data_from_gearbox;
          data_from_gearbox_valid <= buf_valid and valid_i;
        end if;
      end if;
    end if;
  end process proc_gearbox;

  gen_reverse : if TRANSMISSION_MSB_FIRST /= TRANSCEIVER_MSB_FIRST generate

    gen_reverse : for i in 0 to OUTPUT_BITWIDTH - 1 generate
      data_output(i) <= data_from_gearbox_r(OUTPUT_BITWIDTH - 1 - i);
    end generate gen_reverse;

  else generate
    data_output <= data_from_gearbox_r;
  end generate gen_reverse;

  data_o  <= data_output;
  valid_o <= data_from_gearbox_valid;
  ready_o <= not pause_input;

end architecture behavioral;
