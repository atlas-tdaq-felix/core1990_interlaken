----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Tong Xu and Alexander Paramonov
--
-- Create Date: 02/07/2022 12:30:47 PM
-- Design Name: rxgearbox module for interlaken (64b67b) decoding
-- Module Name: rxgearbox_64b67b - Behavioral
-- adapted from DecodingGearbox module authored by Frans Schreuder
-- https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/blob/phase2/master/sources/decoding/DecodingGearBox.vhd 

-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rxgearbox_64b67b is
    Port ( clk : in STD_LOGIC;
           data_in        : in std_logic_vector(63 downto 0);
           data_out : out std_logic_vector(66 downto 0); -- Aligned output with set number of bits.
           data_valid_out : out std_logic; --data_out valid indicator
           BitSlip : in std_logic --R Triggered by the protocol decoder to shift one bit
);
end rxgearbox_64b67b;

architecture Behavioral of rxgearbox_64b67b is

    constant MAX_INPUT : integer := 64;
    constant MAX_OUTPUT : integer := 67;
    signal buf : std_logic_vector((MAX_INPUT+MAX_OUTPUT)-1 downto 0);
    signal BitsInBuf: integer range 0 to (MAX_INPUT+MAX_OUTPUT) := 0;
   
begin
--! Gearbox for 64b input and 66b output. 
--    shift_proc: process(clk, Reset)
    shift_proc: process(clk)
        
        
        variable DataOut_v: std_logic_vector(MAX_OUTPUT-1 downto 0);
    begin
        if rising_edge(clk) then
            data_valid_out <= '0';
                            
            for i in buf'high downto 0 loop
                if(i>=MAX_INPUT) then
                    buf(i) <= buf(i-MAX_INPUT);
                else
                          buf(i) <= data_in(i);
                end if;
            end loop;
                                                
                if (BitsInBuf < MAX_OUTPUT) then --we can't output enough in this case if bitslip is asserted.
                    if BitSlip = '1' then
						BitsInBuf <= BitsInBuf + MAX_INPUT - 1;  --Only add the number of bits on input, since no data was taken out of the buffer, -1 for bitslip.
					else
						BitsInBuf <= BitsInBuf + MAX_INPUT;  --Only add the number of bits on input, since no data was taken out of the buffer
					end if;
                    data_valid_out <= '0';
                else
					for i in MAX_OUTPUT-1 downto 0 loop
							DataOut_v(i) := buf(BitsInBuf-((MAX_OUTPUT)-i));
					end loop;
                    data_out <= DataOut_v;
                    data_valid_out <= '1';
                    if BitSlip = '1' then
						BitsInBuf <= BitsInBuf + (MAX_INPUT - MAX_OUTPUT) - 1; --Add input bits, but subtract output bits that were shefted out, -1 for bitslip.
					else
						BitsInBuf <= BitsInBuf + (MAX_INPUT - MAX_OUTPUT); --Add input bits, but subtract output bits that were shefted out.
					end if;
                end if;
        end if;
        
    end process;


end Behavioral;
