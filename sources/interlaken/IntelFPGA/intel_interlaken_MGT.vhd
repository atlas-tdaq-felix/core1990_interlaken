library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity interlaken_mgt is
 port(
   reset  : in std_logic;
   refclk : in std_logic;

   tx_ready : out std_logic;
   rx_ready : out std_logic;

	rst_txusr_403M_s : out std_logic_vector(0 downto 0);
	rst_rxusr_403M_s : out std_logic_vector(0 downto 0);

   tx_parallel_data : in std_logic_vector(63 downto 0);
   tx_control : in std_logic_vector(2 downto 0);

   rx_parallel_data : out std_logic_vector(63 downto 0);
   rx_control : out std_logic_vector(2 downto 0);

   TX_Gearboxready_Out : out std_logic;

   rx_clk_out : out std_logic;
   tx_clk_out : out std_logic;


   tx_serial : out std_logic;
   rx_serial : in std_logic;

   tx_data_valid : in std_logic;
   rx_data_valid : out std_logic;
	
	reconfig_clk : in std_logic := '0'
	
 );
end entity interlaken_mgt;

architecture rtl of 	interlaken_mgt is

	 component transceiver_reset is
        port (
            clock                : in  std_logic                    := 'X';             -- clk
            reset                : in  std_logic                    := 'X';             -- reset
            tx_analogreset       : out std_logic_vector(0 downto 0);                    -- tx_analogreset
            tx_digitalreset      : out std_logic_vector(0 downto 0);                    -- tx_digitalreset
            tx_ready             : out std_logic_vector(0 downto 0);                    -- tx_ready
            pll_locked           : in  std_logic_vector(0 downto 0) := (others => 'X'); -- pll_locked
            pll_select           : in  std_logic_vector(0 downto 0) := (others => 'X'); -- pll_select
            tx_cal_busy          : in  std_logic_vector(0 downto 0) := (others => 'X'); -- tx_cal_busy
            tx_analogreset_stat  : in  std_logic_vector(0 downto 0) := (others => 'X'); -- tx_analogreset_stat
            tx_digitalreset_stat : in  std_logic_vector(0 downto 0) := (others => 'X'); -- tx_digitalreset_stat
            pll_cal_busy         : in  std_logic_vector(0 downto 0) := (others => 'X'); -- pll_cal_busy
            rx_analogreset       : out std_logic_vector(0 downto 0);                    -- rx_analogreset
            rx_digitalreset      : out std_logic_vector(0 downto 0);                    -- rx_digitalreset
            rx_ready             : out std_logic_vector(0 downto 0);                    -- rx_ready
            rx_is_lockedtodata   : in  std_logic_vector(0 downto 0) := (others => 'X'); -- rx_is_lockedtodata
            rx_cal_busy          : in  std_logic_vector(0 downto 0) := (others => 'X'); -- rx_cal_busy
            rx_analogreset_stat  : in  std_logic_vector(0 downto 0) := (others => 'X'); -- rx_analogreset_stat
            rx_digitalreset_stat : in  std_logic_vector(0 downto 0) := (others => 'X')  -- rx_digitalreset_stat
        );
    end component transceiver_reset;

    component transceiver_phy is
        port (
            tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- tx_analogreset
            rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_analogreset
            tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- tx_digitalreset
            rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_digitalreset
            tx_transfer_ready       : out std_logic_vector(0 downto 0);                     -- tx_transfer_ready
            rx_transfer_ready       : out std_logic_vector(0 downto 0);                     -- rx_transfer_ready
            osc_transfer_en         : out std_logic_vector(0 downto 0);                     -- osc_transfer_en
            tx_fifo_ready           : out std_logic_vector(0 downto 0);                     -- tx_fifo_ready
            rx_fifo_ready           : out std_logic_vector(0 downto 0);                     -- rx_fifo_ready
            tx_digitalreset_timeout : out std_logic_vector(0 downto 0);                     -- tx_digitalreset_timeout
            rx_digitalreset_timeout : out std_logic_vector(0 downto 0);                     -- rx_digitalreset_timeout
            tx_analogreset_stat     : out std_logic_vector(0 downto 0);                     -- tx_analogreset_stat
            rx_analogreset_stat     : out std_logic_vector(0 downto 0);                     -- rx_analogreset_stat
            tx_digitalreset_stat    : out std_logic_vector(0 downto 0);                     -- tx_digitalreset_stat
            rx_digitalreset_stat    : out std_logic_vector(0 downto 0);                     -- rx_digitalreset_stat
            tx_dll_lock             : out std_logic_vector(0 downto 0);                     -- tx_dll_lock
            tx_cal_busy             : out std_logic_vector(0 downto 0);                     -- tx_cal_busy
            rx_cal_busy             : out std_logic_vector(0 downto 0);                     -- rx_cal_busy
            tx_serial_clk0          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            rx_cdr_refclk0          : in  std_logic                     := 'X';             -- clk
            tx_serial_data          : out std_logic_vector(0 downto 0);                     -- tx_serial_data
            rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
            rx_seriallpbken         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_seriallpbken
            rx_is_lockedtoref       : out std_logic_vector(0 downto 0);                     -- rx_is_lockedtoref
            rx_is_lockedtodata      : out std_logic_vector(0 downto 0);                     -- rx_is_lockedtodata
            tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            tx_clkout               : out std_logic_vector(0 downto 0);                     -- clk
            rx_clkout               : out std_logic_vector(0 downto 0);                     -- clk
            tx_parallel_data        : in  std_logic_vector(63 downto 0) := (others => 'X'); -- tx_parallel_data
            tx_fifo_wr_en           : in  std_logic                     := 'X';             -- tx_fifo_wr_en
            unused_tx_parallel_data : in  std_logic_vector(14 downto 0) := (others => 'X'); -- unused_tx_parallel_data
            rx_parallel_data        : out std_logic_vector(63 downto 0);                    -- rx_parallel_data
            rx_data_valid           : out std_logic;                                        -- rx_data_valid
            unused_rx_parallel_data : out std_logic_vector(14 downto 0);                    -- unused_rx_parallel_data
            rx_bitslip              : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_bitslip
            tx_fifo_full            : out std_logic_vector(0 downto 0);                     -- tx_fifo_full
            tx_fifo_pfull           : out std_logic_vector(0 downto 0);                     -- tx_fifo_pfull
            rx_fifo_empty           : out std_logic_vector(0 downto 0);                     -- rx_fifo_empty
            rx_fifo_pfull           : out std_logic_vector(0 downto 0);                     -- rx_fifo_pfull
            rx_fifo_pempty          : out std_logic_vector(0 downto 0);                     -- rx_fifo_pempty
            rx_fifo_rd_en           : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_fifo_rd_en
            rx_fifo_align_clr       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_fifo_align_clr
            rx_enh_frame_lock       : out std_logic_vector(0 downto 0);                     -- rx_enh_frame_lock
            rx_enh_blk_lock         : out std_logic_vector(0 downto 0);                     -- rx_enh_blk_lock
            reconfig_clk            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            reconfig_reset          : in  std_logic_vector(0 downto 0)  := (others => 'X')  -- reset
        );
    end component transceiver_phy;

	     component transceiver_pll is
        port (
            pll_refclk0       : in  std_logic := 'X'; -- clk
            tx_serial_clk_gxt : out std_logic;        -- clk
            pll_locked        : out std_logic;        -- pll_locked
            pll_cal_busy      : out std_logic;        -- pll_cal_busy
            reconfig_clk0         : in  std_logic                     := 'X';             -- clk
            reconfig_reset0       : in  std_logic                     := 'X';             -- reset
            reconfig_write0       : in  std_logic                     := 'X';             -- write
            reconfig_read0        : in  std_logic                     := 'X';             -- read
            reconfig_address0     : in  std_logic_vector(10 downto 0) := (others => 'X'); -- address
            reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
            reconfig_readdata0    : out std_logic_vector(31 downto 0);                    -- readdata
            reconfig_waitrequest0 : out std_logic                                    -- waitrequest
        );
    end component transceiver_pll;


  signal tx_analogreset : std_logic_vector(0 downto 0);
  signal rx_analogreset : std_logic_vector(0 downto 0);
  signal tx_digitalreset : std_logic_vector(0 downto 0);
  signal rx_digitalreset : std_logic_vector(0 downto 0);
  signal tx_analogreset_stat : std_logic_vector(0 downto 0);
  signal rx_analogreset_stat : std_logic_vector(0 downto 0);
  signal tx_digitalreset_stat : std_logic_vector(0 downto 0);
  signal rx_digitalreset_stat : std_logic_vector(0 downto 0);
  signal tx_dll_lock : std_logic_vector(0 downto 0);
  signal tx_cal_busy : std_logic_vector(0 downto 0);
  signal rx_cal_busy : std_logic_vector(0 downto 0);
  signal tx_serial_clk0 : std_logic_vector(0 downto 0);
  signal pll_locked : std_logic_vector(0 downto 0);
  signal pll_select : std_logic_vector(0 downto 0);
  signal pll_cal_busy : std_logic_vector(0 downto 0);
  signal rx_is_lockedtodata : std_logic_vector(0 downto 0);
  signal tx_coreclkin : std_logic_vector(0 downto 0);
  signal rx_coreclkin : std_logic_vector(0 downto 0);

  signal tx_fifo_wr_en : std_logic;
  signal tx_fifo_pfull_s : std_logic_vector(0 downto 0);
  signal rx_fifo_rd_en : std_logic_vector(0 downto 0);
  signal rx_fifo_pempty : std_logic_vector(0 downto 0);
  signal tx_data_valid_s : std_logic_vector(0 downto 0);

  signal rx_bitslip : std_logic;

  signal rx_ready_s : std_logic;

  signal rx_control_s : std_logic_vector(2 downto 0);
  signal rx_enh_data_valid : std_logic;

  signal tx_gearbox_reset_i, rx_gearbox_reset_i : std_logic;

  signal gt_data_valid_out_i   : std_logic_vector(0 downto 0);
  signal gt_pause_data_valid_r : std_logic_vector(0 downto 0);

  signal  gt_txseq_counter_r      : unsigned(8 downto 0);
  signal  gt_txsequence_i         : std_logic_vector(6 downto 0);

  signal rx_fifo_align_clr : std_logic;
  signal rx_enh_frame_lock : std_logic;
  signal rx_fifo_pfull : std_logic;

  signal rst_block_sync : std_logic;

  signal gtwiz_userdata_rx_out_s  : std_logic_vector(63 downto 0);
  signal txgearbox_data_out : std_logic_vector(63 downto 0);
  signal rx_mgt_data : std_logic_vector(63 downto 0);
  signal userdata_rx_reversed : std_logic_vector(63 downto 0);

  signal txgearbox_data_in : std_logic_vector(66 downto 0);
  signal rxgearbox_data_out : std_logic_vector(66 downto 0);

  signal rx_data_valid_phy : std_logic;

  signal reset_i : std_logic;

  signal tx_transfer_ready : std_logic_vector(0 downto 0);
  signal rx_transfer_ready : std_logic_vector(0 downto 0);

begin


  rx_ready <= rx_ready_s;

  tx_fifo_wr_en <= not tx_fifo_pfull_s(0);
  rx_fifo_rd_en(0) <= not rx_fifo_pempty(0);

  rx_clk_out <= rx_coreclkin(0);
  tx_clk_out <= tx_coreclkin(0);

  rx_control <= rx_control_s;

  rst_txusr_403M_s <= not tx_transfer_ready;
  rst_rxusr_403M_s <= not rx_transfer_ready;


  phy : component transceiver_phy
    port map (
      tx_analogreset          => tx_analogreset,
      rx_analogreset          => rx_analogreset,
      tx_digitalreset         => tx_digitalreset,
      rx_digitalreset         => rx_digitalreset,
      tx_analogreset_stat     => tx_analogreset_stat,
      rx_analogreset_stat     => rx_analogreset_stat,
      tx_digitalreset_stat    => tx_digitalreset_stat,
      rx_digitalreset_stat    => rx_digitalreset_stat,
      tx_transfer_ready       => tx_transfer_ready,
      rx_transfer_ready       => rx_transfer_ready,
      tx_dll_lock             => tx_dll_lock,
      tx_cal_busy             => tx_cal_busy,
      rx_cal_busy             => rx_cal_busy,
      tx_serial_clk0          => tx_serial_clk0,
      rx_cdr_refclk0          => refclk,
      tx_serial_data(0)       => tx_serial,
      rx_serial_data(0)       => rx_serial,
      rx_is_lockedtoref       => open,
      rx_seriallpbken         => "0",
      rx_is_lockedtodata      => rx_is_lockedtodata,
      tx_coreclkin            => tx_coreclkin,
      rx_coreclkin            => rx_coreclkin,
      tx_clkout               => tx_coreclkin,
      rx_clkout               => rx_coreclkin,
      tx_parallel_data        => txgearbox_data_out,
      tx_fifo_wr_en           => tx_fifo_wr_en,
      unused_tx_parallel_data => (others => '0'),
      rx_parallel_data        => rx_mgt_data,
      rx_data_valid        => rx_data_valid_phy,
      unused_rx_parallel_data => open,
      rx_bitslip(0)              => rx_bitslip,
      tx_fifo_full            => open,
      tx_fifo_pfull           => tx_fifo_pfull_s,
      rx_fifo_empty           => open,
      rx_fifo_pempty          => rx_fifo_pempty,
      rx_fifo_rd_en           => rx_fifo_rd_en,
      rx_enh_blk_lock         => open,
      rx_fifo_align_clr(0)    => rx_fifo_align_clr,  --in
      rx_enh_frame_lock(0)    => rx_enh_frame_lock,   --out
      rx_fifo_pfull(0)        => rx_fifo_pfull,
      reconfig_clk(0)         => reconfig_clk      
    );

  proc_rst_pipe : process(refclk)
  begin
    if rising_edge(refclk) then
      reset_i <= reset;
    end if;
  end process proc_rst_pipe;

  rst : component transceiver_reset
    port map (
      clock                => refclk,
      reset                => reset_i,
      tx_analogreset       => tx_analogreset,
      tx_digitalreset      => tx_digitalreset,
      tx_ready(0)             => tx_ready,
      pll_locked           => pll_locked,
      pll_select           => pll_select,
      tx_cal_busy          => tx_cal_busy,
      tx_analogreset_stat  => tx_analogreset_stat,
      tx_digitalreset_stat => tx_digitalreset_stat,
      pll_cal_busy         => pll_cal_busy,
      rx_analogreset       => rx_analogreset,
      rx_digitalreset      => rx_digitalreset,
      rx_ready(0)             => rx_ready_s,
      rx_is_lockedtodata   => rx_is_lockedtodata,
      rx_cal_busy          => rx_cal_busy,
      rx_analogreset_stat  => rx_analogreset_stat,
      rx_digitalreset_stat => rx_digitalreset_stat
    );




  pll : component transceiver_pll
    port map (
      pll_refclk0       => refclk,
      tx_serial_clk_gxt => tx_serial_clk0(0),
      pll_locked        => pll_locked(0),
      pll_cal_busy      => pll_cal_busy(0),
      reconfig_clk0     => reconfig_clk
    );

  txgearbox_inst : entity work.txgearbox_64b67b
    Port Map(
      clk           => tx_coreclkin(0),
      reset         => tx_digitalreset(0),
      data_in       => txgearbox_data_in,
      data_valid_in => '1',
      txsequence_in => gt_txsequence_i(6 downto 0),
      data_out      => txgearbox_data_out
    );

  rxgearbox_inst : entity work.rxgearbox_64b67b
    Port Map(
      clk            => rx_coreclkin(0),
      reset          => rx_digitalreset(0),
      data_in        => userdata_rx_reversed,
      data_valid_in  => '1',
      data_out       => rxgearbox_data_out,
      data_valid_out => rx_data_valid,
      BitSlip        => rx_bitslip
    );


  txgearbox_data_in <= tx_control & tx_parallel_data;
  gtwiz_userdata_rx_out_s <= rx_mgt_data;

  proc_pipeline : process(rx_coreclkin(0))
  begin
    if rising_edge(rx_coreclkin(0)) then
      rx_parallel_data <=  rxgearbox_data_out(63 downto 0);
      rx_control_s <= rxgearbox_data_out(66 downto 64);
    end if;
  end process;

  userdata_inverse : for j in 0 to 63 generate
    userdata_rx_reversed(j) <= gtwiz_userdata_rx_out_s(63 - j);
  end generate;

  process(tx_coreclkin(0))
  begin
    if rising_edge(tx_coreclkin(0)) then
      if((tx_digitalreset(0)='1') or (gt_txseq_counter_r = 66)) then
        gt_txseq_counter_r <=  (others => '0') ;
      else
        gt_txseq_counter_r <=  gt_txseq_counter_r + 1 ;
      end if;
    end if;
  end process;

  gt_txsequence_i <= std_logic_vector(gt_txseq_counter_r(6 downto 0));

  TX_Gearboxready_Out <= '1' when ((gt_txsequence_i /= "0010101") and (gt_txsequence_i /= "0101011") and (gt_txsequence_i /= "1000001")) else
                         '0';

  block_sync_sm_0_i  :  entity work.word_boundary_sync
    port map
    (
      clk =>  rx_coreclkin(0),
      rst => rx_digitalreset(0),

      header_i => rx_control_s,
      valid_i => rx_data_valid,
      bitslip_o => rx_bitslip,
      lock_o => open
    );




end architecture rtl;
