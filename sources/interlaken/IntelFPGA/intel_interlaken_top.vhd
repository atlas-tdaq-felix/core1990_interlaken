library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;


entity intel_interlaken_top is
    generic (
        lanes        : positive := 1;
        GTREFCLKS    : integer := 1
    );
port(

	refclk : in std_logic;

	clk_50: in std_logic;

	rx_serial : in std_logic;
	tx_serial : out std_logic;

   qsfp_lp_mode       : out   std_logic;
   qsfp_rst_n         : out   std_logic;
   qsfp_mod_sel_n     : out   std_logic;
   qsfp_scl           : out   std_logic;
   qsfp_sda           : inout std_logic;

   user_pb_n           : in    std_logic_vector(2 downto 0)

);
end entity intel_interlaken_top;

architecture test of intel_interlaken_top is

    constant    BondNumberOfLanes       : positive := 1;

    signal m_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal m_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_tready     : axis_tready_array_type(0 to Lanes-1);
    signal s_axis            : axis_64_array_type(0 to Lanes-1);
    signal s_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal s_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal s_axis_tready     : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal s_axis_tready is never read"
    signal m_axis            : axis_64_array_type(0 to Lanes-1);        -- @suppress "signal m_axis is never read"
    signal m_axis_prog_empty : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal m_axis_prog_empty is never read"

    signal latency_o : std_logic_vector(15 downto 0);
    signal valid_o   : std_logic_vector(lanes-1 downto 0);

    signal count_rx_o : slv_32_array(lanes-1 downto 0);
    signal packet_num_rx_o : slv_32_array(lanes-1 downto 0);
    signal pkt_err_cnt_o : slv_16_array(lanes-1 downto 0);
    signal wrd_err_cnt_o : slv_16_array(lanes-1 downto 0);

	 signal HealthLane        : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal HealthLane is never read"
    signal HealthInterface   : std_logic_vector((Lanes/BondNumberOfLanes)-1 downto 0);

    signal Decoder_lock      : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit-- @suppress "signal Decoder_lock is never read"
	 signal Descrambler_lock  : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal Descrambler_lock is never read"
    signal Channel           : std_logic_vector(7 downto 0);            --TODO use as status bit -- @suppress "signal Channel is never read"
    signal stat_rx_aligned   : STD_LOGIC;

	 signal decoder_error_sync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal decoder_error_sync is never read"
    signal descrambler_error_badsync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_badsync is never read"
    signal descrambler_error_statemismatch : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_statemismatch is never read"
    signal descrambler_error_nosync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_nosync is never read"
    signal burst_crc24_error : std_logic_vector(lanes-1 downto 0); -- @suppress "signal burst_crc24_error is never read" -- @suppress "signal meta_crc32_error is never read"
    signal meta_crc32_error  : std_logic_vector(lanes-1 downto 0); -- @suppress "signal meta_crc32_error is never read"

    signal tx_user_clk_out, rx_user_clk_out : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_burst, m_axis_deburst : axis_64_array_type(0 to Lanes - 1);

	 signal loopback : std_logic_vector(2 downto 0);


    component reset_release is
        port (
            ninit_done : out std_logic   -- ninit_done
        );
    end component reset_release;

	 signal reset : std_logic;

begin

  qsfp_lp_mode <= '0';

  --! reset block
  blk_rst_and_clk : block
    signal reset_boot : std_logic;
    signal ninit_done : std_logic;
	 signal reset_count : std_logic;
  begin

    rst_rls : component reset_release
       port map (
           ninit_done => ninit_done
    );

	 reset <= not user_pb_n(0) or ninit_done;

    -- Reset control
    proc_reset : process (refclk)
    begin
      if rising_edge(refclk) then
        if reset = '1' then
          qsfp_mod_sel_n <= '0';
          qsfp_sda       <= '0';
          qsfp_scl       <= '0';
          qsfp_rst_n     <= '0';
        else
          qsfp_mod_sel_n <= '1';
          qsfp_sda       <= 'Z';
          qsfp_scl       <= 'Z';
          qsfp_rst_n     <= '1';
        end if;
      end if;
     end process proc_reset;

   end block blk_rst_and_clk;



   interface : entity work.interlaken_interface
    generic map(
         BurstMax     => 256, --(Bytes)
         BurstShort   => 64, --(Bytes)
         MetaFrameLength => 256, --(Words)
         Lanes        => lanes,
         BondNumberOfLanes => BondNumberOfLanes,
         txlanes => 1,
         rxlanes => 1,
         CARD_TYPE => 128,
         GTREFCLKS => GTREFCLKS
    )
    port map(
            clk100 => clk_50,
            reset  => reset,
            axis_rst_ext => '0',--axis_rst_ext,
            interlaken_rst_ext => '0',--interlaken_rst_ext,
            GTREFCLK_IN_P(0) => refclk,
            GTREFCLK_IN_N => "0",
            tx_user_clk_out => tx_user_clk_out, --only first lane for now
            rx_user_clk_out => rx_user_clk_out,
            TX_Out_P(0) => tx_serial,
            TX_Out_N => open,--TX_Out_N_s,
            RX_In_P(0)  => rx_serial,
            RX_In_N  => "0",--RX_In_N_s,
            TX_FlowControl => (others => (others => '0')),
            m_axis_burst  => m_axis_burst,
            m_axis_deburst  => m_axis_deburst,
            s_axis_aresetn => s_axis_aresetn,
            m_axis_aresetn => m_axis_aresetn,
            s_axis        => s_axis,
            s_axis_aclk   => s_axis_aclk, --tx_user_clk_out
            s_axis_tready => s_axis_tready,
            FlowControl => open,
            m_axis_aclk   => m_axis_aclk, --rx_user_clk_out
            m_axis        => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            Decoder_Lock     => Decoder_lock,
            Descrambler_lock => Descrambler_lock,
            --Channel => Channel,
            loopback_in                     => loopback,
            HealthLane                      => HealthLane,
            HealthInterface                 => HealthInterface,
            decoder_error_sync              => decoder_error_sync,
            descrambler_error_badsync       => descrambler_error_badsync,
            descrambler_error_statemismatch => descrambler_error_statemismatch,
            descrambler_error_nosync        => descrambler_error_nosync,
            burst_crc24_error               => burst_crc24_error,
            meta_crc32_error                => meta_crc32_error
        );

    s_axis_aclk <= tx_user_clk_out;
    m_axis_aclk <= rx_user_clk_out;


	 ---- Generates input data and interface signals ----
    generate_data : entity work.axis_data_generator
    generic map (
        lanes => lanes
    )
    port map (
        s_axis_aclk => s_axis_aclk,
        m_axis_aclk => m_axis_aclk,
        s_axis_aresetn => s_axis_aresetn,
        m_axis_aresetn => m_axis_aresetn,

        s_axis => s_axis,
        s_axis_tready => s_axis_tready,

        m_axis            => m_axis,
        m_axis_tready     => m_axis_tready,
        m_axis_prog_empty => m_axis_prog_empty,

        latency_o  => latency_o,
        valid_o    => valid_o,
        count_rx_o => count_rx_o,
        packet_num_rx_o => packet_num_rx_o,
        pkt_err_cnt_o  => pkt_err_cnt_o,
        wrd_err_cnt_o  => wrd_err_cnt_o,

        HealthLane  => HealthLane,
        HealthInterface => HealthInterface(0)
    );






end architecture test;
