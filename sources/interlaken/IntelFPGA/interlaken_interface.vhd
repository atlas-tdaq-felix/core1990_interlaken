library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;


entity interlaken_interface is
    generic(
        BurstMax     : positive;    -- Configurable value of BurstMax
        BurstShort   : positive;    -- Configurable value of BurstShort
        MetaFrameLength : positive;    -- Configurable value of MetaFrameLength -- 24 packets * 8  = 192 B
        lanes        : positive;    -- Number of Lanes (Transmission channels)
        txlanes      : integer;
        rxlanes      : integer;
        BondNumberOfLanes : positive;
        CARD_TYPE    : integer;
        GTREFCLKS    : integer
    );
    port (
        reset : in std_logic;
        axis_rst_ext : in std_logic;
        interlaken_rst_ext : in std_logic;
        
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100 : in std_logic;

        ------------------- GT data in/out ------------------------------
        TX_Out_P  : out std_logic_vector(lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(lanes-1 downto 0);
		
		----Transmitter input/ready signals--------------
        TX_FlowControl    : in slv_16_array(0 to lanes-1);
        
        s_axis            : in axis_64_array_type(0 to lanes-1);
        --s_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1);

        s_axis_aclk       : in std_logic_vector(Lanes-1 downto 0);
        s_axis_tready     : out axis_tready_array_type(0 to Lanes-1);

        ----Receiver output signals-----------------------
        FlowControl	      : out slv_16_array(0 to Lanes-1);     -- Flow control data (yet unutilized)
        m_axis_aclk       : in std_logic_vector(Lanes-1 downto 0);
        m_axis            : out axis_64_array_type(0 to Lanes-1);
        m_axis_tready     : in axis_tready_array_type(0 to Lanes-1);
        m_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1);
		
		--- Debug signals (direct core in and output data)-----
		m_axis_deburst    : out axis_64_array_type(0 to lanes-1);
		m_axis_burst      : out axis_64_array_type(0 to lanes-1);
		
		s_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        m_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        
        tx_user_clk_out   : out std_logic_vector(lanes-1 downto 0);
        rx_user_clk_out   : out std_logic_vector(lanes-1 downto 0);

        ------------------Receiver status signals-----------------------
        Decoder_Lock      : out std_logic_vector(lanes-1 downto 0);
        Descrambler_lock  : out std_logic_vector(lanes-1 downto 0);
        
        ------------------Receiver error signals-----------------------
        decoder_error_sync              : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_badsync       : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_statemismatch : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_nosync        : out std_logic_vector(lanes-1 downto 0);
        burst_crc24_error               : out std_logic_vector(lanes-1 downto 0);
        meta_crc32_error                : out std_logic_vector(lanes-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        HealthLane        : out std_logic_vector(lanes-1 downto 0);
        HealthInterface   : out std_logic_vector((lanes/BondNumberOfLanes)-1 downto 0)
		
	);
end entity interlaken_interface;

architecture interface of interlaken_interface is

    signal TX_User_Clock, RX_User_Clock : std_logic_vector(lanes-1 downto 0);
    
    signal RX_Datavalid_Out : std_logic_vector(lanes-1 downto 0);
    signal RX_Header_Out : slv_3_array(0 to lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(lanes-1 downto 0); -- @suppress "signal RX_Headervalid_Out is never read"

    --signal RX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
    signal TX_Gearboxready_Out : std_logic_vector(lanes-1 downto 0);
    signal TX_Header_In : slv_3_array(0 to lanes-1);
    --signal TX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"

    signal Data_Transceiver_In, Data_Transceiver_Out : slv_64_array(0 to lanes-1);
    --signal HealthInterface_s :std_logic(Lanes-1 downto 0);--not used yet
    signal Descrambler_Lock_s, Descrambler_Lock_tx : std_logic_vector(lanes-1 downto 0);

    signal TX_Data_out_s            : slv_67_array(0 to lanes-1);
    signal RX_Data_In_s             : slv_67_array(0 to lanes-1);

    signal rst_txusr_403M, rst_rxusr_403M : std_logic_vector(Lanes-1 downto 0);
    signal interlaken_rst : std_logic;

    signal tx_rdy : std_logic;

begin
    
    tx_user_clk_out <= TX_User_Clock;
    rx_user_clk_out <= RX_User_Clock;
    
    Interlaken_gty : entity work.interlaken_mgt
        port map (
            reset => reset,
				
            rst_txusr_403M_s => rst_txusr_403M,
            rst_rxusr_403M_s => rst_rxusr_403M,
				
            refclk => GTREFCLK_IN_P(0),
            --GTREFCLK_IN_N => GTREFCLK_IN_N,
            --clk100 => clk100,
				
            tx_serial => TX_Out_P(0),
            --TX_Out_N => TX_Out_N,
            rx_serial => RX_In_P(0),
            --RX_In_N => RX_In_N,
				
            tx_clk_out => TX_User_Clock(0),
            rx_clk_out => RX_User_Clock(0),
            --loopback_in => loopback_in,
				
            tx_parallel_data  => Data_Transceiver_In(0),
            rx_parallel_data => Data_Transceiver_Out(0),
				
            rx_data_valid => RX_Datavalid_Out(0),
	         tx_data_valid => tx_rdy,
               --RX_Headervalid_Out_s => RX_Headervalid_Out,
				
            TX_Gearboxready_Out => TX_Gearboxready_Out(0),
				
            rx_control => RX_Header_Out(0),
            tx_control => TX_Header_In(0),
				
				reconfig_clk => clk100
        );
        
		  RX_Headervalid_Out <= RX_Datavalid_Out;
		  
    ---- Reset logic ----
    reset_logic : entity work.interlaken_reset
    generic map (
            lanes => lanes
        )
        port map (
            clk100 => clk100, -- Free-running clock in
            sys_rst => reset, -- Global reset in
            axis_rst_ext => axis_rst_ext, -- External AXI-S reset in
            interlaken_rst_ext => interlaken_rst_ext, -- External core reset in
            s_axis_aclk => s_axis_aclk, -- AXI-S write clock in
            m_axis_aclk => m_axis_aclk, -- AXI-S read clock in
            s_axis_aresetn => s_axis_aresetn, -- AXI-S write reset out
            m_axis_aresetn => m_axis_aresetn, -- AXI-S read reset out
            interlaken_rst => interlaken_rst -- Core reset out
        );
        
    g_unbonded_channels: for i in 0 to (lanes/BondNumberOfLanes)-1 generate
        txlane: if txlanes > 0 generate
            ---------------------------- Transmitting side -----------------------------
            Interlaken_TX : entity work.Interlaken_Transmitter_multiChannel
            generic map(
                BurstMax => BurstMax, -- Configurable value of BurstMax
                BurstShort => BurstShort, -- Configurable value of BurstShort
                MetaFrameLength => MetaFrameLength, -- Configurable value of MetaFrameLength
                Lanes => BondNumberOfLanes
            )
            port map (
                clk   => TX_User_Clock(i),
                reset => rst_txusr_403M(i), -- reset,
                TX_Data_Out     => TX_Data_out_s(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1), --Data_Transceiver_In(i)(63 downto 0), -- 64 bits
                TX_Gearboxready => TX_Gearboxready_Out((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                FlowControl     => TX_FlowControl(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                HealthLane      => Descrambler_Lock_tx((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
    
                m_axis_burst  => m_axis_burst(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                s_axis_aclk   => s_axis_aclk(i),
                s_axis        => s_axis(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                s_axis_tready => s_axis_tready(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1)
            );
        end generate;
        txlane_disabled: if txlanes = 0 generate
            TX_Data_out_s <= (others => (others => '0'));
            s_axis_tready <= (others => '0');
            m_axis_burst  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
        end generate;
        
        rxlane : if rxlanes > 0 generate
            ---------------------------- Receiving side --------------------------------
            Interlaken_RX : entity work.Interlaken_Receiver_multiChannel
            generic map(
                MetaFrameLength    => MetaFrameLength,
                Lanes              => BondNumberOfLanes
            )

            port map (
                clk => RX_User_Clock(i),
                reset => rst_rxusr_403M(i), -- reset,
                RX_Data_In => RX_Data_In_s(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                FlowControl => FlowControl(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                RX_Datavalid => RX_Datavalid_Out((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                Bitslip => open, --Ignored, bitslip is handled by Transceiver_10g_64b67b_BLOCK_SYNC_SM
                m_axis_deburst    => m_axis_deburst(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_aclk       => m_axis_aclk(i),
                m_axis            => m_axis(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_tready     => m_axis_tready(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_prog_empty => m_axis_prog_empty(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                
                Descrambler_lock   => Descrambler_Lock_s((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                Decoder_Lock       => Decoder_Lock((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                
                decoder_error_sync => decoder_error_sync((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                descrambler_error_badsync       => descrambler_error_badsync((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                descrambler_error_statemismatch => descrambler_error_statemismatch((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                descrambler_error_nosync        => descrambler_error_nosync((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                burst_crc24_error  => burst_crc24_error((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                meta_crc32_error   => meta_crc32_error((i + 1) * BondNumberOfLanes - 1 downto i * BondNumberOfLanes),
                
                HealthLane       => HealthLane((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                HealthInterface  => HealthInterface(i)
            );
        end generate;
        rxlane_disabled: if rxlanes = 0 generate
            FlowControl <= (others => (others => '0'));
            m_axis_deburst <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis_prog_empty <= (others => '0');
            Descrambler_lock_s <= (others => '1'); --Set to 1 so the tx side wil just transmit
            Decoder_Lock <= (others => '0');
            HealthLane  <= (others => '1');
            HealthInterface <= (others => '0');
        end generate;
    end generate;
--
    g_lane_data: for i in 0 to Lanes-1 generate
--    
--        xpm_cdc_array_single_inst : xpm_cdc_array_single
--        generic map (
--            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
--            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
--            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
--            SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
--            WIDTH => 1   -- DECIMAL; range: 1-1024
--        )
--        port map (
--            dest_out(0) => Descrambler_Lock_tx(i),
--            dest_clk => TX_User_Clock(i), 
--            src_clk => RX_User_Clock(i),
--            src_in(0) => Descrambler_Lock_s(i) 
--        );

		  proc_cdc_des_lock : process(RX_User_Clock(i))
		  begin
		    if rising_edge(RX_User_Clock(i)) then
			   Descrambler_Lock_tx(i) <= Descrambler_Lock_s(i);
			 end if;
		  end process proc_cdc_des_lock;
    
        -- Map data from TX to the transceiver --
        Data_Transceiver_In(i) <= TX_Data_out_s(i)(63 downto 0);
        TX_Header_In(i) <= TX_Data_out_s(i)(66 downto 64);
        -- Map data from transceiver to RX --
        RX_Data_In_s(i)(63 downto 0) <= Data_Transceiver_Out(i);
        RX_Data_In_s(i)(66 downto 64) <= RX_Header_Out(i);
    end generate;

    Descrambler_lock <= Descrambler_Lock_s; --Descrambler_Lock_s;

end architecture interface;
