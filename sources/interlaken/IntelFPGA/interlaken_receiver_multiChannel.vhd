library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.interlaken_package.all;
use work.axi_stream_package.ALL;

entity Interlaken_Receiver_multiChannel is
    generic (
        MetaFrameLength  : positive;
        Lanes            : positive     -- Configurable value of Transmission channels/Lanes
    );
    port (
        clk   		     : in std_logic;
        reset 		     : in std_logic;
		
        RX_Data_In 	     : in slv_67_array(0 to Lanes-1);
        FlowControl	     : out slv_16_array(0 to Lanes-1);   -- Flow control data (yet unutilized)
        RX_Datavalid     : in std_logic_vector(Lanes-1 downto 0); -- From GTH Transceiver
        Bitslip          : out std_logic_vector(Lanes-1 downto 0);
        m_axis_deburst   : out axis_64_array_type(0 to Lanes-1);
        m_axis_aclk       : in std_logic;
        m_axis            : out axis_64_array_type(0 to Lanes-1);
        m_axis_tready     : in axis_tready_array_type(0 to Lanes-1);
        m_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1);
        Descrambler_lock : out std_logic_vector(Lanes-1 downto 0);
        Decoder_Lock : out std_logic_vector(Lanes-1 downto 0);
        decoder_error_sync              : out std_logic_vector(Lanes-1 downto 0);
        descrambler_error_badsync       : out std_logic_vector(Lanes-1 downto 0);
        descrambler_error_statemismatch : out std_logic_vector(Lanes-1 downto 0);
        descrambler_error_nosync        : out std_logic_vector(Lanes-1 downto 0);
        burst_crc24_error               : out std_logic_vector(Lanes-1 downto 0);
        meta_crc32_error                : out std_logic_vector(Lanes-1 downto 0);
        
        HealthLane : out std_logic_vector(Lanes-1 downto 0);
        HealthInterface: out std_logic
        
--        RX_Header_Out : in slv_3_array(0 to Lanes-1);
--        RX_Headervalid_Out : in std_logic_vector(Lanes-1 downto 0);
--        RX_Gearboxslip_In : out std_logic_vector(Lanes-1 downto 0);
--        gtwiz_reset_rx_done_out : in std_logic
        
    );
end entity Interlaken_Receiver_multiChannel;

architecture Receiver of Interlaken_Receiver_multiChannel is
    signal HealthInterface_s : std_logic_vector(Lanes-1 downto 0);  
    --signal insert_t_valid : std_logic_vector(Lanes-1 downto 0);
    --signal all_lanes_aligned: std_logic;
    --variable wait_for_lane_n :  integer;
    --signal Data_Meta_Out_s , ltc_data_in, Burst_data_in : std_logic_vector(Lanes*67-1 downto 0);
    --signal Data_valid_Meta_out_s, ltc_valid_in, Burst_valid_in : std_logic_vector(Lanes-1 downto 0); 
    --signal almost_full: std_logic_vector(Lanes-1 downto 0); 
    
    --signal lanesaligned : std_logic;
    --signal fifowrite, fiforead :  std_logic_vector(Lanes-1 downto 0); 
    --signal fifo_read_pause : std_logic;
    
    --type slv_4_array is array(natural range <>) of std_logic_vector(4 downto 0);
    --signal wr_data_count : slv_4_array(0 to Lanes-1);
    --signal rx_gearbox_reset : std_logic;
    signal axis : axis_64_array_type(0 to Lanes-1);
    
begin
    m_axis_deburst <= axis;
    
    HealthInterface_procc : process(HealthInterface_s)
    begin
        HealthInterface <= '1';
        for i in 0 to Lanes - 1 loop
            if (HealthInterface_s(i) = '0') then
                HealthInterface <= '0';
            end if;
        end loop;

    end process;
   --! FS: Removed this process, because it was using a single clock and lane synchronization is not needed without channel bonding. 
   --! SyncAlignLanes : process(clk)
   --!     variable linkup : std_logic;
   --! begin
   --!     if reset = '1' then 
   --!         LinkUp := '0';
   --!         lanesaligned <= '0';
   --!     elsif rising_edge(clk) then
   --!     
   --!         if linkup = '0' then -- Only trigger when not aligned yet (later stage multiple states? Sync detected on lanes -> stop because not aligned/sync on all lanes -> start/no sync -> do nothing)
   --!         LinkUp := '1';
   --!         for i in 0 to Lanes - 1 loop
   --!             if (RX_Data_In(i)(63 downto 0) /= SYNCHRONIZATION) then -- Check whether all words are simultaniously SYNC
   --!               LinkUp := '0';
   --!             end if;
   --!         end loop;
   --!         end if;
   --!         
   --!         if linkup = '1' then
   --!             lanesaligned <= '1';
   --!         else 
   --!             lanesaligned <= '0';
   --!         end if;
   --!         
   --!         fifo_read_pause <= '0'; 
   --!         for i in 0 to Lanes - 1 loop
   --!             --if ltc_valid_in(i) = '0' then
   --!             if Data_valid_Meta_out_s(i) = '0' and wr_data_count(i)(0) = '0' then -- Pause reading fifo data since an empty fifo and no data available will result in misaligned data
   --!                 fifo_read_pause <= '1';
   --!             end if;
   --!         end loop;
   --!         
   --!     end if;
   --! end process;
    

----- Instantiation of different Receiver Lanes. -----   
    g_lanes: for i in 0 to Lanes-1 generate      -- Generate RX Lanes (Channels)
        signal s_axis_aresetn : std_logic;
        signal reset_sync: std_logic;
        signal axis_tready  : std_logic;
    begin
    lane_rx: entity work.Interlaken_Receiver
            generic map (
                MetaFrameLength   => MetaFrameLength,
                LaneNumber        => i                  -- Current Lane (RX channel)
            )
            port map(
                clk => clk,
                reset => reset,
 
                RX_Data_In   => RX_Data_In(i)(66 downto 0),
                RX_Datavalid => RX_Datavalid(i),
                m_axis => axis(i),                         --: out axis_64_type;
                m_axis_tready => axis_tready,           --: in std_logic;
                Flowcontrol => FlowControl(i),
                Descrambler_lock => Descrambler_lock(i),
                Decoder_Lock => Decoder_Lock(i),
                decoder_error_sync              => decoder_error_sync(i),
                descrambler_error_badsync       => descrambler_error_badsync(i),
                descrambler_error_statemismatch => descrambler_error_statemismatch(i),
                descrambler_error_nosync        => descrambler_error_nosync(i),
                burst_crc24_error               => burst_crc24_error(i),
                meta_crc32_error                => meta_crc32_error(i),
                Bitslip => Bitslip(i),
                HealthLane => HealthLane(i),
                HealthInterface => HealthInterface_s(i)
            );

        s_axis_aresetn <= not reset;
          
--        s_axis_aresetn <= not reset_sync;
--        --Optional since Interlaken_reset also handles this
--        aresetn_sync: xpm_cdc_sync_rst generic map(
--            DEST_SYNC_FF => 2,
--            INIT => 1,
--            INIT_SYNC_FF => 1,
--            SIM_ASSERT_CHK => 0
--        )
--        port map(
--            src_rst => reset,
--            dest_clk => clk,
--            dest_rst => reset_sync
--        );

        fifo0 : entity work.Axis64Fifo
            generic map(
                DEPTH => 2048
            )
            port map(
                -- axi stream slave
                s_axis_aresetn    => s_axis_aresetn,            --: in std_logic;
                s_axis_aclk       => clk,                       --: in std_logic;
                s_axis            => axis(i),                      --: in axis_64_type;
                s_axis_tready     => axis_tready,               --: out std_logic;

                -- axi stream master
                m_axis_aclk       => m_axis_aclk,               --: in std_logic;
                m_axis            => m_axis(i),                 --: out axis_64_type;
                m_axis_tready     => m_axis_tready(i),          --: in std_logic;

                --Indication that the FIFO contains a block of data (for MUX).
                m_axis_prog_empty => m_axis_prog_empty(i)       --: out std_logic
            );
            
                   ------------------------------- RX Gearbox bitslip -- -------------------------------------
--        block_sync_sm_0_i  :  entity work.Transceiver_10g_64b67b_BLOCK_SYNC_SM
--            generic map
--            (
--                SH_CNT_MAX          => 64,
--                SH_INVALID_CNT_MAX  => 16
--                --Lanes               => Lanes
--            )
--            port map
--            (
--                -- User Interface
--                BLOCKSYNC_OUT             =>    open,
--                RXGEARBOXSLIP_OUT         =>    RX_Gearboxslip_In(i),
--                RXHEADER_IN               =>    RX_Header_Out(i),
--                RXHEADERVALID_IN          =>    RX_Headervalid_Out(i),
 
--                -- System Interface
--                USER_CLK                  =>    clk,
--                SYSTEM_RESET              =>    rx_gearbox_reset
--            ); 
            
--            rx_gearbox_reset <= reset or not gtwiz_reset_rx_done_out;
    end generate;
    
    

end architecture Receiver;
