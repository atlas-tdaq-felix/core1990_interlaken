
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity interlaken_reset is
    generic (
        lanes : positive
    );
    port (
        clk100 : in std_logic;
        sys_rst : in std_logic;
        axis_rst_ext : in std_logic;
        interlaken_rst_ext : in std_logic;
        s_axis_aclk : in std_logic_vector(lanes-1 downto 0);
        m_axis_aclk : in std_logic_vector(lanes-1 downto 0);
        s_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        m_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        interlaken_rst : out std_logic
    );
end interlaken_reset;

architecture reset_logic of interlaken_reset is
    signal m_axis_rst, s_axis_rst : std_logic_vector(lanes-1  downto 0);
    signal axis_rst, sys_rst_sync, axis_rst_sync, interlaken_rst_sync: std_logic;

begin
    
    interlaken_rst  <= sys_rst_sync or interlaken_rst_sync;
    axis_rst <= sys_rst_sync or axis_rst_sync;
    
	 sys_rst_sync <= sys_rst;
	 
	 interlaken_rst_sync <= interlaken_rst_ext;
	 axis_rst_sync <= axis_rst_sync;

    
    g_reset: for i in 0 to lanes-1 generate

		  m_axis_rst(i) <= axis_rst;
		  s_axis_rst(i) <= axis_rst;
        

    end generate;

    m_axis_aresetn <= not m_axis_rst;
    s_axis_aresetn <= not s_axis_rst;
    
end reset_logic;
