library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library altera_mf;
use altera_mf.altera_mf_components.all;
use work.axi_stream_package.ALL;

entity Axis64Fifo is
    generic (
        DEPTH : integer:=2048
        --CLOCKING_MODE : string := "independent_clock";
        --RELATED_CLOCKS : integer range 0 to 1 := 0;
       -- FIFO_MEMORY_TYPE : string := "auto";
        --PACKET_FIFO : string := "false"
    );
    port (
        -- axi stream slave
        s_axis_aresetn : in std_logic;
        s_axis_aclk : in std_logic;
        s_axis : in axis_64_type;
        s_axis_tready : out std_logic;

		-- axi stream master
        m_axis_aclk : in std_logic;
        m_axis : out axis_64_type;
        m_axis_tready : in std_logic;
		
		--Indication that the FIFO contains a block of data (for MUX).
        m_axis_prog_empty : out std_logic
    );
end Axis64Fifo;

architecture rtl of Axis64Fifo is
	------------------------------------------------------------------
	-- dcfifo parameterized megafunction component declaration
	-- Generated with 'clearbox' loader - do not edit
	------------------------------------------------------------------
	component dcfifo
		generic (
			add_ram_output_register	:	string := "OFF";
			add_usedw_msb_bit	:	string := "OFF";
			clocks_are_synchronized	:	string := "FALSE";
			delay_rdusedw	:	integer := 1;
			delay_wrusedw	:	integer := 1;
			intended_device_family	:	string := "unused";
			enable_ecc	:	string := "FALSE";
			lpm_numwords	:	integer;
			lpm_showahead	:	string := "OFF";
			lpm_width	:	integer;
			lpm_widthu	:	integer := 1;
			overflow_checking	:	string := "ON";
			rdsync_delaypipe	:	integer := 0;
			read_aclr_synch	:	string := "OFF";
			underflow_checking	:	string := "ON";
			use_eab	:	string := "ON";
			write_aclr_synch	:	string := "OFF";
			wrsync_delaypipe	:	integer := 0;
			lpm_hint	:	string := "UNUSED";
			lpm_type	:	string := "dcfifo"
		);
		port(
			aclr	:	in std_logic := '0';
			data	:	in std_logic_vector(lpm_width-1 downto 0);
			eccstatus	:	out std_logic_vector(2-1 downto 0);
			q	:	out std_logic_vector(lpm_width-1 downto 0);
			rdclk	:	in std_logic;
			rdempty	:	out std_logic;
			rdfull	:	out std_logic;
			rdreq	:	in std_logic;
			rdusedw	:	out std_logic_vector(lpm_widthu-1 downto 0);
			wrclk	:	in std_logic;
			wrempty	:	out std_logic;
			wrfull	:	out std_logic;
			wrreq	:	in std_logic;
			wrusedw	:	out std_logic_vector(lpm_widthu-1 downto 0)
		);
	end component;
	
	signal din, dout : std_logic_vector(64 downto 0);
	signal rst : std_logic;
	signal wr_empty, wr_full, wr_en : std_logic;
	signal rd_empty, rd_full, rd_en : std_logic;
	signal wr_count, rd_count : std_logic_vector(11-1 downto 0); --static lpm_widthu
	
begin

	fifo_altera : component dcfifo
	generic map(
		lpm_numwords => DEPTH,
		lpm_width => 65,
		lpm_widthu => 11,
		lpm_showahead	=> "ON"
	)
	port map(
		aclr	 => rst,
		data	 => din,
		eccstatus => open,
		q	=> dout,
		rdclk	=> m_axis_aclk,
		rdempty	=> rd_empty,
		rdfull => rd_full,
		rdreq	 => rd_en,
		rdusedw	=> rd_count,
		wrclk	=> s_axis_aclk,
		wrempty	=> wr_empty,
		wrfull	=> wr_full,
		wrreq	   => wr_en,
		wrusedw	=> wr_count
	);
	
	rst <= not s_axis_aresetn;
	din(63 downto 0) <= s_axis.tdata;
	din(64) <= s_axis.tlast;
	s_axis_tready <= not wr_full;
	
	wr_en <= s_axis.tvalid and not wr_full and not rst;
	rd_en <= m_axis_tready and (not rd_empty) and (not rst);
	
	m_axis.tdata <= dout(63 downto 0);
	m_axis.tlast <= dout(64);
	m_axis.tvalid <= not rd_empty;
	m_axis_prog_empty <= '1' when (to_integer(unsigned(wr_count)) <= 127) else '0';
	
end architecture;