
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xpm;
use xpm.vcomponents.all;

entity interlaken_reset is
    generic (
        lanes : positive
    );
    port (
        clk100 : in std_logic;
        sys_rst : in std_logic;
        axis_rst_ext : in std_logic;
        interlaken_rst_ext : in std_logic;
        s_axis_aclk : in std_logic_vector(lanes-1 downto 0);
        m_axis_aclk : in std_logic_vector(lanes-1 downto 0);
        s_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        m_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        interlaken_rst : out std_logic
    );
end interlaken_reset;

architecture reset_logic of interlaken_reset is
    signal m_axis_rst, s_axis_rst : std_logic_vector(lanes-1  downto 0);
    signal axis_rst, sys_rst_sync, axis_rst_sync, interlaken_rst_sync: std_logic;

begin
    
    interlaken_rst  <= sys_rst_sync or interlaken_rst_sync;
    axis_rst <= sys_rst_sync or axis_rst_sync;
    
    -- Sync reset signals to free-running 100 MHz
    INST_RST_SYS : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => sys_rst_sync, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => clk100,   -- 1-bit input: Destination clock.
        src_arst => sys_rst    -- 1-bit input: Source asynchronous reset signal.
    );
    
    INST_RST_INTLKN : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => interlaken_rst_sync, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => clk100,   -- 1-bit input: Destination clock.
        src_arst => interlaken_rst_ext    -- 1-bit input: Source asynchronous reset signal.
    );
    
    INST_RST_AXIS : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => axis_rst_sync, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => clk100,   -- 1-bit input: Destination clock.
        src_arst => axis_rst_ext    -- 1-bit input: Source asynchronous reset signal.
    );
    
    g_reset: for i in 0 to lanes-1 generate

        xpm_cdc_sync_rst_m_axis : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
            INIT => 1,           -- DECIMAL; 0=initialize synchronization registers to 0, 1=initialize synchronization registers to 1
            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        )
        port map (
            dest_rst => m_axis_rst(i), -- 1-bit output: src_rst synchronized to the destination clock domain. This output is registered.
            dest_clk => m_axis_aclk(i), -- 1-bit input: Destination clock.
            src_rst => axis_rst    -- 1-bit input: Source reset signal.
        );
        
        xpm_cdc_sync_rst_s_axis : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
            INIT => 1,           -- DECIMAL; 0=initialize synchronization registers to 0, 1=initialize synchronization registers to 1
            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        )
        port map (
            dest_rst => s_axis_rst(i), -- 1-bit output: src_rst synchronized to the destination clock domain. This output is registered.
            dest_clk => s_axis_aclk(i), -- 1-bit input: Destination clock.
            src_rst => axis_rst    -- 1-bit input: Source reset signal.
        );
        
    end generate;

    m_axis_aresetn <= not m_axis_rst;
    s_axis_aresetn <= not s_axis_rst;
    
end reset_logic;
