library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;

entity interlaken_interface is
    generic(
        BurstMax     : positive;    -- Configurable value of BurstMax
        BurstShort   : positive;    -- Configurable value of BurstShort
        MetaFrameLength : positive;    -- Configurable value of MetaFrameLength -- 24 packets * 8  = 192 B
        lanes        : positive;    -- Number of Lanes (Transmission channels)
        txlanes      : integer; -- Number of enabled TX lanes
        rxlanes      : integer; -- Number of enabled RX lanes
        CARD_TYPE    : integer; -- FPGA/Board type to detemine transceiver type 
        GTREFCLKS    : integer -- Amount of used transceiver REFCLKs
    );
    port (
    	-- Reset core signal
        reset : in std_logic;
        -- Reset AXI-S interface signals
        axis_rst_ext : in std_logic;
        -- Reset Interlaken and transceiver logic
        interlaken_rst_ext : in std_logic;
        
        -- Transceiver REFCLK - 156,25 MHz 
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -- Freerunning clock - 100 MHz
        clk100 : in std_logic;

        -- Transceiver data signals
        TX_Out_P  : out std_logic_vector(lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(lanes-1 downto 0);
		
        TX_FlowControl    : in slv_16_array(0 to lanes-1); -- External disable/enable channel through flow control
        
        s_axis            : in axis_64_array_type(0 to lanes-1); -- TX AXI-S data
        s_axis_aclk       : in std_logic_vector(Lanes-1 downto 0); -- TX AXI-S clock
        s_axis_tready     : out axis_tready_array_type(0 to Lanes-1); -- Interlaken logic ready for TX data

        FlowControl	      : out slv_16_array(0 to Lanes-1);     -- Flow control data (yet unutilized)
        m_axis_aclk       : in std_logic_vector(Lanes-1 downto 0); -- RX AXI-S clock
        m_axis            : out axis_64_array_type(0 to Lanes-1); -- RX AXI-S data
        m_axis_tready     : in axis_tready_array_type(0 to Lanes-1); -- External logic ready for RX data
        m_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1); -- RX FIFO empty
		
        m_axis_deburst    : out axis_64_array_type(0 to lanes-1); -- DEBUG - RX data before RX FIFO
        m_axis_burst      : out axis_64_array_type(0 to lanes-1); -- DEBUG -- TX data after TX FIFO
		
        s_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        m_axis_aresetn : out std_logic_vector(lanes-1 downto 0);
        
        tx_user_clk_out   : out std_logic_vector(lanes-1 downto 0); -- TX Interlaken logic clock out
        rx_user_clk_out   : out std_logic_vector(lanes-1 downto 0); -- RX Interlaken logic clock out
        
        rx_usr_clk_freq    : out slv_array(0 to lanes-1)(31 downto 0);
        tx_usr_clk_freq    : out slv_array(0 to lanes-1)(31 downto 0);
        tx_data_rate_value : out slv_array(0 to lanes-1)(63 downto 0);
        rx_data_rate_value : out slv_array(0 to lanes-1)(63 downto 0);

        ------------------Receiver status signals-----------------------
        Decoder_Lock      : out std_logic_vector(lanes-1 downto 0);
        Descrambler_lock  : out std_logic_vector(lanes-1 downto 0);
        
        ------------------Receiver error signals-----------------------
        decoder_error_sync              : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_badsync       : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_statemismatch : out std_logic_vector(lanes-1 downto 0);
        descrambler_error_nosync        : out std_logic_vector(lanes-1 downto 0);
        burst_crc24_error               : out std_logic_vector(lanes-1 downto 0);
        meta_crc32_error                : out std_logic_vector(lanes-1 downto 0);
        crc24_error_count               : out slv_array(0 to lanes-1)(15 downto 0);
        crc32_error_count               : out slv_array(0 to lanes-1)(15 downto 0);
        error_truncation                : out std_logic_vector(lanes-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        axis_burst_mode   : in std_logic_vector(lanes-1 downto 0);
        HealthLane        : out std_logic_vector(lanes-1 downto 0);
        HealthInterface   : out std_logic_vector(lanes-1 downto 0)
		
	);
end entity interlaken_interface;

architecture interface of interlaken_interface is

    signal TX_User_Clock, RX_User_Clock : std_logic_vector(lanes-1 downto 0);
    
    signal RX_Datavalid_Out : std_logic_vector(lanes-1 downto 0);
    signal RX_Header_Out : slv_3_array(0 to lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(lanes-1 downto 0); -- @suppress "signal RX_Headervalid_Out is never read"

    --signal RX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
    signal TX_Gearboxready_Out : std_logic_vector(lanes-1 downto 0);
    signal TX_Header_In : slv_3_array(0 to lanes-1);
    --signal TX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"

    signal Data_Transceiver_In, Data_Transceiver_Out : slv_64_array(0 to lanes-1);
    --signal HealthInterface_s :std_logic(Lanes-1 downto 0);--not used yet
    signal Descrambler_Lock_s, Descrambler_Lock_tx : std_logic_vector(lanes-1 downto 0);

    signal TX_Data_out_s            : slv_67_array(0 to lanes-1);
    signal RX_Data_In_s             : slv_67_array(0 to lanes-1);

    signal rst_txusr_403M, rst_rxusr_403M : std_logic_vector(Lanes-1 downto 0);
    signal interlaken_rst : std_logic;
    
    signal tx_dvalid, rx_dvalid : std_logic_vector(lanes-1 downto 0);
    

begin
    
    tx_user_clk_out <= TX_User_Clock;
    rx_user_clk_out <= RX_User_Clock;
    
    Interlaken_gty : entity work.interlaken_gty
        generic map(
            Lanes => lanes,
            CARD_TYPE    => CARD_TYPE,
            GTREFCLKS    => GTREFCLKS
        )
        port map (
            reset => interlaken_rst,
            rst_txusr_403M_s => rst_txusr_403M,
            rst_rxusr_403M_s => rst_rxusr_403M,
            GTREFCLK_IN_P => GTREFCLK_IN_P,
            GTREFCLK_IN_N => GTREFCLK_IN_N,
            clk100 => clk100,
            TX_Out_P => TX_Out_P,
            TX_Out_N => TX_Out_N,
            RX_In_P => RX_In_P,
            RX_In_N => RX_In_N,
            TX_User_Clock_s => TX_User_Clock,
            RX_User_Clock_s => RX_User_Clock,
            loopback_in => loopback_in,
            Data_Transceiver_In  => Data_Transceiver_In,
            Data_Transceiver_Out => Data_Transceiver_Out,
            RX_Datavalid_Out => RX_Datavalid_Out,
            RX_Header_Out_s => RX_Header_Out,
            RX_Headervalid_Out_s => RX_Headervalid_Out,
            TX_Gearboxready_Out => TX_Gearboxready_Out,
            TX_Header_In => TX_Header_In
        );
        
    ---- Reset logic ----
    reset_logic : entity work.interlaken_reset
    generic map (
            lanes => lanes
        )
        port map (
            clk100 => clk100, -- Free-running clock in
            sys_rst => reset, -- Global reset in
            axis_rst_ext => axis_rst_ext, -- External AXI-S reset in
            interlaken_rst_ext => interlaken_rst_ext, -- External core reset in
            s_axis_aclk => s_axis_aclk, -- AXI-S write clock in
            m_axis_aclk => m_axis_aclk, -- AXI-S read clock in
            s_axis_aresetn => s_axis_aresetn, -- AXI-S write reset out
            m_axis_aresetn => m_axis_aresetn, -- AXI-S read reset out
            interlaken_rst => interlaken_rst -- Core reset out
        );
        
    g_unbonded_channels: for i in 0 to (lanes-1) generate
        txlane: if txlanes > 0 generate
            ---------------------------- Transmitting side -----------------------------
            Interlaken_TX : entity work.interlaken_transmitter_channel
            generic map(
                BurstMax => BurstMax, -- Configurable value of BurstMax
                BurstShort => BurstShort, -- Configurable value of BurstShort
                MetaFrameLength => MetaFrameLength -- Configurable value of MetaFrameLength
            )
            port map (
                clk   => TX_User_Clock(i),
                reset => rst_txusr_403M(i), -- reset,
                TX_Data_Out     => TX_Data_out_s(i), --Data_Transceiver_In(i)(63 downto 0), -- 64 bits
                TX_Gearboxready => TX_Gearboxready_Out(i),
                FlowControl     => TX_FlowControl(i),
                HealthLane      => Descrambler_Lock_tx(i),
                axis_burst_mode => axis_burst_mode(i),
    
                m_axis_burst  => m_axis_burst(i),
                s_axis_aclk   => s_axis_aclk(i),
                s_axis        => s_axis(i),
                s_axis_tready => s_axis_tready(i)
            );
        end generate;
        txlane_disabled: if txlanes = 0 generate
            TX_Data_out_s <= (others => (others => '0'));
            s_axis_tready <= (others => '0');
            m_axis_burst  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
        end generate;
        
        rxlane : if rxlanes > 0 generate
            ---------------------------- Receiving side --------------------------------
            Interlaken_RX : entity work.interlaken_receiver_channel
            generic map(
                MetaFrameLength    => MetaFrameLength
            )

            port map (
                clk => RX_User_Clock(i),
                reset => rst_rxusr_403M(i), -- reset,
                RX_Data_In => RX_Data_In_s(i),
                FlowControl => FlowControl(i),
                RX_Datavalid => RX_Datavalid_Out(i),
                Bitslip => open, --Ignored, bitslip is handled by Transceiver_10g_64b67b_BLOCK_SYNC_SM
                m_axis_deburst    => m_axis_deburst(i),
                m_axis_aclk       => m_axis_aclk(i),
                m_axis            => m_axis(i),
                m_axis_tready     => m_axis_tready(i),
                m_axis_prog_empty => m_axis_prog_empty(i),
                
                Descrambler_lock   => Descrambler_Lock_s(i),
                Decoder_Lock       => Decoder_Lock(i),
                
                decoder_error_sync => decoder_error_sync(i),
                descrambler_error_badsync       => descrambler_error_badsync(i),
                descrambler_error_statemismatch => descrambler_error_statemismatch(i),
                descrambler_error_nosync        => descrambler_error_nosync(i),
                burst_crc24_error  => burst_crc24_error(i),
                meta_crc32_error   => meta_crc32_error(i),
                crc24_error_count  => crc24_error_count(i), 
                crc32_error_count  => crc32_error_count(i),
                error_truncation   => error_truncation(i),
                
                HealthLane       => HealthLane(i),
                HealthInterface  => HealthInterface(i)
            );
        end generate;
        rxlane_disabled: if rxlanes = 0 generate
            FlowControl <= (others => (others => '0'));
            m_axis_deburst <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis_prog_empty <= (others => '0');
            Descrambler_lock_s <= (others => '1'); --Set to 1 so the tx side wil just transmit
            Decoder_Lock <= (others => '0');
            HealthLane  <= (others => '1');
            HealthInterface <= (others => '0');
        end generate;
    end generate;

    g_lane_data: for i in 0 to Lanes-1 generate
    
        xpm_cdc_array_single_inst : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
            WIDTH => 1   -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out(0) => Descrambler_Lock_tx(i),
            dest_clk => TX_User_Clock(i), 
            src_clk => RX_User_Clock(i),
            src_in(0) => Descrambler_Lock_s(i) 
        );
    
        -- Map data from TX to the transceiver --
        Data_Transceiver_In(i) <= TX_Data_out_s(i)(63 downto 0);
        TX_Header_In(i) <= TX_Data_out_s(i)(66 downto 64);
        -- Map data from transceiver to RX --
        RX_Data_In_s(i)(63 downto 0) <= Data_Transceiver_Out(i);
        RX_Data_In_s(i)(66 downto 64) <= RX_Header_Out(i);
    end generate;

    Descrambler_lock <= Descrambler_lock_s; --Descrambler_Lock_s;
    
    statistics : entity work.interlaken_statistics
    generic map(
        clk_sys_freq  => 100000000,
        lanes         => lanes
    )
    port map(
        clk_sys_in => clk100,
        tx_usr_clk => tx_user_clock,
        rx_usr_clk => rx_user_clock,
        tx_dvalid  => tx_dvalid,
        rx_dvalid  => rx_dvalid,
        rx_usr_clk_freq => rx_usr_clk_freq,
        tx_usr_clk_freq => tx_usr_clk_freq,
        tx_data_rate_value => tx_data_rate_value,
        rx_data_rate_value => rx_data_rate_value
    );
    
    g_datavalid: for i in 0 to Lanes-1 generate 
    begin
        tx_dvalid(i) <= s_axis(i).tvalid and s_axis_tready(i);
        rx_dvalid(i) <= m_axis(i).tvalid and m_axis_tready(i);
    end generate;

end architecture interface;
