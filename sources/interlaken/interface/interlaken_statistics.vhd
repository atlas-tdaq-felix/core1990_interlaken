library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;

entity interlaken_statistics is
    generic (
        clk_sys_freq  : integer;
        lanes         : integer
    );
    port ( 
        clk_sys_in : in std_logic;
        rx_usr_clk : in std_logic_vector(lanes-1 downto 0);
        tx_usr_clk : in std_logic_vector(lanes-1 downto 0);
        tx_dvalid  : in std_logic_vector(lanes-1 downto 0);
        rx_dvalid  : in std_logic_vector(lanes-1 downto 0);
        rx_usr_clk_freq : out slv_array(0 to lanes-1)(31 downto 0);
        tx_usr_clk_freq : out slv_array(0 to lanes-1)(31 downto 0);
        tx_data_rate_value : out slv_array(0 to lanes-1)(63 downto 0);
        rx_data_rate_value : out slv_array(0 to lanes-1)(63 downto 0)
  );
end interlaken_statistics;

architecture Behavioral of interlaken_statistics is

    signal rx_usr_clk_freq_ch : std_logic_vector(5 downto 0);
    --signal rx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal rx_usr_clk_freq_val: std_logic_vector(lanes-1 downto 0);
    
    signal tx_usr_clk_freq_ch : std_logic_vector(5 downto 0);
    --signal tx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal tx_usr_clk_freq_val: std_logic_vector(lanes-1 downto 0);
    
    signal tx_data_rate_ch, rx_data_rate_ch : std_logic_vector(5 downto 0);
    --signal tx_data_rate_value, rx_data_rate_value : slv_array(0 to lanes-1)(63 downto 0);
    signal tx_data_rate_valid, rx_data_rate_valid : std_logic_vector(lanes-1 downto 0);
   
begin
        -- Measure the clock frequency per lane
    rx_clock_freq : entity work.gc_multichannel_frequency_meter
    generic map(
        g_CLK_SYS_FREQ           => 100000000,
        g_CHANNELS               => lanes
    )
    port map (
        clk_sys_i     => clk_sys_in,
        clk_in_i      => rx_usr_clk,
        rst_n_i       => '1',
        pps_p1_i      => '0', 
        channel_sel_i => rx_usr_clk_freq_ch,
        freq_o        => rx_usr_clk_freq,
        freq_valid_o  => rx_usr_clk_freq_val
    );
    
    -- Measure the clock frequency per lane
    tx_clock_freq : entity work.gc_multichannel_frequency_meter
    generic map(
        g_CLK_SYS_FREQ           => 100000000,
        g_CHANNELS               => lanes
    )
    port map (
        clk_sys_i     => clk_sys_in,
        clk_in_i      => tx_usr_clk,
        rst_n_i       => '1',
        pps_p1_i      => '0', 
        channel_sel_i => tx_usr_clk_freq_ch,
        freq_o        => tx_usr_clk_freq,
        freq_valid_o  => tx_usr_clk_freq_val
    );
    

----    -- Measure the data rate per lane
    tx_datarate : entity work.datarate_meter
    generic map(
        g_WITH_INTERNAL_TIMEBASE => true,
        g_CLK_SYS_FREQ           => 100000000,
        g_COUNTER_BITS           => 64,
        g_CHANNELS               => lanes
    )
    port map (
        clk_sys_i     => clk_sys_in,
        clk_in_i      => tx_usr_clk,
        rst_n_i       => '1',
        pps_p1_i      => '0', 
        dvalid_i      => tx_dvalid,
        channel_sel_i => tx_data_rate_ch,
        freq_o        => tx_data_rate_value,
        freq_valid_o  => tx_data_rate_valid
    );
    
    rx_datarate : entity work.datarate_meter
    generic map(
        g_WITH_INTERNAL_TIMEBASE => true,
        g_CLK_SYS_FREQ           => 100000000,
        g_COUNTER_BITS           => 64,
        g_CHANNELS               => lanes
    )
    port map (
        clk_sys_i     => clk_sys_in,
        clk_in_i      => rx_usr_clk,
        rst_n_i       => '1',
        pps_p1_i      => '0', 
        dvalid_i      => rx_dvalid,
        channel_sel_i => rx_data_rate_ch,
        freq_o        => rx_data_rate_value,
        freq_valid_o  => rx_data_rate_valid
    );

end Behavioral;
