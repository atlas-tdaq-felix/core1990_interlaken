----------------------------------------------------------------------------------
-- Company: 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;
library xpm;
use xpm.vcomponents.all;

entity interlaken_top is
    generic (
    	-- Number of lanes required
        lanes        : positive := 4;
        -- Number of REFCLKs required
        GTREFCLKS    : integer := 1
    );
    port(
        -- Freerunning clock - 100 MHz
        freerun_clk_p : in std_logic;
        freerun_clk_n : in std_logic;
        
        -- Transceiver REFCLK - 156,25 MHz 
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -- External reset
        sys_rst : in std_logic;
        
        qsfp_resetl_ls : out std_logic_vector(lanes/4-1 downto 0);        
        qsfp_lpmode_ls : out std_logic_vector(lanes/4-1 downto 0);
    
        -- Transceiver data signals
        gt_rx_n : in std_logic_vector(lanes-1 downto 0);
        gt_rx_p : in std_logic_vector(lanes-1 downto 0);
        gt_tx_n : out std_logic_vector(lanes-1 downto 0);
        gt_tx_p : out std_logic_vector(lanes-1 downto 0)
    
    );
end entity interlaken_top;

architecture Test of interlaken_top is
    
    signal TX_Out_P_s        : std_logic_vector(lanes-1  downto 0);
    signal TX_Out_N_s        : std_logic_vector(lanes-1  downto 0);
    signal RX_In_P_s         : std_logic_vector(lanes-1  downto 0);
    signal RX_In_N_s         : std_logic_vector(lanes-1  downto 0);
    signal clk100            : std_logic;

    signal clk150            : std_logic;
    signal clk300            : std_logic;
    signal m_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal m_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_tready     : axis_tready_array_type(0 to Lanes-1);
    signal s_axis            : axis_64_array_type(0 to Lanes-1);
    signal s_axis_aresetn    : std_logic_vector(lanes-1 downto 0);
    signal s_axis_aclk       : std_logic_vector(Lanes-1 downto 0);
    signal s_axis_tready     : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal s_axis_tready is never read"
    signal m_axis            : axis_64_array_type(0 to Lanes-1);        -- @suppress "signal m_axis is never read"
    signal m_axis_prog_empty : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal m_axis_prog_empty is never read"
    signal Decoder_lock      : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit-- @suppress "signal Decoder_lock is never read"
    signal HealthLane        : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal HealthLane is never read"
    signal HealthInterface   : std_logic_vector(lanes-1 downto 0);
    signal Descrambler_lock  : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal Descrambler_lock is never read"
    signal Channel           : std_logic_vector(7 downto 0);            --TODO use as status bit -- @suppress "signal Channel is never read"
    signal stat_rx_aligned   : STD_LOGIC;  

    signal decoder_error_sync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal decoder_error_sync is never read"
    signal descrambler_error_badsync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_badsync is never read"
    signal descrambler_error_statemismatch : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_statemismatch is never read"
    signal descrambler_error_nosync : std_logic_vector(lanes-1 downto 0); -- @suppress "signal descrambler_error_nosync is never read"
    signal burst_crc24_error : std_logic_vector(lanes-1 downto 0); -- @suppress "signal burst_crc24_error is never read" -- @suppress "signal meta_crc32_error is never read"
    signal meta_crc32_error  : std_logic_vector(lanes-1 downto 0); -- @suppress "signal meta_crc32_error is never read"

    signal tx_user_clk_out, rx_user_clk_out : std_logic_vector(Lanes-1 downto 0);
    signal m_axis_burst, m_axis_deburst : axis_64_array_type(0 to Lanes - 1);
    
    signal latency_o : slv_16_array(lanes-1 downto 0);
    signal valid_o   : std_logic_vector(lanes-1 downto 0);
    
    signal count_rx_o : slv_32_array(lanes-1 downto 0);
    signal packet_num_rx_o : slv_32_array(lanes-1 downto 0);
    signal pkt_err_cnt_o : slv_16_array(lanes-1 downto 0);
    signal wrd_err_cnt_o : slv_16_array(lanes-1 downto 0); 

    signal axis_rst_ext, interlaken_rst_ext : std_logic;
    signal loopback : std_logic_vector(2 downto 0);
    signal axis_burst_mode : std_logic_vector(lanes-1 downto 0); -- configures burst mode. 0 = burstmax/short; 1 = burst config ignored, only axis tlast triggers eop/sop
    
    signal data_length_i : slv_array(0 to lanes-1)(15 downto 0);
    signal packet_length_i : slv_array(0 to lanes-1)(15 downto 0);
    
    signal rx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal tx_usr_clk_freq    : slv_array(0 to lanes-1)(31 downto 0);
    signal tx_data_rate_value    : slv_array(0 to lanes-1)(63 downto 0);
    signal rx_data_rate_value    : slv_array(0 to lanes-1)(63 downto 0);
    
    
    
    signal crc24_error_count : slv_array(0 to lanes-1)(15 downto 0);
    signal crc32_error_count : slv_array(0 to lanes-1)(15 downto 0);
    signal error_truncation  : std_logic_vector(lanes-1 downto 0);
    
    COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        trig_out : OUT STD_LOGIC;
        trig_out_ack : IN STD_LOGIC;
        trig_in : IN STD_LOGIC;
        trig_in_ack : OUT STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;

    COMPONENT ila_1
    PORT (
        clk : IN STD_LOGIC;
        trig_out : OUT STD_LOGIC;
        trig_out_ack : IN STD_LOGIC;
        trig_in : IN STD_LOGIC;
        trig_in_ack : OUT STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe9 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe11 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        probe12 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
        probe13 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
    END COMPONENT  ;
    
    COMPONENT ila_2
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe9 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
        probe10 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
        
    COMPONENT vio_0
      PORT (
        clk : IN STD_LOGIC;
        probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out2 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0) 
      );
    END COMPONENT;
    
    COMPONENT vio_1
      PORT (
        clk : IN STD_LOGIC;
        probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        probe_out2 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0) 
      );
    END COMPONENT;
    
    COMPONENT vio_5
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe_in7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0) 
      );
    END COMPONENT;

    COMPONENT vio_6
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in3 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in5 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in6 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe_in7 : IN STD_LOGIC_VECTOR(63 DOWNTO 0) 
      );
    END COMPONENT;
    
begin
    
    qsfp_lpmode_ls <= (others => '0');
    qsfp_resetl_ls <= (others => '1');
    
    ---- 100MHz clock DS to SE
    IBUFDS_inst : IBUFDS -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE, IOSTANDARD"
        port map(
            O  => clk100,
            I  => freerun_clk_p,
            IB => freerun_clk_n
        );

    RX_In_N_s <= gt_rx_n;
    RX_In_P_s <= gt_rx_p;
    gt_tx_n   <= TX_Out_N_s;
    gt_tx_p   <= TX_Out_P_s;

	------- The Interlaken Interface -------
    interface : entity work.interlaken_interface
    generic map(
         BurstMax     => 256, --(Bytes)
         BurstShort   => 64, --(Bytes)
         MetaFrameLength => 2048, --(Words)
         Lanes        => lanes,
         txlanes => 1,
         rxlanes => 1,
         CARD_TYPE => 128,
         GTREFCLKS => GTREFCLKS
    )
    port map(
            clk100 => clk100,
            reset  => sys_rst,
            axis_rst_ext => axis_rst_ext,
            interlaken_rst_ext => interlaken_rst_ext,            
            GTREFCLK_IN_P => GTREFCLK_IN_P,
            GTREFCLK_IN_N => GTREFCLK_IN_N,
            tx_user_clk_out => tx_user_clk_out, --only first lane for now
            rx_user_clk_out => rx_user_clk_out,
            TX_Out_P => TX_Out_P_s,
            TX_Out_N => TX_Out_N_s,
            RX_In_P  => RX_In_P_s,
            RX_In_N  => RX_In_N_s,
            TX_FlowControl => (others => (others => '0')),
            m_axis_burst  => m_axis_burst,
            m_axis_deburst  => m_axis_deburst,
            s_axis_aresetn => s_axis_aresetn,
            m_axis_aresetn => m_axis_aresetn,
            s_axis        => s_axis,
            s_axis_aclk   => s_axis_aclk, --tx_user_clk_out 
            s_axis_tready => s_axis_tready,
            FlowControl => open,
            m_axis_aclk   => m_axis_aclk, --rx_user_clk_out
            m_axis        => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            rx_usr_clk_freq => rx_usr_clk_freq,
            tx_usr_clk_freq => tx_usr_clk_freq,
            tx_data_rate_value => tx_data_rate_value,
            rx_data_rate_value => rx_data_rate_value,
            Decoder_Lock     => Decoder_lock,
            Descrambler_lock => Descrambler_lock,
            --Channel => Channel,
            loopback_in                     => loopback,
            axis_burst_mode                 => axis_burst_mode,
            HealthLane                      => HealthLane,
            HealthInterface                 => HealthInterface,
            decoder_error_sync              => decoder_error_sync,
            descrambler_error_badsync       => descrambler_error_badsync,
            descrambler_error_statemismatch => descrambler_error_statemismatch,
            descrambler_error_nosync        => descrambler_error_nosync,
            burst_crc24_error               => burst_crc24_error,
            meta_crc32_error                => meta_crc32_error,
            crc24_error_count               => crc24_error_count,
            crc32_error_count               => crc32_error_count,
            error_truncation                => error_truncation
        );
        
    s_axis_aclk <= tx_user_clk_out; 
    m_axis_aclk <= rx_user_clk_out; 
        
    ---- Generates input data and interface signals ----
    generate_data : entity work.axis_data_generator
    generic map (
        lanes => lanes
    )
    port map (
		s_axis_aclk => s_axis_aclk,
        m_axis_aclk => m_axis_aclk,
        s_axis_aresetn => s_axis_aresetn,
        m_axis_aresetn => m_axis_aresetn,
      
        s_axis => s_axis,
        s_axis_tready => s_axis_tready,

        m_axis            => m_axis,
        m_axis_tready     => m_axis_tready,
        m_axis_prog_empty => m_axis_prog_empty,
        
        data_length_i => data_length_i,
        packet_length_i => packet_length_i,
        
        latency_o  => latency_o,--latency_o,
        valid_o    => valid_o,
        count_rx_o => count_rx_o,  
        packet_num_rx_o => packet_num_rx_o,
        pkt_err_cnt_o  => pkt_err_cnt_o,
        wrd_err_cnt_o  => wrd_err_cnt_o,
        
        HealthLane  => HealthLane,
        HealthInterface => HealthInterface(0)
    );
    
    -- Clock and data rate VIO's - For now using channels 0-3 only
    clock_freq_vio : vio_5
    PORT MAP (
        clk => clk100,
        probe_in0 => rx_usr_clk_freq(0),
        probe_in1 => rx_usr_clk_freq(1),
        probe_in2 => rx_usr_clk_freq(2),
        probe_in3 => rx_usr_clk_freq(3),
        probe_in4 => tx_usr_clk_freq(0),
        probe_in5 => tx_usr_clk_freq(1),
        probe_in6 => tx_usr_clk_freq(2),
        probe_in7 => tx_usr_clk_freq(3)
    );
  
    data_rate_vio : vio_6
    PORT MAP (
        clk => clk100,
        probe_in0 => rx_data_rate_value(0),
        probe_in1 => rx_data_rate_value(1),
        probe_in2 => rx_data_rate_value(2),
        probe_in3 => rx_data_rate_value(3),
        probe_in4 => tx_data_rate_value(0),
        probe_in5 => tx_data_rate_value(1),
        probe_in6 => tx_data_rate_value(2),
        probe_in7 => tx_data_rate_value(3)
    );

    -- Reset VIO to access and trigger resets
    reset_vio : vio_0
    PORT MAP (
        clk => clk100,
        probe_out0(0) => interlaken_rst_ext,
        probe_out1(0) => axis_rst_ext,
        probe_out2    => loopback
    );
    
    
    -- Generate ILA for each lane to makes sure all lanes will be synthesized --
    g_lanes: for i in 0 to Lanes-1 generate 
        signal trig_out, trig_out_ack, trig_in, trig_in_ack: std_logic;
    
    begin
    
        control_vio : vio_1
          PORT MAP (
            clk => s_axis_aclk(i),
            probe_out0(0) => axis_burst_mode(i),
            probe_out1 => data_length_i(i),
            probe_out2 => packet_length_i(i)
          );
        
        -- ADD ILA 
        s_axis_ila : ila_0
        PORT MAP (
            clk => s_axis_aclk(i),
            trig_out => trig_out,
            trig_out_ack => trig_out_ack,
            trig_in => trig_in,
            trig_in_ack => trig_in_ack,
            
            probe0 => s_axis(i).tdata,
            probe1(0) => s_axis(i).tvalid,
            probe2(0) => s_axis(i).tlast,
            probe3 => s_axis(i).tkeep, 
            
            probe4 => m_axis_burst(i).tdata,  --64
            probe5(0) => m_axis_burst(i).tvalid, --1
            probe6(0) => m_axis_burst(i).tlast, --1
            probe7 => m_axis_burst(i).tkeep, --8
            
            probe8(0) => s_axis_tready(i) --1
        );
        
        m_axis_ila : ila_1
        PORT MAP (
            clk => m_axis_aclk(i),
            trig_out => trig_in,
            trig_out_ack => trig_in_ack,
            trig_in => trig_out,
            trig_in_ack => trig_out_ack,
            
            probe0 => m_axis(i).tdata, 
            probe1(0) =>  m_axis(i).tvalid, 
            probe2(0) => m_axis(i).tlast,
            probe3 => m_axis(i).tkeep, 
            
            probe4 => m_axis_deburst(i).tdata, --64
            probe5(0) => m_axis_deburst(i).tvalid, --1
            probe6(0) => m_axis_deburst(i).tlast, --1
            probe7 => m_axis_deburst(i).tkeep, --8
            
            probe8(0) => m_axis_tready(i), 
            probe9 => latency_o(i),
            probe10(0) => valid_o(i),
            probe11 => pkt_err_cnt_o(i),
            probe12 => wrd_err_cnt_o(i),
            probe13 => count_rx_o(i),
            probe14 => packet_num_rx_o(i)
        );
        
        status_ila : ila_2
        PORT MAP (
            clk => m_axis_aclk(i),
            probe0(0) => Decoder_lock(i), 
            probe1(0) => Descrambler_lock(i), 
            probe2(0) => HealthLane(i), 
            probe3(0) => decoder_error_sync(i), 
            probe4(0) => descrambler_error_badsync(i), 
            probe5(0) => descrambler_error_statemismatch(i), 
            probe6(0) => descrambler_error_nosync(i), 
            probe7(0) => burst_crc24_error(i), 
            probe8(0) => meta_crc32_error(i),
            probe9    => crc24_error_count(i),
            probe10   => crc32_error_count(i),
            probe11(0) => error_truncation(i)
        );
        
    end generate;
    
end architecture Test;