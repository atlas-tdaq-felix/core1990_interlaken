library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity htile_transceiver is
  port(
		system_reset_n : in std_logic;
		refclk_qsfp_p : in std_logic; -- changed from pin Y38 (644 MHz) to pin AF9 (156.25 MHz) -- reverted
		tx_transfer_ready_n : out std_logic;
		rx_transfer_ready_n : out std_logic;
		clk_fpga_100m : in std_logic;

		qsfp_rx_p : in std_logic_vector(0 downto 0);
		qsfp_tx_p : out std_logic_vector(0 downto 0);

		tx_user_clk : out std_logic_vector(0 downto 0);
		rx_user_clk : out std_logic_vector(0 downto 0);
		rx_parallel_data : out  std_logic_vector(63 downto 0);
		tx_parallel_data : in  std_logic_vector(63 downto 0);
		rx_enh_data_valid : out std_logic;
		rx_header : out std_logic_vector(2 downto 0);
		tx_fifo_pfull : out std_logic_vector(0 downto 0);
		tx_header : in std_logic_vector(2 downto 0);
		rx_bitslip : in std_logic
  );
end entity htile_transceiver;


architecture rtl of htile_transceiver is
--	signal qsfp_xcvr_atx_pll_locked1: std_logic;
	signal qsfp_xcvr_atx_pll_locked : std_logic_vector(0 downto 0);
--	signal sdi_xcvr_atx_pll_locked, sdi_test_0_xcvr_rx_is_lockedtoref : std_logic;
--	signal qsfp_test_xcvr_rx_is_lockedtoref : std_logic_vector(3 downto 0);

--	signal tx_scrambler_data, Data_Scrambler_Out, tx_encoder_out : std_logic_vector(66 downto 0);
--	
--	signal tx_parallel_data : std_logic_vector(63 downto 0);
--	signal tx_header : std_logic_vector(2 downto 0);
	signal tx_data_valid : std_logic;
	signal tx_unused : std_logic_vector(11 downto 0);
--
--	signal rx_parallel_data : std_logic_vector(63 downto 0);
--	signal rx_header : std_logic_vector(2 downto 0);
	signal rx_data_valid : std_logic; --Not routed outside entity since same signal als enh_data_valid
-- signal rx_enh_data_valid : std_logic;
	signal rx_unused : std_logic_vector(10 downto 0);
--	
--	signal system_reset_n, cpu_reset : std_logic;
--	signal datacnt : integer;-- range 0 to 63
--	
--	signal heart_beat_cnt : integer;--range 0 to 27
--	signal heart_beat_cnt_s : std_logic_vector(27 downto 0);
--	
--	signal rx_data_out : std_logic_vector(66 downto 0);
--	signal Data_Decoder_Out : std_logic_vector(66 downto 0);
--   signal Data_valid_decoder_out, Decoder_Lock, Error_Decoder_Sync: std_logic;
--	signal rx_bitslip : std_logic;
	signal tx_fifo_full, tx_transfer_ready, rx_transfer_ready, tx_fifo_ready, rx_fifo_ready, rx_fifo_rd_en : std_logic_vector(0 downto 0);
--	signal tx_fifo_full, tx_fifo_pfull, tx_transfer_ready, rx_transfer_ready, tx_fifo_ready, rx_fifo_ready, rx_fifo_rd_en : std_logic_vector(0 downto 0);
	signal tx_coreclkin_clk, rx_coreclkin_clk, tx_clkout_clk, rx_clkout_clk, tx_ready, rx_ready : std_logic_vector(0 downto 0);
	
	component test_sys is
        port (
            clock_156_in_clk                                       : in  std_logic                     := 'X';             -- clk
            clk_100_clk                                            : in  std_logic                     := 'X';             -- clk
            reset_100_reset_n                                      : in  std_logic                     := 'X';             -- reset_n
            pll_status_interconnect_0_pll_locked_output_pll_locked : out std_logic_vector(0 downto 0);                     -- pll_locked
            tx_transfer_ready_tx_transfer_ready                    : out std_logic_vector(0 downto 0);                     -- tx_transfer_ready
            rx_transfer_ready_rx_transfer_ready                    : out std_logic_vector(0 downto 0);                     -- rx_transfer_ready
            tx_fifo_ready_tx_fifo_ready                            : out std_logic_vector(0 downto 0);                     -- tx_fifo_ready
            rx_fifo_ready_rx_fifo_ready                            : out std_logic_vector(0 downto 0);                     -- rx_fifo_ready
            tx_serial_data_tx_serial_data                          : out std_logic_vector(0 downto 0);                     -- tx_serial_data
            rx_serial_data_rx_serial_data                          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
            tx_coreclkin_clk                                       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            rx_coreclkin_clk                                       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
            tx_clkout_clk                                          : out std_logic_vector(0 downto 0);                     -- clk
            rx_clkout_clk                                          : out std_logic_vector(0 downto 0);                     -- clk
            tx_parallel_data_tx_parallel_data                      : in  std_logic_vector(63 downto 0) := (others => 'X'); -- tx_parallel_data
            tx_header_tx_control                                   : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- tx_control
            tx_enh_data_valid_tx_enh_data_valid                    : in  std_logic                     := 'X';             -- tx_enh_data_valid
            unused_tx_parallel_data_unused_tx_parallel_data        : in  std_logic_vector(11 downto 0) := (others => 'X'); -- unused_tx_parallel_data
            rx_parallel_data_rx_parallel_data                      : out std_logic_vector(63 downto 0);                    -- rx_parallel_data
            rx_header_rx_control                                   : out std_logic_vector(2 downto 0);                     -- rx_control
            rx_enh_data_valid_rx_enh_data_valid                    : out std_logic;                                        -- rx_enh_data_valid
            rx_data_valid_rx_data_valid                            : out std_logic;                                        -- rx_data_valid
            unused_rx_parallel_data_unused_rx_parallel_data        : out std_logic_vector(10 downto 0);                    -- unused_rx_parallel_data
            rx_bitslip_rx_bitslip                                  : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_bitslip
            tx_fifo_full_tx_fifo_full                              : out std_logic_vector(0 downto 0);                     -- tx_fifo_full
            tx_fifo_pfull_tx_fifo_pfull                            : out std_logic_vector(0 downto 0);                     -- tx_fifo_pfull
            rx_fifo_rd_en_rx_fifo_rd_en                            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_fifo_rd_en
				reset_control_tx_ready_tx_ready                        : out std_logic_vector(0 downto 0);                     -- tx_ready
            reset_control_rx_ready_rx_ready                        : out std_logic_vector(0 downto 0)                      -- rx_ready
        );
    end component test_sys;
 
begin

--            system_reset_n => reset, 
--            tx_transfer_ready_n => rst_txusr_403M, --invert
--            rx_transfer_ready_n => rst_rxusr_403M, --invert
--            refclk_qsfp_p => GTREFCLK_IN_P,
--            refclk_qsfp_n => GTREFCLK_IN_N, -- not assigned
--            clk_fpga_100m => clk100,
--				
--            qsfp_tx_p => TX_Out_P,
--            qsfp_tx_p => TX_Out_N, -- not assigned
--            qsfp_rx_p => RX_In_P,
--            qsfp_rx_n => RX_In_N, -- not assigned
--				
--            tx_clkout_clk => TX_User_Clock,
--            rx_clkout_clk => RX_User_Clock,
--				
--            loopback_in => loopback_in, -- not assigned
--				
--            rx_parallel_data => Data_Transceiver_Out,
--            tx_parallel_data  => Data_Transceiver_In,
--            rx_enh_data_valid => RX_Datavalid_Out,
--            rx_header => RX_Header_Out,
--            rx_enh_data_valid => RX_Headervalid_Out,
--            tx_fifo_pfull(0) => TX_Gearboxready_Out,
--            tx_header => TX_Header_In,
--				rx_bitslip => rx_bitslip
				
	tx_transfer_ready_n <= not tx_transfer_ready(0);
	rx_transfer_ready_n <= not rx_transfer_ready(0);
	
	u0 : component test_sys
	port map (
		clk_100_clk                                  	=> clk_fpga_100m,                                  --              clk_100_in.clk
		clock_156_in_clk                                => refclk_qsfp_p,                                --            clock_156_in.clk
		reset_100_reset_n                               => system_reset_n,                                     --                   reset.reset
		pll_status_interconnect_0_pll_locked_output_pll_locked => qsfp_xcvr_atx_pll_locked, -- pll_status_interconnect_0_pll_locked_output.pll_locked
		
		tx_transfer_ready_tx_transfer_ready             => tx_transfer_ready,                    --                           tx_transfer_ready.tx_transfer_ready
      rx_transfer_ready_rx_transfer_ready             => rx_transfer_ready,                    --                           rx_transfer_ready.rx_transfer_ready
      tx_fifo_ready_tx_fifo_ready                     => tx_fifo_ready,                            --                               tx_fifo_ready.tx_fifo_ready
      rx_fifo_ready_rx_fifo_ready                     => rx_fifo_ready,                            --                               rx_fifo_ready.rx_fifo_ready
            
		tx_serial_data_tx_serial_data                   => qsfp_tx_p,                   --          tx_serial_data.tx_serial_data
		rx_serial_data_rx_serial_data                   => qsfp_rx_p,                   --          rx_serial_data.rx_serial_data
		tx_coreclkin_clk                                => tx_coreclkin_clk,                                --            tx_coreclkin.clk
		rx_coreclkin_clk                                => rx_coreclkin_clk,                                --            rx_coreclkin.clk

		tx_clkout_clk                                   => tx_clkout_clk,                                   --               tx_clkout.clk
		rx_clkout_clk                                   => rx_clkout_clk,                                   --               rx_clkout.clk
		tx_parallel_data_tx_parallel_data               => tx_parallel_data,               --        tx_parallel_data.tx_parallel_data
		tx_header_tx_control                            => tx_header,                            --               tx_header.tx_control
		tx_enh_data_valid_tx_enh_data_valid             => tx_data_valid,             --       tx_enh_data_valid.tx_enh_data_valid
		unused_tx_parallel_data_unused_tx_parallel_data => tx_unused, -- unused_tx_parallel_data.unused_tx_parallel_data
		
		rx_parallel_data_rx_parallel_data               => rx_parallel_data,               --        rx_parallel_data.rx_parallel_data
		rx_header_rx_control                            => rx_header,                            --               rx_header.rx_control
		
      rx_enh_data_valid_rx_enh_data_valid             => rx_enh_data_valid,             --       rx_enh_data_valid.rx_enh_data_valid
	   rx_data_valid_rx_data_valid                     => rx_data_valid,                            --                               rx_data_valid.rx_data_valid
		unused_rx_parallel_data_unused_rx_parallel_data => rx_unused,  -- unused_rx_parallel_data.unused_rx_parallel_data
	   
		rx_bitslip_rx_bitslip(0)                        => rx_bitslip,
		tx_fifo_full_tx_fifo_full                       => tx_fifo_full,                              --                                tx_fifo_full.tx_fifo_full
      tx_fifo_pfull_tx_fifo_pfull                     => tx_fifo_pfull,                            --                               tx_fifo_pfull.tx_fifo_pfull
		rx_fifo_rd_en_rx_fifo_rd_en                     => rx_fifo_rd_en,                            --                               rx_fifo_rd_en.rx_fifo_rd_en
		reset_control_tx_ready_tx_ready                 => tx_ready,                        --                      reset_control_tx_ready.tx_ready
      reset_control_rx_ready_rx_ready                 => rx_ready                         --                      reset_control_rx_ready.rx_ready
        
	);
	rx_fifo_rd_en(0) <= '1';
	tx_unused <= (others => '0');
	--rx_unused <= (others => '0');
	tx_coreclkin_clk <= tx_clkout_clk;
	rx_coreclkin_clk <= rx_clkout_clk;
	tx_user_clk <= tx_clkout_clk;
	rx_user_clk <= rx_clkout_clk;
	
	tx_data_valid <= '1';
	
--	counter : process(tx_clkout_clk) 
--	begin
--		if rising_edge(tx_clkout_clk(0)) then
--			if (cpu_resetn = '0') then
--				datacnt <= 0;
--				tx_data_valid <= '0';
--			else
--				if (tx_fifo_pfull(0) = '0') then
--					datacnt <= datacnt + 1;
--					tx_data_valid <= '1';
--			   else 
--					tx_data_valid <= '0';
--				end if;
--			end if;
--		end if;
--	end process;
	
--	rx_data_out <= rx_header & rx_parallel_data;
	
	--tx_parallel_data <=  std_logic_vector(to_unsigned(datacnt, tx_parallel_data'length)); --scrambler input
--	tx_scrambler_data <=  "001"  & std_logic_vector(to_unsigned(datacnt, tx_scrambler_data'length-3)); --scrambler input
--   tx_parallel_data <= tx_encoder_out(63 downto 0);
--	tx_header <= tx_encoder_out(66 downto 64);
--	cpu_reset <= not cpu_resetn;

end architecture rtl;