library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity transceiver_test is
  port(
    clk_fpga_100m : in std_logic;
    clk_50 : in std_logic;
    cpu_resetn : in std_logic;
    refclk_qsfp_p : in std_logic; -- changed from pin Y38 (644 MHz) to pin AF9 (156.25 MHz) -- reverted
    
    qsfp_rx_p : in std_logic_vector(0 downto 0);
    qsfp_tx_p : out std_logic_vector(0 downto 0);
 
    qsfp_interruptn : in std_logic;
    qsfp_mod_prsn : in std_logic;
    qsfp_scl: out std_logic;
    qsfp_sda: inout std_logic;
    qsfp_mod_seln: out std_logic;     
    qsfp_lp_mode: out std_logic;
    qsfp_rstn : out std_logic
  );
end entity transceiver_test;

architecture rtl of transceiver_test is
	signal qsfp_xcvr_atx_pll_locked : std_logic_vector(0 downto 0);

	signal tx_scrambler_data, Data_Scrambler_Out, tx_encoder_out : std_logic_vector(66 downto 0);
	
	signal tx_parallel_data : std_logic_vector(63 downto 0);
	signal tx_header : std_logic_vector(2 downto 0);
	signal tx_data_valid : std_logic;
	signal tx_unused : std_logic_vector(11 downto 0);

	signal rx_parallel_data : std_logic_vector(63 downto 0);
	signal rx_header : std_logic_vector(2 downto 0);
	signal rx_data_valid,rx_enh_data_valid : std_logic;
	signal rx_unused : std_logic_vector(10 downto 0);
	
	signal system_reset_n, cpu_reset : std_logic;
	signal datacnt : integer;-- range 0 to 63
	
	signal rx_data_out : std_logic_vector(66 downto 0);
	signal Data_Decoder_Out : std_logic_vector(66 downto 0);
   signal Data_valid_decoder_out, Decoder_Lock, Error_Decoder_Sync: std_logic;
	signal rx_bitslip : std_logic;
	
	signal tx_fifo_full, tx_fifo_pfull, tx_transfer_ready, rx_transfer_ready, tx_fifo_ready, rx_fifo_ready, rx_fifo_rd_en : std_logic_vector(0 downto 0);
	signal tx_clkout_clk, rx_clkout_clk, tx_coreclkin_clk, rx_coreclkin_clk, tx_ready, rx_ready : std_logic_vector(0 downto 0);
	
	signal tx_transfer_ready_n, rx_transfer_ready_n : std_logic;
--component test_sys is
--        port (
--            clock_156_in_clk                                       : in  std_logic                     := 'X';             -- clk
--            clk_100_clk                                            : in  std_logic                     := 'X';             -- clk
--            reset_100_reset_n                                      : in  std_logic                     := 'X';             -- reset_n
--            pll_status_interconnect_0_pll_locked_output_pll_locked : out std_logic_vector(0 downto 0);                     -- pll_locked
--            tx_transfer_ready_tx_transfer_ready                    : out std_logic_vector(0 downto 0);                     -- tx_transfer_ready
--            rx_transfer_ready_rx_transfer_ready                    : out std_logic_vector(0 downto 0);                     -- rx_transfer_ready
--            tx_fifo_ready_tx_fifo_ready                            : out std_logic_vector(0 downto 0);                     -- tx_fifo_ready
--            rx_fifo_ready_rx_fifo_ready                            : out std_logic_vector(0 downto 0);                     -- rx_fifo_ready
--            tx_serial_data_tx_serial_data                          : out std_logic_vector(0 downto 0);                     -- tx_serial_data
--            rx_serial_data_rx_serial_data                          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
--            tx_coreclkin_clk                                       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
--            rx_coreclkin_clk                                       : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
--            tx_clkout_clk                                          : out std_logic_vector(0 downto 0);                     -- clk
--            rx_clkout_clk                                          : out std_logic_vector(0 downto 0);                     -- clk
--            tx_parallel_data_tx_parallel_data                      : in  std_logic_vector(63 downto 0) := (others => 'X'); -- tx_parallel_data
--            tx_header_tx_control                                   : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- tx_control
--            tx_enh_data_valid_tx_enh_data_valid                    : in  std_logic                     := 'X';             -- tx_enh_data_valid
--            unused_tx_parallel_data_unused_tx_parallel_data        : in  std_logic_vector(11 downto 0) := (others => 'X'); -- unused_tx_parallel_data
--            rx_parallel_data_rx_parallel_data                      : out std_logic_vector(63 downto 0);                    -- rx_parallel_data
--            rx_header_rx_control                                   : out std_logic_vector(2 downto 0);                     -- rx_control
--            rx_enh_data_valid_rx_enh_data_valid                    : out std_logic;                                        -- rx_enh_data_valid
--            rx_data_valid_rx_data_valid                            : out std_logic;                                        -- rx_data_valid
--            unused_rx_parallel_data_unused_rx_parallel_data        : out std_logic_vector(10 downto 0);                    -- unused_rx_parallel_data
--            rx_bitslip_rx_bitslip                                  : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_bitslip
--            tx_fifo_full_tx_fifo_full                              : out std_logic_vector(0 downto 0);                     -- tx_fifo_full
--            tx_fifo_pfull_tx_fifo_pfull                            : out std_logic_vector(0 downto 0);                     -- tx_fifo_pfull
--            rx_fifo_rd_en_rx_fifo_rd_en                            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_fifo_rd_en
--				reset_control_tx_ready_tx_ready                        : out std_logic_vector(0 downto 0);                     -- tx_ready
--            reset_control_rx_ready_rx_ready                        : out std_logic_vector(0 downto 0)                      -- rx_ready
--        );
--    end component test_sys;
 
begin
  
  sysreset : process(clk_fpga_100m) 
  begin
    if rising_edge(clk_fpga_100m) then
		if (cpu_resetn = '0') then
		  system_reset_n <= '0';
		else
		  system_reset_n <= '1';
		end if;
	 end if;
  end process;
  
  hbcount : process(clk_fpga_100m)
  begin
	if rising_edge(clk_fpga_100m) then
		if (cpu_resetn = '0') then
		  heart_beat_cnt <= 0;
		else
		  heart_beat_cnt <= heart_beat_cnt + 1;
		end if;
	 end if;
  end process;
  
  heart_beat_cnt_s <=  std_logic_vector(to_unsigned(heart_beat_cnt, heart_beat_cnt_s'length));
  
	htile : entity work.htile_transceiver
	port map (
		system_reset_n => cpu_resetn, 
		tx_transfer_ready_n => tx_transfer_ready_n, --invert
		rx_transfer_ready_n => rx_transfer_ready_n, --invert
		refclk_qsfp_p => refclk_qsfp_p,
		--refclk_qsfp_n => GTREFCLK_IN_N, -- not assigned
		clk_fpga_100m => clk_fpga_100m,

		qsfp_tx_p => qsfp_tx_p,
		--qsfp_tx_p => TX_Out_N, -- not assigned
		qsfp_rx_p => qsfp_rx_p,
		--qsfp_rx_n => qsfp_rx_n, -- not assigned

		tx_user_clk => tx_clkout_clk,
		rx_user_clk => rx_clkout_clk,

		--loopback_in => loopback_in, -- not assigned

		rx_parallel_data => rx_parallel_data,
		tx_parallel_data  => tx_parallel_data,
		rx_enh_data_valid => rx_enh_data_valid,
		rx_header => rx_header,
		--rx_enh_data_valid => rx_enh_data_valid,
		tx_fifo_pfull(0) => tx_fifo_pfull(0),
		tx_header => tx_header,
		rx_bitslip => rx_bitslip
	);
	--tx_transfer_ready_n <= not tx_transfer_ready;
	--rx_transfer_ready_n <= not rx_transfer_ready;
	
	
--     system_reset_n => reset, 
--		tx_transfer_ready_n => rst_txusr_403M, --invert
--		rx_transfer_ready_n => rst_rxusr_403M, --invert
--		refclk_qsfp_p => GTREFCLK_IN_P,
--		refclk_qsfp_n => GTREFCLK_IN_N, -- not assigned
--		clk_fpga_100m => clk100,
--
--		qsfp_tx_p => TX_Out_P,
--		qsfp_tx_p => TX_Out_N, -- not assigned
--		qsfp_rx_p => RX_In_P,
--		qsfp_rx_n => RX_In_N, -- not assigned
--
--		tx_clkout_clk => TX_User_Clock,
--		rx_clkout_clk => RX_User_Clock,
--
--		loopback_in => loopback_in, -- not assigned
--
--		rx_parallel_data => Data_Transceiver_Out,
--		tx_parallel_data  => Data_Transceiver_In,
--		rx_enh_data_valid => RX_Datavalid_Out,
--		rx_header => RX_Header_Out,
--		rx_enh_data_valid => RX_Headervalid_Out,
--		tx_fifo_pfull(0) => TX_Gearboxready_Out,
--		tx_header => TX_Header_In,
--		rx_bitslip => rx_bitslip
		
--	u0 : component test_sys
--	port map (
--		clk_100_clk                                  	=> clk_fpga_100m,                                  --              clk_100_in.clk
--		clock_156_in_clk                                => refclk_qsfp_p,                                --            clock_156_in.clk
--		reset_100_reset_n                               => system_reset_n,                                     --                   reset.reset
--		pll_status_interconnect_0_pll_locked_output_pll_locked => qsfp_xcvr_atx_pll_locked, -- pll_status_interconnect_0_pll_locked_output.pll_locked
--		
--		tx_transfer_ready_tx_transfer_ready             => tx_transfer_ready,                    --                           tx_transfer_ready.tx_transfer_ready
--      rx_transfer_ready_rx_transfer_ready             => rx_transfer_ready,                    --                           rx_transfer_ready.rx_transfer_ready
--      tx_fifo_ready_tx_fifo_ready                     => tx_fifo_ready,                            --                               tx_fifo_ready.tx_fifo_ready
--      rx_fifo_ready_rx_fifo_ready                     => rx_fifo_ready,                            --                               rx_fifo_ready.rx_fifo_ready
--            
--		tx_serial_data_tx_serial_data                   => qsfp_tx_p,                   --          tx_serial_data.tx_serial_data
--		rx_serial_data_rx_serial_data                   => qsfp_rx_p,                   --          rx_serial_data.rx_serial_data
--		tx_coreclkin_clk                                => tx_coreclkin_clk,                                --            tx_coreclkin.clk
--		rx_coreclkin_clk                                => rx_coreclkin_clk,                                --            rx_coreclkin.clk
--
--		tx_clkout_clk                                   => tx_clkout_clk,                                   --               tx_clkout.clk
--		rx_clkout_clk                                   => rx_clkout_clk,                                   --               rx_clkout.clk
--		tx_parallel_data_tx_parallel_data               => tx_parallel_data,               --        tx_parallel_data.tx_parallel_data
--		tx_header_tx_control                            => tx_header,                            --               tx_header.tx_control
--		tx_enh_data_valid_tx_enh_data_valid             => tx_data_valid,             --       tx_enh_data_valid.tx_enh_data_valid
--		unused_tx_parallel_data_unused_tx_parallel_data => tx_unused, -- unused_tx_parallel_data.unused_tx_parallel_data
--		
--		rx_parallel_data_rx_parallel_data               => rx_parallel_data,               --        rx_parallel_data.rx_parallel_data
--		rx_header_rx_control                            => rx_header,                            --               rx_header.rx_control
--		
--      rx_enh_data_valid_rx_enh_data_valid             => rx_enh_data_valid,             --       rx_enh_data_valid.rx_enh_data_valid
--	   rx_data_valid_rx_data_valid                     => rx_data_valid,                            --                               rx_data_valid.rx_data_valid
--		unused_rx_parallel_data_unused_rx_parallel_data => rx_unused,  -- unused_rx_parallel_data.unused_rx_parallel_data
--	   
--		rx_bitslip_rx_bitslip(0)                        => rx_bitslip,
--		tx_fifo_full_tx_fifo_full                       => tx_fifo_full,                              --                                tx_fifo_full.tx_fifo_full
--      tx_fifo_pfull_tx_fifo_pfull                     => tx_fifo_pfull,                            --                               tx_fifo_pfull.tx_fifo_pfull
--		rx_fifo_rd_en_rx_fifo_rd_en                     => rx_fifo_rd_en,                            --                               rx_fifo_rd_en.rx_fifo_rd_en
--		reset_control_tx_ready_tx_ready                 => tx_ready,                        --                      reset_control_tx_ready.tx_ready
--      reset_control_rx_ready_rx_ready                 => rx_ready                         --                      reset_control_rx_ready.rx_ready
--        
--	);
	rx_fifo_rd_en(0) <= '1';
	tx_unused <= (others => '0');
	--rx_unused <= (others => '0');
	tx_coreclkin_clk <= tx_clkout_clk;
	rx_coreclkin_clk <= rx_clkout_clk;
	 
	counter : process(tx_clkout_clk) 
	begin
		if rising_edge(tx_clkout_clk(0)) then
			if (tx_transfer_ready_n = '1') then
				datacnt <= 0;
				tx_data_valid <= '0';
			else
				if (tx_fifo_pfull(0) = '0') then
					datacnt <= datacnt + 1;
					tx_data_valid <= '1';
			   else 
					tx_data_valid <= '0';
				end if;
			end if;
		end if;
	end process;
	
	rx_data_out <= rx_header & rx_parallel_data;
	
	--tx_parallel_data <=  std_logic_vector(to_unsigned(datacnt, tx_parallel_data'length)); --scrambler input
	tx_scrambler_data <=  "001"  & std_logic_vector(to_unsigned(datacnt, tx_scrambler_data'length-3)); --scrambler input
   tx_parallel_data <= tx_encoder_out(63 downto 0);
	tx_header <= tx_encoder_out(66 downto 64);
	cpu_reset <= not cpu_resetn;
	--transmit
	Scrambling : entity work.Scrambler
   port map (
		Clk => tx_clkout_clk(0),
		Scram_Rst => tx_transfer_ready_n,
		Data_In => tx_scrambler_data,
		Data_Out => Data_Scrambler_Out,
		LaneNumber => "0001",
		Scrambler_En => '1',
		Gearboxready => not tx_fifo_pfull(0)
	);
	
	 Encoding : entity work.Encoder
        port map (
            Clk             => tx_clkout_clk(0),
            Data_In         => Data_Scrambler_Out,
            Data_Out        => tx_encoder_out,
            Encoder_En      => '1',
            Encoder_Rst     => tx_transfer_ready_n,
            Gearboxready    => not tx_fifo_pfull(0)
        );
		  
   --receive
	Decoder : entity work.Decoder
	port map (
		Clk => rx_clkout_clk(0),
		Reset => rx_transfer_ready_n,
		Data_In => rx_data_out,
		Decoder_En => '1',
		Data_Valid_In => rx_enh_data_valid,
		Data_Valid_Out => Data_valid_decoder_out,
		Data_Out => Data_Decoder_Out,
		Decoder_Lock => Decoder_Lock,
		Sync_Error => Error_Decoder_Sync,
		Bitslip => rx_bitslip
	);
 
--    always @(posedge tx_clkout_clk or negedge cpu_resetn)
--			if (!cpu_resetn)
--				 datacnt <= 64'h0; //0x7FF_FFFF
--				 assign tx_data_valid = 1'b0;
--			else //2.3s
--				 assign tx_data_valid = 1'b1;
--				 datacnt <= datacnt + 1'b1;
--	 
	  --tx_parallel_data <= datacnt;
	  --tx_header <= "010";

     qsfp_lp_mode        <= '0';
     qsfp_mod_seln       <= '1';
     qsfp_rstn           <= cpu_resetn;
     qsfp_sda            <= 'Z';
     qsfp_scl            <= 'Z';

end architecture rtl;

