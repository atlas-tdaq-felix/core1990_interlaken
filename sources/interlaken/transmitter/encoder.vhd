library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;

entity Encoder is
    port(
        Clk          : in std_logic;                     -- Clock input
        Data_In      : in std_logic_vector(66 downto 0); -- Data input
        Data_Out     : out std_logic_vector(66 downto 0);-- Encoded 67-bit output

        Encoder_En   : in std_logic;                     -- Enables the encoder
        Encoder_Rst  : in std_logic;                     -- Resets the encoder

        Gearboxready : in std_logic
    );
end Encoder;

architecture Encoding of Encoder is
    signal data_p1, data_p2, data_p3   : std_logic_vector(66 downto 0);    
    signal disparity_pos_p2, disparity_pos_p3, disparity_neg_p2, disparity_neg_p3 : integer range 0 to 65; -- @suppress "The type of a signal has to be constrained in size"
    signal disparity_Running_s : integer; -- range 0 to 256; -- @suppress "The type of a signal has to be constrained in size"
    -- debug
    signal count, disparity_Running_avg, disparity_running_avg_s, offset_s: natural; -- @suppress 
    signal Offset    :  std_logic_vector(7 downto 0); -- @suppress "signal Offset is never read"
begin

    output : process (Clk, Encoder_Rst)
        variable offset_v : natural := 0;
    begin
        if (Encoder_Rst = '1') then
            Data_Out    <= (others => '0');
            data_p1     <= (others => '0');
            data_p2     <= (others => '0');
            data_p3     <= (others => '0');
            disparity_running_s <= 128;
            
            --debug
--            disparity_Running_avg   <= 0;
--            disparity_Running_avg_s <= 0;
--            count    <= 1;
--            offset   <= (others => '0');
--            offset_v := 0;
--            offset_s <= 0;
            
        elsif (rising_edge(Clk)) then
            if (Encoder_En = '1' and Gearboxready = '1') then   
                                
                --Pipelining data and disparity signals
                data_p1 <= data_in;
                data_p2 <= data_p1;

                -- generate encoded data (non-inverted case)
                data_p3 <= data_p2;
                data_p3(66) <= '0';

                -- add the parity of data_p2(66) (non-inverted case)
                disparity_pos_p3 <= disparity_pos_p2; --p3
                disparity_neg_p3 <= disparity_neg_p2 + 1; --p3

                -- Determine whether data should be inverted or not
                if(Disparity_running_s >= 128) then
                    if (disparity_pos_p2 >= 32) then
                        data_p3(63 downto 0) <= not(data_p2(63 downto 0));
                        data_p3(66) <= '1';
                        Disparity_pos_p3 <= disparity_neg_p2 + 1;
                        disparity_neg_p3 <= disparity_pos_p2;
                    end if;
                else
                    if (disparity_pos_p2 < 32) then
                        data_p3(63 downto 0) <= not(data_p2(63 downto 0));
                        data_p3(66) <= '1';
                        Disparity_pos_p3 <= disparity_neg_p2 + 1;
                        disparity_neg_p3 <= disparity_pos_p2;
                    end if;
                end if;
                
                --------------------------------------------
--                -- Debug to see transmitted disparity
--                for j in 63 downto 0 loop
--                    if (Data_p3(j) = '1') then
--                        Offset_V := Offset_V + 1;
--                    else
--                        Offset_V := Offset_V - 1;
--                    end if;
--                end loop;
                
--                --Offset <= std_logic_vector(to_unsigned(Offset_V, Offset'length));
--                Offset_s <= Offset_V;
                
--                -- Debug to see average transmitted disparity. Only for simulation purposes
--                Disparity_Running_avg <= (Disparity_Running_avg + Disparity_Running_s); 
--                count <= count +1;
--                Disparity_Running_avg_s <= Disparity_Running_avg/count;
                --------------------------------------------
                
                Data_Out <= data_p3;
                Disparity_running_s <= Disparity_running_s + Disparity_pos_p3 - disparity_neg_p3; --p4

            end if;
        end if;
    end process output;
    
    
    
 -- Calculating positive disparity of data
    run_disp_pos : process (Clk, Encoder_Rst)
        variable Disparity_v : natural range 0 to 64 := 0; -- @suppress "The type of a variable has to be constrained in size"
    begin
        if (Encoder_Rst = '1') then
            Disparity_v := 0;
            disparity_pos_p2 <= 0;
        elsif (rising_edge(Clk)) then
            if (Encoder_En = '1' and Gearboxready = '1') then
                
                Disparity_v := 0;
                for j in 63 downto 0 loop
                    if (data_p1(j) = '1') then
                        Disparity_v := Disparity_v + 1; --p1
                    end if;
                end loop;
                disparity_pos_p2 <= Disparity_v; --p2
                
            end if;     
        end if;
    end process run_disp_pos;
    
    -- Calculating negative disparity of data
    run_disp_neg : process (Clk, Encoder_Rst)
        variable Disparity_v : natural range 0 to 64:= 0; -- @suppress "The type of a variable has to be constrained in size"
    begin
        if (Encoder_Rst = '1') then
            Disparity_v := 0;
            disparity_neg_p2 <= 0;
        elsif (rising_edge(Clk)) then
            if (Encoder_En = '1' and Gearboxready = '1') then
                
                Disparity_v := 0;
                for j in 63 downto 0 loop
                    if (data_p1(j) = '0') then
                        Disparity_v := Disparity_v + 1;
                    end if;
                end loop;
                disparity_neg_p2 <= Disparity_v;
                
            end if;     
        end if;
    end process run_disp_neg;


    
end architecture Encoding;
