library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;

entity Encoder is
    port(
        Clk          : in std_logic;                     -- Clock input
        Data_In      : in std_logic_vector(66 downto 0); -- Data input
        Data_Out     : out std_logic_vector(66 downto 0);-- Encoded 67-bit output

        --Data_Control : in std_logic;                     -- Determines whether the word is data or control

        Encoder_En   : in std_logic;                     -- Enables the encoder
        Encoder_Rst  : in std_logic;                     -- Resets the encoder

        --Offset       : out std_logic_vector(7 downto 0); -- Test to see the average values of ones and zeros
        Gearboxready : in std_logic
    );
end Encoder;

architecture Encoding of Encoder is
    signal data_p1, data_p2, data_p3   : std_logic_vector(66 downto 0);
    signal disparity_pos_p2, Disparity_pos_p3 : integer;
    signal Disparity_Running_s : integer;
    -- debug
    signal Disparity_Running_avg, Disparity_Running_avg_s, Offset_s: integer;
    signal Offset    :  std_logic_vector(7 downto 0);
begin

    output : process (Clk, Encoder_Rst)
        variable offset_v : integer := 0;
    begin
        if (Encoder_Rst = '1') then
            data_out    <= (others => '0');
            data_p1     <= (others => '0');
            data_p2     <= (others => '0');
            data_p3     <= (others => '0');
            Disparity_running_s <= 0;

            --debug
            Disparity_Running_avg   <= 0;
            Disparity_Running_avg_s <= 0;
            --count    <= 1;
            offset   <= (others => '0');
            offset_v := 0;
            offset_s <= 0;

        elsif (rising_edge(Clk)) then
            if (Encoder_En = '1' and Gearboxready = '1') then

                --Pipelining data and disparity signals
                data_p1 <= data_in;
                data_p2 <= data_p1;
                data_p3 <= data_p2;
                data_p3(66) <= '0';


                disparity_pos_p3 <= disparity_pos_p2; --p3

                -- Determine whether data should be inverted or not
                if(Disparity_running_s >= 32) then
                  data_p3(63 downto 0) <= not(data_p2(63 downto 0));
                  data_p3(66) <= '1';
                  Disparity_pos_p3 <= 64-disparity_pos_p2;
                end if;

                --------------------------------------------
--                -- Debug to see transmitted disparity
--                for j in 63 downto 0 loop
--                    if (Data_p3(j) = '1') then
--                        Offset_V := Offset_V + 1;
--                    else
--                        Offset_V := Offset_V - 1;
--                    end if;
--                end loop;

--                --Offset <= std_logic_vector(to_unsigned(Offset_V, Offset'length));
--                Offset_s <= Offset_V;

----                -- Debug to see average transmitted disparity. Only for simulation purposes
--                Disparity_Running_avg <= (Disparity_Running_avg + Disparity_Running_s);
--                count <= count +1;
--                Disparity_Running_avg_s <= Disparity_Running_avg/count;
                --------------------------------------------

                data_out <= data_p3;
                Disparity_running_s <= Disparity_running_s + Disparity_pos_p3 - (64-disparity_pos_p3); --p4

            end if;
        end if;
    end process output;

 -- Calculating positive disparity of data
    run_disp_pos : process (Clk, Encoder_Rst)
        variable Disparity_v : integer := 0;
    begin
        if (Encoder_Rst = '1') then
            Disparity_v := 0;
            disparity_pos_p2 <= 0;
        elsif (rising_edge(Clk)) then
            if (Encoder_En = '1' and Gearboxready = '1') then

                Disparity_v := 0;
                for j in 63 downto 0 loop
                    if (data_p1(j) = '1') then
                        Disparity_v := Disparity_v + 1; --p1
                    end if;
                end loop;
                disparity_pos_p2 <= Disparity_v; --p2
            end if;
        end if;
    end process run_disp_pos;




end architecture Encoding;
