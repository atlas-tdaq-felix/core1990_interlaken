library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;        -- @suppress "Deprecated package" (used for sigasi to recognize - operator with std_logic vector)

use work.interlaken_package.all;

library XPM;
use xpm.vcomponents.all;
use work.axi_stream_package.ALL;

entity interlaken_transmitter_channel is
    generic(
        BurstMax         : positive;        -- Configurable value of BurstMax
        BurstShort       : positive;        -- Configurable value of BurstShort
        MetaFrameLength  : positive         -- Configurable value of PacketLength
    );
    port(
        clk             : in  std_logic;
        reset           : in  std_logic;
        TX_Data_Out     : out std_logic_vector(66 downto 0);
        TX_Gearboxready : in  std_logic;
        FlowControl     : in  std_logic_vector(15 downto 0);
        HealthLane      : in  std_logic;
        axis_burst_mode : in  std_logic; -- Configures burst mode. 0 = burstmax/short; 1 = burst config ignored, only axis tlast triggers eop/sop
        m_axis_burst    : out axis_64_type;
        s_axis_aclk     : in  std_logic;
        s_axis          : in  axis_64_type;
        s_axis_tready   : out std_logic
    );
end entity interlaken_transmitter_channel;

architecture transmitter of interlaken_transmitter_channel is
    signal HealthInterface_s : std_logic;
    signal m_axis_tready    : std_logic;
    signal m_axis           : axis_64_type;
    signal insert_burst_idle: std_logic; -- IDLE word
    signal insert_burst_idle_df: std_logic; -- BC word
    signal insert_burst_sop: std_logic;
    signal insert_burst_seop : std_logic;
    signal insert_burst_eop: std_logic;
    signal insert_burst_eop_now: std_logic;
    signal insert_burst_eop_next: std_logic;
    signal insert_burst_max : std_logic;
    signal insert_burst_short : std_logic;
    
    signal SOP_FLAG : std_logic;
    --type slv8_array is array (natural range <>) of std_logic_vector(7 downto 0);
    signal tid_cur, tid_next :std_logic_vector(7 downto 0);
    signal m_axis_mapped : axis_64_type;
    signal m_axis_tready_mapped : std_logic;
    
    signal m_axis_aresetn : std_logic;
    signal FlowControl_s     : std_logic_vector(15 downto 0);
    signal BurstCounter_s : integer;
    signal idle_pad : std_logic;
    
begin

    m_axis_burst <= m_axis_mapped;
    HealthInterface_procc : process(HealthLane)
    begin
        HealthInterface_s <= '1';
        if (HealthLane = '0') then
            HealthInterface_s <= '0';
        end if;
    end process;

    m_axis_mapped <= m_axis;
    m_axis_tready <= m_axis_tready_mapped;

    lane_tx : entity work.Interlaken_Transmitter
        generic map(
            --BurstMax => BurstMax, -- Configurable value of BurstMax
            --BurstShort => BurstShort, -- Configurable value of BurstShort
            MetaFrameLength => MetaFrameLength, -- Configurable value of PacketLength
            Lanes => 1, -- Set to single lane
            LaneNumber => 1)
        port map(                
            clk => clk,
            reset => reset,
            TX_Lane_Data_Out => TX_Data_Out(66 downto 0),
            TX_Gearboxready => TX_Gearboxready,
            --FlowControl => FlowControl_s, -- Per channel flow control, 1 means Xon, 0 means Xoff.
            HealthLane => HealthLane,
            HealthInterface => HealthInterface_s,
            s_axis => m_axis_mapped, --: out axis_64_type;
            --Channel => tid_next,
            s_axis_tready => m_axis_tready_mapped, --: in std_logic;
            insert_burst_idle => insert_burst_idle,
            insert_burst_idle_df => insert_burst_idle_df,
            insert_burst_sop => insert_burst_sop,
            insert_burst_seop => insert_burst_seop,
            insert_burst_eop => insert_burst_eop
        );
    FlowControl_s <= FlowControl;
    m_axis_aresetn <= not reset;

    fifo0 : entity work.Axis64Fifo
        generic map(
            DEPTH => 2048
        )
        port map(
            s_axis_aresetn    => m_axis_aresetn,
            s_axis_aclk       => s_axis_aclk,
            s_axis            => s_axis,
            s_axis_tready     => s_axis_tready,
            m_axis_aclk       => clk,
            m_axis            => m_axis,
            m_axis_tready     => m_axis_tready,
            m_axis_prog_empty => open
        );


LaneFormatHandling: process (SOP_FLAG, m_axis, insert_burst_eop_now, insert_burst_eop_next, insert_burst_eop, insert_burst_seop, tid_cur, idle_pad, insert_burst_idle)
    variable addIdle : std_logic;
begin

    insert_burst_idle<= '0';
    --insert_burst_idle_df<= '0';
    insert_burst_sop <= '0';
    insert_burst_eop <= '0';
    insert_burst_seop <= '0';
    insert_burst_eop_now <= '0';
    
    if SOP_FLAG = '1' then
        tid_next <= m_axis.tid;
    else
        tid_next <= tid_cur;
    end if;
    
    -- if user data ready to follow this burst, combine the SOP and EOP (maybe add fifo threshold to see how much data is ready to follow up?)
    if(m_axis.tvalid ='1' and insert_burst_eop_next = '1' and insert_burst_idle = '0'  and insert_burst_short = '0') then
        insert_burst_seop <= '1';
        tid_next <= m_axis.tid;
    else
        insert_burst_eop <= insert_burst_eop_now or insert_burst_eop_next;
    end if;
    
    -- SOP when new valid data after an EOP and idle padding
    if(m_axis.tvalid ='1' and SOP_FLAG = '1' and insert_burst_idle = '0') then
        insert_burst_sop <= '1';
    end if;

    
    if idle_pad = '1' then
        insert_burst_idle <= '1'; --addIdle or idle_pad; --Add an idle in the remaining lanes to keep the channels aligned.
    end if;

end process LaneFormatHandling;


LaneFormatHandling_SOP_encoder: process (clk, reset)
begin
    if (reset = '1') then
        SOP_FLAG <= '1';
        tid_cur <= x"00";
    elsif rising_edge(clk) then
        tid_cur <= tid_next;
        
        if(insert_burst_eop='1' ) then
            SOP_FLAG <= '1';
        elsif (insert_burst_sop='1' ) then
            SOP_FLAG <= '0'; --when done reset flag
        end if;
    end if;
end process;


LaneFormatHandling_EOP_encoder: process (clk, reset)
begin
    if (reset = '1') then
        insert_burst_eop_next <= '0';
    elsif rising_edge(clk) then 
        insert_burst_eop_next <= '0';
        if (m_axis.tlast = '1' and m_axis.tvalid = '1' and m_axis_tready = '1') then
            insert_burst_eop_next <= '1';
        end if;
    end if;
end process;


burstCounter_proc: process(clk, reset)
    variable burstCounter: integer range 0 to BurstMax;
    variable insert_burst_max_next: std_logic;
begin
    if (reset = '1') then
        burstCounter := 0;
        insert_burst_max <= '0';
        insert_burst_max_next := '0';
        insert_burst_short <= '0';
        insert_burst_idle_df <= '0';
        idle_pad <= '0';
    elsif rising_edge(clk) then
        
        insert_burst_max <= '0';
        --insert_burst_short <= '0';
        insert_burst_idle_df <= '0';
        idle_pad <= '0';
        
        if(insert_burst_short = '1') then
            idle_pad <= '1';
            burstCounter := burstCounter + 8;
            if burstCounter >= (BurstShort) then -- equal or bigger (since EOP/tlast and burst short can trigger same moment and miss the equal frame)
                burstCounter := 0;
                insert_burst_short <= '0';
                idle_pad <= '0';
            end if;
        elsif SOP_FLAG = '1' or insert_burst_seop = '1' then -- reset counter after SOP of SEOP word
            burstCounter :=  0; --SOP is not counted in frame length, so all others are counter
        elsif(m_axis_mapped.tvalid = '1' and m_axis_tready_mapped = '1' and axis_burst_mode = '0') then
            -- skip tlast because otherwise max or short get triggered while the packet already ended
            if (m_axis_mapped.tlast = '0') then
                -- BurstMax
                burstCounter := burstCounter + 8;
                if burstCounter = (BurstMax) then
                    burstCounter := 0;
                    insert_burst_max <= '1';
                    insert_burst_idle_df <= '1';
                end if;
            else
                -- BurstShort
                if burstCounter < (BurstShort-8) then
                    burstCounter := burstCounter + 8;
                    insert_burst_short <= '1';
                end if;
                
            end if;
        end if;
        burstCounter_s <= burstCounter;

    end if;
end process;

end architecture transmitter;
