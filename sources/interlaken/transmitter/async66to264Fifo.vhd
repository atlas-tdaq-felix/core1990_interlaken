library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library XPM;
use XPM.VCOMPONENTS.ALL;

entity sync66to264Fifo is
  generic (
    DEPTH : integer:=2048;
    LANES : integer := 4;
    FIFO_MEMORY_TYPE : string := "auto"
  );
  port (
    clk : in std_logic;
    rst : in std_logic;
    data_in : in std_logic_vector(66 downto 0);
    data_out : out std_logic_vector(67*LANES-1 downto 0);
    
    rd_en : in std_logic;
    wr_en : in std_logic;
    
    data_valid : out std_logic;
    
    --Indication that the FIFO contains a block of data (for MUX).
    prog_full  : out std_logic;
    prog_empty : out std_logic
  );
end entity sync66to264Fifo;

architecture rtl of sync66to264Fifo is
begin
  xpm_fifo_sync_inst : xpm_fifo_sync
  generic map (
    DOUT_RESET_VALUE => "0",    -- String
    ECC_MODE => "no_ecc",       -- String
    FIFO_MEMORY_TYPE => FIFO_MEMORY_TYPE, -- String
    FIFO_READ_LATENCY => 1,     -- DECIMAL
    FIFO_WRITE_DEPTH => DEPTH,   -- DECIMAL
    FULL_RESET_VALUE => 0,      -- DECIMAL
    PROG_EMPTY_THRESH => 10,    -- DECIMAL
    PROG_FULL_THRESH => DEPTH-16,     -- DECIMAL
    RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
    READ_DATA_WIDTH => LANES*67,      -- DECIMAL
    READ_MODE => "fwft",         -- String
    SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    USE_ADV_FEATURES => "1707", -- String
    WAKEUP_TIME => 0,           -- DECIMAL
    WRITE_DATA_WIDTH => 67,     -- DECIMAL
    WR_DATA_COUNT_WIDTH => 1    -- DECIMAL
  )
  port map (
      sleep => '0',
      rst    => rst,
      wr_clk => clk,
      wr_en  => wr_en,
      din    => data_in,
      full         => open,
      prog_full  => prog_full,
      wr_data_count => open,
      overflow     => open,
      wr_rst_busy   => open,
      almost_full  => open,
      wr_ack        => open, -- useful?
      rd_en  => rd_en,
      dout   => data_out,
      empty        => open,
      prog_empty => prog_empty,
      rd_data_count => open,
      underflow     => open,
      rd_rst_busy   => open,
      almost_empty => open,
      data_valid => data_valid,
      injectsbiterr => '0',
      injectdbiterr => '0',
      sbiterr       => open,
      dbiterr       => open
  );

end architecture;
