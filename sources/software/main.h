/*
 * main.h
 *
 *  Created on: Aug 10, 2023
 *      Author: nayibb
 */

//I2C Mezzanine
#define Si5345_1_ADDRESS    0x68
#define Si5345_2_ADDRESS    0x69
#define FF_TRANSMIT_ADDRESS 0x50
#define FF_RECEIVE_ADDRESS  0x54
#define PCA9548A_ADDRESS    0x70 //PCA9548A I2C_MUX

//I2C VCU128
#define TCA6416A_ADDRESS   0x20 //TCA6416A 16 BIT PORT EXTENDER
#define TCA9548A_1_ADDRESS 0x74 // I2C_MUX Clocks/EEPROM
#define PCA9544A_ADDRESS   0x75 // I2C_MUX INA226/PMBUS
#define TCA9548A_2_ADDRESS 0x76 // I2C_MUX FMCP/Sysmon/QSFP
