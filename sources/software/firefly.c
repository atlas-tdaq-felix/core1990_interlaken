/*
 * firefly.c
 *
 *  Created on: Aug 7, 2023
 *      Author: nayibb
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "i2c_functions.h"
#include "xil_exception.h"
#include "xil_io.h"
#include "sleep.h"

#include "firefly.h"
#include "i2c_components.h"
#include "main.h"

/*
 * Sets the PCA9548A I2C mux to access the desired firefly module
 *
 * @param   num_FF FireFly module number/output channel of I2C mux (FireFly 1 - 4)
 *
 */
int firefly_sel(u8 FF_num){
	tca9548_config(PCA9548A_ADDRESS, FF_num);
	tca9548_read(PCA9548A_ADDRESS);
	return XST_SUCCESS;
}

/*
 * Reads ID info from FireFly module (Vendor Name / Part Number / Serial Number)
 */
int firefly_read_id(u8 DeviceAddress){

	u8 buf[16];

	i2c_read_register(DeviceAddress, 0x98, buf, 10);
	xil_printf("Vendor ");
	for (u8 i=0; i<10; i++){
		xil_printf("%c", buf[i]);
	}
	xil_printf("\n\r");

	i2c_read_register(DeviceAddress, 0xAB, buf, 16);
	xil_printf("Part ");
	for (u8 i=0; i<16; i++){
		xil_printf("%c", buf[i]);
	}
	xil_printf("\n\r");

	i2c_read_register(DeviceAddress, 0xBD, buf, 10);
	xil_printf("Serial ");
	for (u8 i=0; i<10; i++){
		xil_printf("%c", buf[i]);
	}
	xil_printf("\n\r");

	return XST_SUCCESS;
}

/*
 * Reads status info from FireFly module
 */
int firefly_read_status(){

	u8 buf[16];

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x02, buf, 1);
    xil_printf("Reg 2 - Val 0x%02x \n\r", buf[0]);

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x06, buf, 1);
    xil_printf("Reg 6 - Val 0x%02x \n\r", buf[0]);

	return XST_SUCCESS;
}

/*
 * Reads interrupt info from FireFly module
 *
 * @note This function starts reading the interrupt registers
 */
int firefly_read_tx_interrupt(){

	u8 buf[16];

	//Latched Alarm – Transmit Loss Of Signal (LOS)
	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x07, buf, 4);
	for (u8 i=0; i<4; i++){
		u8 reg = 0x07 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	//
	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x11, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x11 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x14, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x14 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	return XST_SUCCESS;
}

/*
 * Reads interrupt info from FireFly module
 *
 * @note This function starts reading the interrupt registers
 */
int firefly_read_rx_interrupt(){

	u8 buf[16];

	//Latched Alarm – Receive LOS
	i2c_read_register(FF_RECEIVE_ADDRESS, 0x07, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x07 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	//Latched Alarm – Receive Power High-Low Alarms
	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x0E, buf, 3);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x11 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	//Latched Alarm- Receive Temperature
	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x11, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x20 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	//Latched Alarm – Receive CDR Loss Of Lock (LOL)
	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x14, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x20 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	return XST_SUCCESS;
}
/*
 * Reads control info from FireFly module
 *
 * @note This function starts reading the control registers from 0x52 (Channel disable) till 0x59 (Polarity invert)
 */
int firefly_read_control(){

	u8 buf[16];

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x34, buf, 16);
	for (u8 i=0; i<16; i++){
		u8 reg = 0x34 + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0x4A, buf, 2);
	for (u8 i=0; i<2; i++){
		u8 reg = 0x4A + i;
		xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[i]);
	}

	buf[0] = 0x02;
	i2c_write(FF_TRANSMIT_ADDRESS, 0x7F, buf, 1);

	i2c_read_register(FF_TRANSMIT_ADDRESS, 0xFF, buf, 1);
	u8 reg = 0xFF;
	xil_printf("Reg %d - Val 0x%02x \n\r", reg, buf[0]);

	buf[0] = 0x00;
	i2c_write(FF_TRANSMIT_ADDRESS, 0x7F, buf, 1);

	return XST_SUCCESS;
}
