/*
 * i2c_components.h
 *
 *  Created on: Aug 10, 2023
 *      Author: nayibb
 */

int tca9548_config(u8 DeviceAddress, u8 setMux);
int tca9548_read(u8 DeviceAddress);
int tca6416a_config(u8 DeviceAddress, u16 output, u16 polarity, u16 inout);
int tca6416a_read(u8 DeviceAddress);
