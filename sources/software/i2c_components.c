/*
 * i2c_components.c
 *
 *  Created on: Aug 10, 2023
 *      Author: nayibb
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xil_exception.h"
#include "xil_io.h"
#include "i2c_functions.h"
#include "i2c_components.h"

/*
 * Configures the TCA9548 I2C multiplexer
 *
 * @param   DeviceAddress I2C address of the device
 * @param 	setMux        sets enabled outputs, each bit corresponds to one of the eight outputs
 *
 */
int tca9548_config(u8 DeviceAddress, u8 setMux){
	i2c_write(DeviceAddress, setMux, 0, 0);
	return XST_SUCCESS;
}

/*
 * Reads the TCA9548 I2C multiplexer that contains no register addresses, just a command byte
 *
 * @param   DeviceAddress I2C address of the device
 *
 */
int tca9548_read(u8 DeviceAddress){
	u8 buf[1];
	i2c_read(DeviceAddress, 0x01, buf, 1);
	xil_printf("I2C tca9548 mux read val 0x%x from device 0x%x \n\r", buf[0], DeviceAddress);
	return XST_SUCCESS;
}

/*
 * Reads the TCA6416A port extender
 *
 * @param   DeviceAddress I2C address of the device
 *
 */
int tca6416a_read(u8 DeviceAddress){
	u8 buffer[2];
	//Read port extender
	xil_printf("Read port extender tca6416a 0x%02x \n\r", DeviceAddress);
	xil_printf("| P7-0 | P17-10 |\n\r", buffer[0], buffer[1]);
	i2c_read_register(DeviceAddress, 0x00, buffer, 2);
	xil_printf("| 0x%02x | 0x%02x |\n\r", buffer[0], buffer[1]);
	i2c_read_register(DeviceAddress, 0x02, buffer, 2);
	xil_printf("| 0x%02x | 0x%02x |\n\r", buffer[0], buffer[1]);
	i2c_read_register(DeviceAddress, 0x04, buffer, 2);
	xil_printf("| 0x%02x | 0x%02x |\n\r", buffer[0], buffer[1]);
	i2c_read_register(DeviceAddress, 0x06, buffer, 2);
	xil_printf("| 0x%02x | 0x%02x |\n\r", buffer[0], buffer[1]);

	return XST_SUCCESS;
}

/*
 * Configures the TCA6416A port extender
 *
 * @param   DeviceAddress I2C address of the device
 * @param	output 		  sets the outgoing logic levels of the pins defined as outputs in the config register
 * @param	polarity 	  sets inversion of pins defined as inputs by the config register (1=inverted, 0=original polarity)
 * @param	config 		  sets the direction of I/O pins (1= input, 0=output)
 *
 * @note All u16 inputs are read as P17-P0
 *
 */
int tca6416a_config(u8 DeviceAddress, u16 output, u16 polarity, u16 config){
	u8 buffer[2];

	buffer[0] = output & 0xFF;
	buffer[1] = (output>>8) & 0xFF;
	i2c_write(DeviceAddress, 0x02, buffer, 2);

	buffer[0] = polarity & 0xFF;
	buffer[1] = (polarity>>8) & 0xFF;
	i2c_write(DeviceAddress, 0x04, buffer, 2);

	buffer[0] = config & 0xFF;
	buffer[1] = (config>>8) & 0xFF;
	i2c_write(DeviceAddress, 0x06, buffer, 2);

	return XST_SUCCESS;
}
