/*
 * firefly.h
 *
 *  Created on: Aug 7, 2023
 *      Author: nayibb
 */



int firefly_sel(u8 FF_num);
int firefly_read_id(u8 DeviceAddress);
int firefly_read_status();
int firefly_read_control();
int firefly_read_tx_interrupt();
int firefly_read_rx_interrupt();
int firefly_read_rx_rssi();
