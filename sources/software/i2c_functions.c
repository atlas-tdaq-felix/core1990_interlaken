
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xiic.h"
#include "xintc.h"
#include "i2c_functions.h"

static int SetupInterruptSystem(XIic *IicInstPtr);
static void SendHandler(XIic *InstancePtr);
static void ReceiveHandler(XIic *InstancePtr);
static void StatusHandler(XIic *InstancePtr, int Event);

XIic IicInstance;	/* The instance of the IIC device. */
XIntc Intc; 	/* The instance of the Interrupt Controller Driver */

volatile u8 TransmitComplete;	/* Flag to check completion of Transmission */
volatile u8 ReceiveComplete;	/* Flag to check completion of Reception */



/*
 * Initializes the I2C device
 *
 * @param   IicDeviceId   Definition for IIC peripheral
 *
 */
int i2c_init(u16 IicDeviceId)
{
	int Status;
	static int Initialized = FALSE;
	XIic_Config *ConfigPtr;	/* Pointer to configuration data */

	if (!Initialized) {
		Initialized = TRUE;

		/*
		 * Initialize the IIC driver so that it is ready to use.
		 */
		ConfigPtr = XIic_LookupConfig(IicDeviceId);
		if (ConfigPtr == NULL) {
			return XST_FAILURE;
		}

		Status = XIic_CfgInitialize(&IicInstance, ConfigPtr,
						ConfigPtr->BaseAddress);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Setup handler to process the asynchronous events which occur,
		 * the driver is only interrupt driven such that this must be
		 * done prior to starting the device.
		 */
		XIic_SetSendHandler(&IicInstance, &IicInstance,
					(XIic_Handler) SendHandler);
		XIic_SetRecvHandler(&IicInstance, &IicInstance,
					(XIic_Handler) ReceiveHandler);
		XIic_SetStatusHandler(&IicInstance, &IicInstance,
					  (XIic_StatusHandler) StatusHandler);

		/*
		 * Connect the ISR to the interrupt and enable interrupts.
		 */
		Status = SetupInterruptSystem(&IicInstance);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Start the IIC driver such that it is ready to send and
		 * receive messages on the IIC interface, set the address
		 * to send to which is the temperature sensor address
		 */

		XIic_Start(&IicInstance);
		xil_printf("I2C init done\n\r");
	}
	return XST_SUCCESS;
}

/*
 * Read a register by writing the desired register address and after this reads the response
 *
 * @param   DeviceAddress  Address of I2C device to be communicated with
 * @param   Register   Address of the register to be read
 * @param   *data      Pointer to where the read data can be stored
 * @param   ByteCount  Amount of bytes to be read
 *
 */
int i2c_read_register(u8 DeviceAddress, u8 Register, u8 *data, u16 ByteCount){
	i2c_write(DeviceAddress, Register, 0, 0);
	i2c_read(DeviceAddress, Register, data, ByteCount);
	return XST_SUCCESS;
}

/*
 * Writes the desired bytes to a I2C device
 *
 * @param   DeviceAddress  Address of I2C device to be communicated with
 * @param   Register   Address to write to (first transmitted byte)
 * @param   *data      Pointer to where the to be transmitted data is stored
 * @param   ByteCount  Amount of bytes to be read
 *
 */
int i2c_write(u8 DeviceAddress, u8 Register, u8 *data, u16 ByteCount)
{
	int Status, i;
	TransmitComplete = 1;
    IicInstance.Stats.TxErrors = 0;

	// Reserve array for to be transmitted data and always send register first
	u8 buffer[ByteCount+1];
	buffer[0] = Register;

	// When data provided, place this in the transmit buffer
	for (i=0; i<ByteCount; i++){
		buffer[1+i] = data[i];
	}

	// Only when the i2c device address is new, set the new address
	u8 ReadAddress = XIic_GetAddress(&IicInstance, XII_ADDR_TO_SEND_TYPE);
	if (ReadAddress != DeviceAddress){
		XIic_SetAddress(&IicInstance, XII_ADDR_TO_SEND_TYPE, DeviceAddress);
		xil_printf("I2C address set 0x%02x \n\r", DeviceAddress);
	}

	// Start the IIC device
	Status = XIic_Start(&IicInstance);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Send the Data
	Status = XIic_MasterSend(&IicInstance, buffer, ByteCount+1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Wait till the transmission is complete
	while ((TransmitComplete) || (XIic_IsIicBusy(&IicInstance) == TRUE)) {
		/*
		 * This condition is required to be checked in the case where we
		 * are writing two consecutive buffers of data to the EEPROM.
		 * The EEPROM takes about 2 milliseconds time to update the data
		 * internally after a STOP has been sent on the bus.
		 * A NACK will be generated in the case of a second write before
		 * the EEPROM updates the data internally resulting in a
		 * Transmission Error.
		 */
		if (IicInstance.Stats.TxErrors != 0) {

			// Enable the IIC device.
			Status = XIic_Start(&IicInstance);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			if (!XIic_IsIicBusy(&IicInstance)) {
				// Send the Data.
				Status = XIic_MasterSend(&IicInstance, buffer, ByteCount);
				if (Status == XST_SUCCESS) {
					IicInstance.Stats.TxErrors = 0;
				}
				else {
				}
			}
		}
	}

    // Stop the IIC device
	Status = XIic_Stop(&IicInstance);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

/*
 * Reads a defined amount of bytes from a I2C device
 *
 * @param   DeviceAddress  Address of I2C device to be communicated with
 * @param   Register   Address of the register to be read
 * @param   *BufferPtr Pointer to where the read data can be stored
 * @param   ByteCount  Amount of bytes to be read
 *
 */
int i2c_read(u8 DeviceAddress, u8 Register, u8 *BufferPtr, u16 ByteCount)
{
	int Status;
	ReceiveComplete = 1;

	u8 ReadAddress = XIic_GetAddress(&IicInstance, XII_ADDR_TO_SEND_TYPE);
	if (ReadAddress != DeviceAddress){
		XIic_SetAddress(&IicInstance, XII_ADDR_TO_SEND_TYPE, DeviceAddress);
		xil_printf("I2C address set 0x%02x \n\r", DeviceAddress);
	}

	// Start the IIC device.
	Status = XIic_Start(&IicInstance);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Receive the Data.
	Status = XIic_MasterRecv(&IicInstance, BufferPtr, ByteCount);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Wait till all the data is received.
	while ((ReceiveComplete) || (XIic_IsIicBusy(&IicInstance) == TRUE)) {

	}

	// Stop the IIC device.
	Status = XIic_Stop(&IicInstance);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

/*
 * Initializes the interrupt system
 *
 * @param   *IicPtr  Instance of the Interrupt Controller
 *
 */
static int SetupInterruptSystem(XIic *IicPtr)
{
	int Status;

	/*
	 * Initialize the interrupt controller driver so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h
	 */
	Status = XIntc_Initialize(&Intc, INTC_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	/*
	 * Connect a device driver handler that will be called when an interrupt
	 * for the device occurs, the device driver handler performs the
	 * specific interrupt processing for the device
	 */
	Status = XIntc_Connect(&Intc, INTC_IIC_INTERRUPT_ID,
					XIic_InterruptHandler, IicPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Start the interrupt controller such that interrupts are recognized
	 * and handled by the processor.
	 */
	Status = XIntc_Start(&Intc, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Enable the interrupts for the IIC device.
	XIntc_Enable(&Intc, INTC_IIC_INTERRUPT_ID);

	// Initialize the exception table.
	Xil_ExceptionInit();

	// Register the interrupt controller handler with the exception table.
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				 (Xil_ExceptionHandler) XIntc_InterruptHandler,
				 &Intc);

	// Enable non-critical exceptions.
	Xil_ExceptionEnable();

	return XST_SUCCESS;

}

static void SendHandler(XIic *InstancePtr)
{
	TransmitComplete = 0;
}

static void ReceiveHandler(XIic *InstancePtr)
{
	ReceiveComplete = 0;
}

static void StatusHandler(XIic *InstancePtr, int Event)
{

}
