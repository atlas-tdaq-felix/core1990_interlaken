
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "Si5345A-RevD-VCU128_156_250_Registers.h"
#include "i2c_functions.h"
#include "xil_exception.h"
#include "xil_io.h"
#include "sleep.h"
#include "si5345.h"

/*
 * Configures the Si5345 Jitter Attenuator/Clock Multiplier
 *
 * @param   DeviceAddress I2C address of the device
 *
 * @note    Currently this function directly reads the register values from the included header file
 */
int si5345_config(u8 DeviceAddress){

	int reg_page, reg_address, reg_value;
	u8 current_page = 0xFF;
	u8 buf[10];

    for (int i=0; i<SI5345_REVD_REG_CONFIG_NUM_REGS; i++){
    	if (si5345_revd_registers[i].address == 0x000B) {
    		continue;
    	}
    	reg_page = (si5345_revd_registers[i].address>>8) & 0xFF;
    	reg_address  = si5345_revd_registers[i].address&0xFF;
    	reg_value =  si5345_revd_registers[i].value;
    	//xil_printf("Si5345 write reg 0x%02x - val 0x%02x\n\r", reg_address, reg_value);

    	if (current_page != reg_page) {
    		buf[0] = reg_page;
    		i2c_write(DeviceAddress, 0x01, buf ,1);
    		i2c_read_register(DeviceAddress, 0x01, buf, 1);
    		current_page = buf[0];
    		//xil_printf("Si5345 page set to 0x%02x from 0x%02x\n\r", reg_page, current_page);

    	}
    	buf[0] = reg_value;
    	i2c_write(DeviceAddress, reg_address, buf ,1);
    	//xil_printf("Si5345 write reg 0x%02x - val 0x%02x\n\r", reg_address, buf[0]);
//    	i2c_read_register(DeviceAddress, reg_address, buf, 1);
//    	xil_printf("Si5345 read reg 0x%02x - val 0x%02x\n\r", reg_address, buf[0]);
    }

    xil_printf("Si5345 0x%02x config done \n\r", DeviceAddress);
	return XST_SUCCESS;
}

/*
 * Checks whether the Si5345 Jitter Attenuator/Clock Multiplier has been configured correctly and the output is in lock
 *
 * @param   DeviceAddress I2C address of the device
 *
 * @note    Currently this function directly reads the register values from the included header file
 */
int si5345_output(u8 DeviceAddress){
	u8 reg, buffer[2];

	//set page to 0
	reg = 0x01;
	buffer[0] = 0x00;
	i2c_write(DeviceAddress, 0x01, buffer, 1);

	// Wait for si5345 to become stable
	sleep(1);

	reg = 0x0D;
	i2c_read_register(DeviceAddress, reg, buffer, 1);
	xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);

	reg = 0x12;
	i2c_read_register(DeviceAddress, reg, buffer, 1);
	buffer[0] = 0x00;
	i2c_write(DeviceAddress, reg, buffer, 1);
	i2c_read_register(DeviceAddress, reg, buffer, 1);
	//xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);

	reg = 0x13;
	i2c_read_register(DeviceAddress, reg, buffer, 1);
	buffer[0] = 0x00;
	i2c_write(DeviceAddress, reg, buffer, 1);
	i2c_read_register(DeviceAddress, reg, buffer, 1);
	//xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);

	reg = 0x0E;
	u8 i, lock;
	for (i=0; i<10; i++){
		i2c_read_register(DeviceAddress, reg, buffer, 1);
		lock = buffer[0] & 0x02;
		if (lock == 0){
			xil_printf("Si5345 0x%02x lock found in %d second(s) \n\r", DeviceAddress,i);
			break;
		}
		sleep(1);
	}
	if (lock != 0) {
		xil_printf("Si5345 0x%02x no lock found \n\r", DeviceAddress);
	}

	//i2c_read_register(DeviceAddress, reg, buffer, 1);
	xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);

//	buffer[0] = 0x00;
//	i2c_write(DeviceAddress, 0x01, buffer, 1);
//	i2c_read_register(DeviceAddress, 0x01, buffer, 1);
//	xil_printf("Reg 0x01 - Val 0x%02x \n\r", buffer[0]);
//	for (u8 i=0; i<50; i++){
//		u8 reg = 0x00 + i;
//		i2c_read_register(DeviceAddress, reg, buffer, 1);
//		xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);
//	}

//	int reg_page, reg_address, reg_value;
//	u8 current_page = 0xFF;
//	u8 buf[10];
//
//    for (int i=0; i<SI5345_REVD_REG_CONFIG_NUM_REGS; i++){
//
//    	reg_page = (si5345_revd_registers[i].address>>8) & 0xFF;
//    	reg_address  = si5345_revd_registers[i].address&0xFF;
//    	reg_value =  si5345_revd_registers[i].value;
//    	//xil_printf("Si5345 write reg 0x%02x - val 0x%02x\n\r", reg_address, reg_value);
//
//    	if (current_page != reg_page) {
//    		buf[0] = reg_page;
//    		i2c_write(DeviceAddress, 0x01, buf ,1);
//    		i2c_read_register(DeviceAddress, 0x01, buf, 1);
//    		current_page = buf[0];
//    		xil_printf("Si5345 page set to 0x%02x from 0x%02x\n\r", reg_page, current_page);
//
//    	}
//    	i2c_read_register(DeviceAddress, reg_address, buf, 1);
//    	xil_printf("Si5345 read reg 0x%02x - val 0x%02x\n\r", reg_address, buf[0]);
//    }

	return XST_SUCCESS;
}

/*
 * Checks whether the Si5345 Jitter Attenuator/Clock Multiplier has been configured correctly and the output is in lock
 *
 * @param   DeviceAddress I2C address of the device
 *
 * @note    Currently this function directly reads the register values from the included header file
 */
int si5345_verify(u8 DeviceAddress){

//	buffer[0] = 0x00;
//	i2c_write(DeviceAddress, 0x01, buffer, 1);
//	i2c_read_register(DeviceAddress, 0x01, buffer, 1);
//	xil_printf("Reg 0x01 - Val 0x%02x \n\r", buffer[0]);
//	for (u8 i=0; i<50; i++){
//		u8 reg = 0x00 + i;
//		i2c_read_register(DeviceAddress, reg, buffer, 1);
//		xil_printf("Reg 0x%02x - Val 0x%02x \n\r", reg, buffer[0]);
//	}

	int reg_page, reg_address, reg_value;
	u8 current_page = 0xFF;
	u8 buf[10];

    for (int i=0; i<SI5345_REVD_REG_CONFIG_NUM_REGS; i++){

    	reg_page = (si5345_revd_registers[i].address>>8) & 0xFF;
    	reg_address  = si5345_revd_registers[i].address&0xFF;
    	reg_value =  si5345_revd_registers[i].value;

    	if (current_page != reg_page) {
    		buf[0] = reg_page;
    		i2c_write(DeviceAddress, 0x01, buf ,1);
    		i2c_read_register(DeviceAddress, 0x01, buf, 1);
    		current_page = buf[0];
    		xil_printf("Si5345 page set to 0x%02x from 0x%02x\n\r", reg_page, current_page);

    	}

    	if ((reg_page == 0x00 && reg_address == 0x1C) || (reg_page == 0x05 && reg_address == 0x14)) {
            continue;
    	} else {
    	    i2c_read_register(DeviceAddress, reg_address, buf, 1);
    	}
    	if (buf[0] != reg_value) {
    		xil_printf("Si5345 read error reg 0x%02x - val 0x%02x, expected 0x%02x \n\r", reg_address, buf[0], reg_value);
    	}
//    	xil_printf("Si5345 read reg 0x%02x - val 0x%02x\n\r", reg_address, buf[0]);
    }

	return XST_SUCCESS;
}
