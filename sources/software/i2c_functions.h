

#define IIC_DEVICE_ID		XPAR_IIC_0_DEVICE_ID
#define INTC_DEVICE_ID		XPAR_INTC_0_DEVICE_ID
#define INTC_IIC_INTERRUPT_ID	XPAR_INTC_0_IIC_0_VEC_ID

int i2c_init(u16 IicDeviceId);
int i2c_write(u8 DeviceAddress, u8 Register, u8 *data, u16 ByteCount);
int i2c_read(u8 DeviceAddress, u8 Register, u8 *BufferPtr, u16 ByteCount);
int i2c_read_register(u8 DeviceAddress, u8 Register, u8 *data, u16 ByteCount);
